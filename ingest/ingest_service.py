import sqlite3
from pathlib import Path
from sqlite3 import Connection, Cursor, Error
from typing import Optional, List

from qgis.core import QgsProject
from qgis.utils import iface


class IngestService:
    def __init__(self, project: QgsProject = None):
        self.project: Optional[QgsProject] = project
        self.connection: Optional[Connection] = None
        self.cursor: Optional[Cursor] = None
        self.home_path: Path
        if self.project:
            self.home_path = Path(self.project.homePath())
        else:
            self.home_path = Path.cwd()

    def sqlite_connection(self, database_file_name) -> Optional[Connection]:
        try:
            self.connection = sqlite3.connect(self.home_path / database_file_name)
        except Error as e:
            message = 'Connection failed - ' + str(e)
            iface.messageBar().pushWarning('Database connection', message)
        finally:
            if self.connection:
                self.cursor = self.connection.cursor()
            return self.connection

    def delete_current_content(self) -> bool:
        return NotImplementedError

    def build_target_dict(self, file_name: str) -> List[tuple]:
        return NotImplementedError

    def process(self, file_name: str, database_file_name: str):
        return NotImplementedError
