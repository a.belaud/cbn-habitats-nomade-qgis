import csv
import sys
from typing import List

from qgis.core import QgsProject

from ingest_service import IngestService


class CatPhytoIngestService(IngestService):
    def __init__(self, project: QgsProject = None):
        super().__init__(project)

    def delete_current_content(self) -> bool:
        if self.connection and self.cursor:
            self.cursor.execute('DELETE FROM catalogue_phyto;')
            self.connection.commit()
            return True
        else:
            return False

    def import_new_cat(self, data) -> bool:
        if self.connection and self.cursor:
            self.cursor.executemany('INSERT INTO catalogue_phyto (code_phyto_parent, code_phyto, code_pvf1, '
                                    'nom_syntaxon_simple, nom_auteur_syntaxon_simple, code_phyto_syntaxon_retenu, '
                                    'presence) VALUES (?, ?, ?, ?, ?, ?, ?);', data)
            self.connection.commit()
            return True
        else:
            return False

    def build_target_dict(self, file_name: str) -> List[tuple]:
        target_data: List[tuple]
        with open(self.home_path / file_name, 'r', encoding='utf-8') as file:
            data = csv.DictReader(file, delimiter=';')
            target_data = [(line['supra'], line['id'], line['code'], line['syntaxon'], line['aut'],
                            line['correct'], int(line['presenceCBNBP']))
                           for line in data]
        return target_data

    def update_retained_syntaxon_name(self):
        if self.connection and self.cursor:
            self.cursor.execute('SELECT id, code_phyto_syntaxon_retenu FROM catalogue_phyto')
            syntaxons = self.cursor.fetchall()
            for syntaxon in syntaxons:
                self.cursor.execute('SELECT id, nom_syntaxon_simple FROM catalogue_phyto WHERE code_phyto=?',
                                    (syntaxon[1],))
                result = self.cursor.fetchone()
                self.cursor.execute('UPDATE catalogue_phyto SET nom_syntaxon_retenu=? WHERE id=?;',
                                    (result[1], syntaxon[0]))
            self.connection.commit()
            return True
        else:
            return False

    def process(self, file_name: str, database_file_name: str):
        self.sqlite_connection(database_file_name)
        target_data = self.build_target_dict(file_name)
        if len(target_data) > 0 and self.cursor:
            self.delete_current_content()
            self.import_new_cat(target_data)
            self.update_retained_syntaxon_name()


if __name__ == '__main__':
    ingest_service = CatPhytoIngestService()
    ingest_service.process(sys.argv[1], 'donnees_habitats.sqlite')
