import csv
import sys
from typing import List

from qgis.core import QgsProject

from ingest_service import IngestService


class ObserversIngestService(IngestService):
    def __init__(self, project: QgsProject = None):
        super().__init__(project)

    def delete_current_content(self) -> bool:
        if self.connection and self.cursor:
            self.cursor.execute('DELETE FROM observateurs;')
            self.connection.commit()
            return True
        else:
            return False

    def import_new_list_values(self, data) -> bool:
        if self.connection and self.cursor:
            self.cursor.executemany('INSERT INTO observateurs (obs_id, obs_prenom, obs_nom, obs_email, obs_turned_on)'
                                    ' VALUES (?, ?, ?, ?, ?);',
                                    data)
            self.connection.commit()
            return True
        else:
            return False

    def build_target_dict(self, file_name: str) -> List[tuple]:
        target_data: List[tuple]
        with open(self.home_path / file_name, 'r', encoding='utf-8') as file:
            data = csv.DictReader(file, delimiter=';')
            target_data = [(line['id'], line['prenom'], line['nom'], line['email'], line['active'])
                           for line in data]
        return target_data

    def process(self, file_name: str, database_file_name: str):
        self.sqlite_connection(database_file_name)
        target_data = self.build_target_dict(file_name)
        if len(target_data) > 0 and self.cursor:
            self.delete_current_content()
            self.import_new_list_values(target_data)


if __name__ == '__main__':
    ingest_service = ObserversIngestService()
    ingest_service.process(sys.argv[1], 'donnees_habitats.sqlite')
