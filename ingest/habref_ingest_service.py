import csv
import sys
from typing import List

from qgis.core import QgsProject

from ingest_service import IngestService


class HabrefIngestService(IngestService):
    def __init__(self, project: QgsProject = None):
        super().__init__(project)

    def delete_current_content(self) -> bool:
        if self.connection and self.cursor:
            self.cursor.execute('DELETE FROM habref;')
            self.connection.commit()
            return True
        else:
            return False

    def import_new_ref_habitat(self, data) -> bool:
        if self.connection and self.cursor:
            self.cursor.executemany('INSERT INTO habref (cd_hab, cd_typo, lb_code, '
                                    'lb_hab_fr, cd_hab_sup'
                                    ') VALUES (?, ?, ?, ?, ?);', data)
            self.connection.commit()
            return True
        else:
            return False

    def build_target_dict(self, file_name: str) -> List[tuple]:
        target_data: List[tuple]
        with open(self.home_path / file_name, 'r', encoding='utf-8') as file:
            data = csv.DictReader(file, delimiter=';')
            target_data = [(line['cd_hab'], line['cd_typo'], line['lb_code'], line['lb_hab_fr'], line['cd_hab_sup'])
                           for line in data]
        return target_data

    def process(self, file_name: str, database_file_name: str):
        self.sqlite_connection(database_file_name)
        target_data = self.build_target_dict(file_name)
        if len(target_data) > 0 and self.cursor:
            self.delete_current_content()
            self.import_new_ref_habitat(target_data)


if __name__ == '__main__':
    ingest_service = HabrefIngestService()
    ingest_service.process(sys.argv[1], 'donnees_habitats.sqlite')
