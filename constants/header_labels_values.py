TAXONS_HEADER_LABELS = ['cd_nom', 'taxon', 'strate_arbo', 'strate_arbu', 'strate_herb', 'strate_musc', 'suppr.']
SYNTAXONS_HEADER_LABELS = ['code_phyto', 'syntaxon', 'rec.', 'forme', 'com. ', 'attr. add.', 'suppr.']
HIC_HEADER_LABELS = ['code', 'libelle', 'rec.', 'forme', 'com. ', 'attr. add.', 'suppr.']
EUNIS_HEADER_LABELS = ['code', 'libelle', 'rec.', 'forme', 'com. ', 'attr. add.', 'suppr.']
CORINE_HEADER_LABELS = ['code', 'libelle', 'rec.', 'forme', 'com. ', 'attr. add.', 'suppr.']
HABITATS_HEADER_LABELS = ['code', 'libelle', 'code typo', 'typologie', 'suppr.']
STRATES_STRUCT_LABELS = ['id', 'strate', 'rec.', 'h min', 'h mod.', 'h max', 'suppr.']
