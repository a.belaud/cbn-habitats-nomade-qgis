from typing import Optional, List

from PyQt5.QtWidgets import QFrame, QLabel, QTableWidget, QLineEdit, QComboBox, QGroupBox, QListWidget, QTabWidget
from PyQt5.QtWidgets import QMessageBox, QCheckBox, QButtonGroup
from qgis.core import QgsFeature, QgsVectorLayer, QgsProject
from qgis.gui import QgsAttributeForm
from qgis.utils import iface

from .survey_widget_service import WidgetService
from ..constants.header_labels_values import *
from ..data.database_specification_service import DatabaseSpecificationService
from ..data.geometry_manager import GeometryManager
from ..data.list_values_provider import ListValuesProvider
from ..data.survey_data_manager import SurveyDataManager


class SurveyFormManager:
    def __init__(self, form: QgsAttributeForm, layer: QgsVectorLayer, feature: QgsFeature):
        self.form = form
        self.layer = layer
        self.feature = feature
        self.project: QgsProject = QgsProject.instance()
        self.data_manager: SurveyDataManager = SurveyDataManager(self.project, form, layer, feature)
        self.geometry_manager: GeometryManager = GeometryManager(layer, feature)
        self.list_values_provider: ListValuesProvider = ListValuesProvider(self.project)
        self.survey_feature: Optional[QgsFeature] = None

        # TODO replace this enumeration of widgets by a big dict, populate by specifications ?
        # global variables to store widgets
        self.frel: Optional[QFrame] = None
        self.fobs: Optional[QFrame] = None
        self.fobs_taxons: Optional[QFrame] = None
        self.ftypo_taxons: Optional[QFrame] = None
        self.fobs_syntaxons: Optional[QFrame] = None
        self.ftypo_syntaxons: Optional[QFrame] = None
        self.frame_jdd: Optional[QFrame] = None
        self.frame_syntaxon_complement: Optional[QFrame] = None
        self.frame_syntaxon_data_add: Optional[QFrame] = None

        self.labrel: Optional[QLabel] = None
        self.labobs: Optional[QLabel] = None
        self.save_rel: Optional[QLabel] = None
        self.activate: Optional[QLabel] = None
        self.add_taxon: Optional[QLabel] = None
        self.save_taxon: Optional[QLabel] = None
        self.add_syntaxon: Optional[QLabel] = None
        self.save_syntaxon: Optional[QLabel] = None
        self.cal1_label: Optional[QLabel] = None
        self.cal2_label: Optional[QLabel] = None
        self.rel_create_new_polygon_rel: Optional[QLabel] = None
        self.rel_create_new_line_rel: Optional[QLabel] = None
        self.rel_create_new_point_rel: Optional[QLabel] = None
        self.rel_syntax_create_new_polygon_rel: Optional[QLabel] = None
        self.rel_syntax_create_new_line_rel: Optional[QLabel] = None
        self.rel_syntax_create_new_point_rel: Optional[QLabel] = None
        self.lab_alerte_structure: Optional[QLabel] = None
        self.lab_alerte_desc_gen: Optional[QLabel] = None
        self.add_obs_btn: Optional[QLabel] = None
        self.suppr_obs_btn: Optional[QLabel] = None
        self.add_strate_struct: Optional[QLabel] = None

        self.rel_date_min: Optional[QLineEdit] = None
        self.rel_date_max: Optional[QLineEdit] = None
        self.taxon_cite: Optional[QLineEdit] = None
        self.taxon_valide: Optional[QLineEdit] = None
        self.taxon_cd_nom_cite: Optional[QLineEdit] = None
        self.taxon_cd_nom_valide: Optional[QLineEdit] = None
        self.obs_hab_nom_retenu: Optional[QLineEdit] = None
        self.obs_hab_code_retenu: Optional[QLineEdit] = None
        self.obs_hab_nom_cite: Optional[QLineEdit] = None
        self.obs_hab_code_cite: Optional[QLineEdit] = None
        self.syntaxon_retenu: Optional[QLineEdit] = None
        self.syntaxon_code_retenu: Optional[QLineEdit] = None
        self.syntaxon_cite: Optional[QLineEdit] = None
        self.syntaxon_code_cite: Optional[QLineEdit] = None
        self.obs_hab_rec_pct: Optional[QLineEdit] = None
        self.obs_hab_code_typo: Optional[QLineEdit] = None
        self.struct_rec_pct_strate: Optional[QLineEdit] = None
        self.struct_haut_mod_strate: Optional[QLineEdit] = None
        self.struct_haut_min_strate: Optional[QLineEdit] = None
        self.struct_haut_max_strate: Optional[QLineEdit] = None
        self.taxon_dominant_code: Optional[QLineEdit] = None
        self.taxon_dominant_nom: Optional[QLineEdit] = None
        self.obs_hab_taxon_dominant_code: Optional[QLineEdit] = None
        self.obs_hab_taxon_dominant_nom: Optional[QLineEdit] = None

        self.ctrl: Optional[QGroupBox] = None
        self.structure: Optional[QGroupBox] = None
        self.desc_gen: Optional[QGroupBox] = None
        self.obs_hab_action_buttons: Optional[QGroupBox] = None

        self.habitat_buttons: Optional[QButtonGroup] = None
        self.typo_habitat_buttons: Optional[QButtonGroup] = None

        self.observer_list: Optional[QListWidget] = None
        self.obs_hab_assoc_list: Optional[QListWidget] = None

        self.lst_obs: Optional[QComboBox] = None
        self.select_taxon: Optional[QComboBox] = None
        self.select_habitat: Optional[QComboBox] = None
        self.select_strate_struct: Optional[QComboBox] = None
        self.obs_hab_recouvrement: Optional[QComboBox] = None
        self.strate_arbo: Optional[QComboBox] = None
        self.strate_arbu: Optional[QComboBox] = None
        self.strate_herb: Optional[QComboBox] = None
        self.strate_musc: Optional[QComboBox] = None
        self.rel_type_rel_id: Optional[QComboBox] = None
        self.rel_protocole_id: Optional[QComboBox] = None
        self.syntaxon_recherche: Optional[QComboBox] = None
        self.habitat_recherche: Optional[QComboBox] = None
        self.obs_hab_forme: Optional[QComboBox] = None
        self.syntaxon_comm_non_sat_obs: Optional[QComboBox] = None
        self.taxon_dominant_recherche: Optional[QComboBox] = None
        self.select_taxon_dominant: Optional[QComboBox] = None

        self.syntaxon_diag_originale: Optional[QCheckBox] = None

        self.cal1: QMessageBox = QMessageBox()
        self.cal2: QMessageBox = QMessageBox()

        self.table_taxons: Optional[QTableWidget] = None
        self.table_habitats: Optional[QTableWidget] = None
        self.table_syn_habitats: Optional[QTableWidget] = None
        self.table_strates_struct: Optional[QTableWidget] = None

        self.tab_widget: Optional[QTabWidget] = None

        self.releves: Optional[QgsVectorLayer] = None
        self.observations: Optional[QgsVectorLayer] = None
        self.obs_habitats: Optional[QgsVectorLayer] = None
        self.liens_releve_observateur: Optional[QgsVectorLayer] = None
        self.liens_releve_strate: Optional[QgsVectorLayer] = None
        self.liens_obs_habitat: Optional[QgsVectorLayer] = None
        self.observateurs: Optional[QgsVectorLayer] = None
        self.catalogue_phyto: Optional[QgsVectorLayer] = None
        self.habref: Optional[QgsVectorLayer] = None
        self.taxref: Optional[QgsVectorLayer] = None

    def initialize(self):
        self.layer.startEditing()
        self.populate_variables()
        self.populate_layers()
        self.populate_combo_boxes()

        strates = [self.strate_arbo, self.strate_arbu, self.strate_herb, self.strate_musc]
        add_survey_to_syntaxon_widgets = [self.rel_syntax_create_new_point_rel, self.rel_syntax_create_new_line_rel,
                                          self.rel_syntax_create_new_polygon_rel]

        if self.releves is not None:
            self.survey_feature = self.geometry_manager.get_survey_feature(self.releves)
        else:
            iface.messageBar().pushWarning('Layer error', 'la couche releves n\'est pas disponible')

        self.prepare_table_widget(add_survey_to_syntaxon_widgets)
        self.populate_form()
        self.activate_event_listeners(strates, add_survey_to_syntaxon_widgets)
        self.prepare_startup_interface(add_survey_to_syntaxon_widgets)

    def populate_form(self):
        feature_id = 0
        if self.survey_feature:
            feature_id = self.survey_feature.id()
        if feature_id > 0:
            self.data_manager.map_survey(self.releves, self.survey_feature)
            self.data_manager.get_observers(feature_id, self.liens_releve_observateur, self.lst_obs, self.observer_list)
            self.data_manager.get_taxons(feature_id, self.observations, self.table_taxons)
            self.data_manager.get_syntaxon(self.form, self.survey_feature['rel_obs_syntaxon_id'], self.obs_habitats)
            self.data_manager.get_habitats(feature_id, self.obs_habitats, self.table_habitats, self.obs_hab_assoc_list,
                                           self.habitat_buttons, self.obs_hab_code_typo, self.frame_syntaxon_complement,
                                           self.frame_syntaxon_data_add, self.obs_hab_action_buttons)
            self.data_manager.get_typo_habitats(self.survey_feature, self.obs_habitats, self.table_syn_habitats)
            self.data_manager.get_strates(feature_id, self.liens_releve_strate, self.table_strates_struct)
        else:
            self.rel_type_rel_id.setCurrentIndex(self.rel_type_rel_id.findData(0))

    def populate_variables(self):
        # TODO replace this enumeration by a loop reading specifications
        self.frel = self.form.findChild(QFrame, "frel")
        self.fobs = self.form.findChild(QFrame, "fobs")
        self.fobs_taxons = self.form.findChild(QFrame, "frame_obs_taxons")
        self.ftypo_taxons = self.form.findChild(QFrame, "frame_typologie_phyto")
        self.fobs_syntaxons = self.form.findChild(QFrame, "frame_obs_habitats")
        self.ftypo_syntaxons = self.form.findChild(QFrame, "frame_typologie_symphyto")
        self.frame_jdd = self.form.findChild(QFrame, "frame_jdd")
        self.frame_syntaxon_complement = self.form.findChild(QFrame, "frame_syntaxon_complement")
        self.frame_syntaxon_data_add = self.form.findChild(QFrame, "frame_obs_hab_data_add_syntaxon")
        self.labrel = self.form.findChild(QLabel, "lab_form_rel")
        self.labobs = self.form.findChild(QLabel, "lab_form_obs")
        self.save_rel = self.form.findChild(QLabel, "save_releve")
        self.activate = self.form.findChild(QLabel, "edit_releve")
        self.add_taxon = self.form.findChild(QLabel, "add_taxon")
        self.save_taxon = self.form.findChild(QLabel, "save_taxon")
        self.add_syntaxon = self.form.findChild(QLabel, "lb_add_syntaxon")
        self.save_syntaxon = self.form.findChild(QLabel, "lb_save_syntaxon")
        self.cal1_label = self.form.findChild(QLabel, "calendrier_min")
        self.cal2_label = self.form.findChild(QLabel, "calendrier_max")
        self.rel_create_new_polygon_rel = self.form.findChild(QLabel, "rel_create_new_polygon_rel")
        self.rel_create_new_line_rel = self.form.findChild(QLabel, "rel_create_new_ligne_rel")
        self.rel_create_new_point_rel = self.form.findChild(QLabel, "rel_create_new_point_rel")
        self.rel_syntax_create_new_polygon_rel = self.form.findChild(QLabel, "rel_syntax_create_new_polygon_rel")
        self.rel_syntax_create_new_line_rel = self.form.findChild(QLabel, "rel_syntax_create_new_ligne_rel")
        self.rel_syntax_create_new_point_rel = self.form.findChild(QLabel, "rel_syntax_create_new_point_rel")
        self.lab_alerte_structure = self.form.findChild(QLabel, "lab_alerte_structure")
        self.lab_alerte_desc_gen = self.form.findChild(QLabel, "lab_alerte_desc_gen")
        self.add_obs_btn = self.form.findChild(QLabel, "add_obs_btn")
        self.suppr_obs_btn = self.form.findChild(QLabel, "suppr_obs_btn")
        self.add_strate_struct = self.form.findChild(QLabel, "add_strate_struct")
        self.rel_date_min = self.form.findChild(QLineEdit, "rel_date_min")
        self.rel_date_max = self.form.findChild(QLineEdit, "rel_date_max")
        self.taxon_cite = self.form.findChild(QLineEdit, "nom_cite")
        self.taxon_valide = self.form.findChild(QLineEdit, "nom_valide")
        self.taxon_cd_nom_cite = self.form.findChild(QLineEdit, "cd_nom_cite")
        self.taxon_cd_nom_valide = self.form.findChild(QLineEdit, "cd_nom_valide")
        self.obs_hab_nom_retenu = self.form.findChild(QLineEdit, "obs_habitat_nom_retenu")
        self.obs_hab_code_retenu = self.form.findChild(QLineEdit, "obs_habitat_code_retenu")
        self.obs_hab_nom_cite = self.form.findChild(QLineEdit, "obs_habitat_nom_cite")
        self.obs_hab_code_cite = self.form.findChild(QLineEdit, "obs_habitat_code_cite")
        self.syntaxon_retenu = self.form.findChild(QLineEdit, "syntaxon_retenu")
        self.syntaxon_code_retenu = self.form.findChild(QLineEdit, "syntaxon_code_retenu")
        self.syntaxon_cite = self.form.findChild(QLineEdit, "syntaxon_cite")
        self.syntaxon_code_cite = self.form.findChild(QLineEdit, "syntaxon_code_cite")
        self.obs_hab_rec_pct = self.form.findChild(QLineEdit, "obs_hab_rec_pct")
        self.obs_hab_code_typo = self.form.findChild(QLineEdit, "obs_hab_code_typo")
        self.struct_rec_pct_strate = self.form.findChild(QLineEdit, "strate_rec_pct")
        self.struct_haut_mod_strate = self.form.findChild(QLineEdit, "strate_haut_mod")
        self.struct_haut_min_strate = self.form.findChild(QLineEdit, "strate_haut_min")
        self.struct_haut_max_strate = self.form.findChild(QLineEdit, "strate_haut_max")
        self.taxon_dominant_code = self.form.findChild(QLineEdit, "syntaxon_taxon_dominant_cns_code")
        self.taxon_dominant_nom = self.form.findChild(QLineEdit, "syntaxon_taxon_dominant_cns_nom")
        self.obs_hab_taxon_dominant_code = self.form.findChild(QLineEdit, "syn_comm_non_sat_taxon_code")
        self.obs_hab_taxon_dominant_nom = self.form.findChild(QLineEdit, "syn_comm_non_sat_taxon_nom")
        self.ctrl = self.form.findChild(QGroupBox, 'typo_groupbox')
        self.structure = self.form.findChild(QGroupBox, 'groupbox_structure')
        self.desc_gen = self.form.findChild(QGroupBox, 'groupbox_desc_gen')
        self.obs_hab_action_buttons = self.form.findChild(QGroupBox, 'obs_hab_action_buttons')
        self.habitat_buttons = self.form.findChild(QButtonGroup, 'habitats_buttons')
        self.typo_habitat_buttons = self.form.findChild(QButtonGroup, 'habitat_buttons')
        self.observer_list = self.form.findChild(QListWidget, "observateurs_list")
        self.obs_hab_assoc_list = self.form.findChild(QListWidget, "list_hab_obs_assoc")
        self.lst_obs = self.form.findChild(QComboBox, "lst_obs")
        self.select_taxon = self.form.findChild(QComboBox, "select_taxon")
        self.select_habitat = self.form.findChild(QComboBox, "select_habitat")
        self.obs_hab_recouvrement = self.form.findChild(QComboBox, "obs_hab_rec_indic")
        self.obs_hab_forme = self.form.findChild(QComboBox, "obs_hab_forme_id")
        self.syntaxon_comm_non_sat_obs = self.form.findChild(QComboBox, "syn_comm_non_sat_obs")
        self.strate_arbo = self.form.findChild(QComboBox, "strate_arbo")
        self.strate_arbu = self.form.findChild(QComboBox, "strate_arbu")
        self.strate_herb = self.form.findChild(QComboBox, "strate_herb")
        self.strate_musc = self.form.findChild(QComboBox, "strate_musc")
        self.rel_type_rel_id = self.form.findChild(QComboBox, "rel_type_rel_id")
        self.rel_protocole_id = self.form.findChild(QComboBox, "rel_protocole_id")
        self.syntaxon_recherche = self.form.findChild(QComboBox, "syntaxon_recherche")
        self.habitat_recherche = self.form.findChild(QComboBox, "habitat_recherche")
        self.select_strate_struct = self.form.findChild(QComboBox, "cb_strate_struct")
        self.taxon_dominant_recherche = self.form.findChild(QComboBox, "taxon_dominant_recherche")
        self.select_taxon_dominant = self.form.findChild(QComboBox, "select_taxon_dominant")
        self.syntaxon_diag_originale = self.form.findChild(QCheckBox, "syn_diag_originale")
        self.table_habitats = self.form.findChild(QTableWidget, "table_habitats")
        self.table_syn_habitats = self.form.findChild(QTableWidget, "table_syn_habitats")
        self.table_taxons = self.form.findChild(QTableWidget, "table_taxons")
        self.table_strates_struct = self.form.findChild(QTableWidget, "table_strates_struct")
        self.tab_widget = self.form.findChild(QTabWidget, "tabWidget_phyto")

    def populate_layers(self):
        for layer in self.project.mapLayers().values():
            if layer.name() == 'releves':
                self.releves = layer
            elif layer.name() == 'observations':
                self.observations = layer
            elif layer.name() == 'obs_habitats':
                self.obs_habitats = layer
            elif layer.name() == 'liens_releve_observateur':
                self.liens_releve_observateur = layer
            elif layer.name() == 'liens_releve_strate':
                self.liens_releve_strate = layer
            elif layer.name() == 'liens_obs_habitat':
                self.liens_obs_habitat = layer
            elif layer.name() == 'observateurs':
                self.observateurs = layer
            elif layer.name() == 'catalogue_phyto':
                self.catalogue_phyto = layer
            elif layer.name() == 'taxref':
                self.taxref = layer
            elif layer.name() == 'habref':
                self.habref = layer

    def populate_combo_boxes(self):
        combo_boxes_specification: List[dict] = DatabaseSpecificationService.get_combo_boxes_specification()
        for combo_box_spec in combo_boxes_specification:
            for combo_box_name in combo_box_spec['input_name'].split(', '):
                widget: Optional[QComboBox] = self.form.findChild(QComboBox, combo_box_name)
                if widget is not None:
                    list_values = self.list_values_provider.get_values_from_list_name(combo_box_spec['input_list_name'])
                    WidgetService.load_combo_box(widget, list_values)

    def prepare_table_widget(self, add_survey_to_syntaxon_widgets):
        WidgetService.build_table(self.table_taxons, TAXONS_HEADER_LABELS)
        self.table_taxons.itemDoubleClicked.connect(
            lambda i: WidgetService.cell_clicked(i, self.survey_feature, self.observations, self.form, self.add_taxon,
                                                 self.save_taxon, self.select_taxon, self.habitat_buttons))

        self.table_habitats.itemDoubleClicked.connect(
            lambda i: WidgetService.cell_clicked(i, self.survey_feature, self.obs_habitats, self.form,
                                                 self.add_syntaxon, self.save_syntaxon, self.select_habitat,
                                                 self.habitat_buttons, add_survey_to_syntaxon_widgets))

        WidgetService.build_table(self.table_syn_habitats, HABITATS_HEADER_LABELS)
        self.table_syn_habitats.itemDoubleClicked.connect(
            lambda i: WidgetService.cell_clicked(i, self.survey_feature, self.obs_habitats, self.form, QLabel(),
                                                 QLabel(), self.habitat_recherche, QButtonGroup())
        )

        WidgetService.build_table(self.table_strates_struct, STRATES_STRUCT_LABELS)
        self.table_strates_struct.itemDoubleClicked.connect(
            lambda i: WidgetService.cell_clicked(i, self.survey_feature, self.liens_releve_strate, self.form, QLabel(),
                                                 QLabel(), self.select_strate_struct, QButtonGroup()))

    def activate_event_listeners(self, strates, add_survey_to_syntaxon_widgets):
        self.rel_type_rel_id.currentIndexChanged.connect(
            lambda: WidgetService.fobs_show(self.rel_type_rel_id, self.rel_protocole_id, self.fobs_taxons,
                                            self.fobs_syntaxons, self.ftypo_taxons, self.ftypo_syntaxons,
                                            self.save_rel, self.lab_alerte_structure, self.structure,
                                            self.lab_alerte_desc_gen, self.desc_gen))
        self.habitat_buttons.buttonClicked.connect(
            lambda: self.data_manager.get_habitats(self.survey_feature.id(), self.obs_habitats, self.table_habitats,
                                                   self.obs_hab_assoc_list, self.habitat_buttons,
                                                   self.obs_hab_code_typo, self.frame_syntaxon_complement,
                                                   self.frame_syntaxon_data_add, self.obs_hab_action_buttons))
        WidgetService.clickable(self.labrel).connect(
            lambda: WidgetService.fshow(self.rel_type_rel_id, self.rel_protocole_id, self.frel, self.fobs,
                                        self.save_rel, self.activate))
        WidgetService.clickable(self.labobs).connect(
            lambda: WidgetService.fshow(self.rel_type_rel_id, self.rel_protocole_id, self.frel, self.fobs,
                                        self.save_rel, self.activate))
        WidgetService.clickable(self.save_rel).connect(
            lambda: self.data_manager.save_survey(self.releves, self.survey_feature, self.obs_habitats))
        WidgetService.clickable(self.activate).connect(
            lambda: self.data_manager.activate_survey(self.frel, self.fobs, self.save_rel, self.activate,
                                                      self.releves, self.survey_feature, self.obs_habitats))
        WidgetService.clickable(self.add_taxon).connect(
            lambda: self.data_manager.add_taxon(self.survey_feature, self.observations, self.select_taxon,
                                                self.taxon_cite, self.taxon_cd_nom_cite, strates, self.table_taxons,
                                                self.add_taxon, self.save_taxon))
        WidgetService.clickable(self.save_taxon).connect(
            lambda: self.data_manager.save_taxon(self.survey_feature, self.observations, self.select_taxon,
                                                 self.taxon_cite, self.taxon_cd_nom_cite, strates, self.table_taxons,
                                                 self.add_taxon, self.save_taxon))
        WidgetService.clickable(self.add_syntaxon).connect(
            lambda: self.data_manager.add_habitat(self.survey_feature, self.obs_habitats, self.select_habitat,
                                                  self.obs_hab_nom_cite, self.obs_hab_code_cite,
                                                  self.obs_hab_recouvrement, self.obs_hab_forme, self.table_habitats,
                                                  self.obs_hab_assoc_list, self.add_syntaxon, self.save_syntaxon,
                                                  self.habitat_buttons))
        WidgetService.clickable(self.save_syntaxon).connect(
            lambda: self.data_manager.save_habitat(self.survey_feature, self.obs_habitats, self.select_habitat,
                                                   self.obs_hab_nom_cite, self.obs_hab_code_cite,
                                                   self.obs_hab_recouvrement, self.obs_hab_forme, self.table_habitats,
                                                   self.obs_hab_assoc_list, self.add_syntaxon, self.save_syntaxon,
                                                   add_survey_to_syntaxon_widgets, self.habitat_buttons))
        WidgetService.clickable(self.cal1_label).connect(
            lambda: WidgetService.calendar(self.cal1, self.cal1_label, self.rel_date_min, self.rel_date_max,
                                           WidgetService.transform_date_1))
        WidgetService.clickable(self.cal2_label).connect(
            lambda: WidgetService.calendar(self.cal2, self.cal2_label, self.rel_date_min, self.rel_date_max,
                                           WidgetService.transform_date_2))
        WidgetService.clickable(self.rel_create_new_polygon_rel).connect(
            lambda: self.data_manager.add_associated_survey(self.rel_create_new_polygon_rel, self.releves,
                                                            self.survey_feature))
        WidgetService.clickable(self.rel_create_new_point_rel).connect(
            lambda: self.data_manager.add_associated_survey(self.rel_create_new_point_rel, self.releves,
                                                            self.survey_feature))
        WidgetService.clickable(self.rel_create_new_line_rel).connect(
            lambda: self.data_manager.add_associated_survey(self.rel_create_new_line_rel, self.releves,
                                                            self.survey_feature))
        WidgetService.clickable(self.rel_syntax_create_new_polygon_rel).connect(
            lambda: self.data_manager.add_survey_associated_to_syntaxon(self.rel_syntax_create_new_polygon_rel,
                                                                        self.releves, self.obs_habitats,
                                                                        self.survey_feature,
                                                                        self.obs_hab_nom_cite,
                                                                        self.obs_hab_code_cite))
        WidgetService.clickable(self.rel_syntax_create_new_point_rel).connect(
            lambda: self.data_manager.add_survey_associated_to_syntaxon(self.rel_syntax_create_new_point_rel,
                                                                        self.releves, self.obs_habitats,
                                                                        self.survey_feature,
                                                                        self.obs_hab_nom_cite,
                                                                        self.obs_hab_code_cite))
        WidgetService.clickable(self.rel_syntax_create_new_line_rel).connect(
            lambda: self.data_manager.add_survey_associated_to_syntaxon(self.rel_syntax_create_new_line_rel,
                                                                        self.releves, self.obs_habitats,
                                                                        self.survey_feature,
                                                                        self.obs_hab_nom_cite,
                                                                        self.obs_hab_code_cite))
        WidgetService.clickable(self.add_obs_btn).connect(
            lambda: self.data_manager.add_observer(self.survey_feature.id(), self.liens_releve_observateur,
                                                   self.lst_obs, self.observer_list))
        WidgetService.clickable(self.suppr_obs_btn).connect(
            lambda: self.data_manager.delete_observer(self.survey_feature.id(), self.liens_releve_observateur,
                                                      self.observer_list, self.observateurs))
        WidgetService.clickable(self.add_strate_struct).connect(
            lambda: self.data_manager.add_strate(self.survey_feature.id(), self.liens_releve_strate,
                                                 self.table_strates_struct, self.select_strate_struct,
                                                 self.struct_rec_pct_strate, self.struct_haut_mod_strate,
                                                 self.struct_haut_min_strate, self.struct_haut_max_strate))
        self.select_taxon.lineEdit().returnPressed.connect(
            lambda: self.data_manager.taxon_auto_complete(self.select_taxon, self.taxref, self.taxon_cite,
                                                          self.taxon_cd_nom_cite, self.taxon_valide,
                                                          self.taxon_cd_nom_valide))
        self.taxon_dominant_recherche.lineEdit().returnPressed.connect(
            lambda: self.data_manager.taxon_auto_complete(self.taxon_dominant_recherche, self.taxref,
                                                          self.taxon_dominant_nom, self.taxon_dominant_code,
                                                          QLineEdit(), QLineEdit()))
        self.select_taxon_dominant.lineEdit().returnPressed.connect(
            lambda: self.data_manager.taxon_auto_complete(self.select_taxon_dominant, self.taxref,
                                                          self.obs_hab_taxon_dominant_nom,
                                                          self.obs_hab_taxon_dominant_code, QLineEdit(), QLineEdit()))
        self.select_habitat.lineEdit().returnPressed.connect(
            lambda: self.data_manager.habitat_auto_complete(self.select_habitat, self.catalogue_phyto, self.habref,
                                                            self.habitat_buttons, self.obs_hab_nom_cite,
                                                            self.obs_hab_code_cite, self.obs_hab_nom_retenu,
                                                            self.obs_hab_code_retenu))
        self.habitat_recherche.lineEdit().returnPressed.connect(
            lambda: self.data_manager.habitat_auto_complete(self.habitat_recherche, self.catalogue_phyto, self.habref,
                                                            self.typo_habitat_buttons, QLineEdit(), QLineEdit(),
                                                            QLineEdit(), QLineEdit(), self.survey_feature,
                                                            self.obs_habitats, self.table_syn_habitats))
        self.syntaxon_recherche.lineEdit().returnPressed.connect(
            lambda: self.data_manager.syntaxon_auto_complete(self.syntaxon_recherche, self.catalogue_phyto,
                                                             self.syntaxon_cite, self.syntaxon_code_cite,
                                                             self.syntaxon_retenu, self.syntaxon_code_retenu))

    def prepare_startup_interface(self, add_survey_to_syntaxon_widgets):
        widgets_to_hide = [self.save_taxon, self.save_syntaxon, self.syntaxon_code_cite, self.syntaxon_code_retenu,
                           self.taxon_cd_nom_cite, self.taxon_cd_nom_valide, self.rel_create_new_point_rel,
                           self.rel_create_new_line_rel, self.rel_create_new_polygon_rel, self.obs_hab_code_typo,
                           self.frame_jdd]
        widgets_to_hide.extend(add_survey_to_syntaxon_widgets)
        for widget in widgets_to_hide:
            widget.hide()

        self.frel.hide()
        self.tab_widget.setCurrentIndex(0)
        self.form.hideButtonBox()
        if self.rel_type_rel_id.itemData(self.rel_type_rel_id.currentIndex()) != 0 and self.rel_protocole_id.itemData(
                self.rel_protocole_id.currentIndex()) != 0:
            self.activate.hide()

        WidgetService.fshow(self.rel_type_rel_id, self.rel_protocole_id, self.frel, self.fobs, self.save_rel,
                            self.activate)
        WidgetService.fobs_show(self.rel_type_rel_id, self.rel_protocole_id, self.fobs_taxons, self.fobs_syntaxons,
                                self.ftypo_taxons, self.ftypo_syntaxons, self.save_rel, self.lab_alerte_structure,
                                self.structure, self.lab_alerte_desc_gen, self.desc_gen)
