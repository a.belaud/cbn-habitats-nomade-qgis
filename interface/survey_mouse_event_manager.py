from PyQt5.QtCore import QObject, pyqtSignal, QEvent


class MouseEventManager(QObject):
    clicked = pyqtSignal()

    def __init__(self, widget: QObject):
        super(MouseEventManager, self).__init__(widget)
        self.widget = widget

    def eventFilter(self, obj, event) -> bool:
        if obj == self.widget:
            if event.type() == QEvent.MouseButtonRelease:
                if obj.rect().contains(event.pos()):
                    self.clicked.emit()
                    return True
        return False
