from typing import List, Optional

from PyQt5.QtCore import QObject, pyqtSignal, Qt
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QButtonGroup, QListWidget, QListWidgetItem
from PyQt5.QtWidgets import QTableWidget, QLineEdit, QPlainTextEdit, QComboBox, QLabel, QTextEdit, QCheckBox
from qgis.core import QgsFeature, QgsVectorLayer, QgsProject
from qgis.gui import QgsAttributeForm

from .survey_mouse_event_manager import MouseEventManager
from .survey_widget_service import WidgetService
from ..constants.header_labels_values import CORINE_HEADER_LABELS
from ..constants.header_labels_values import SYNTAXONS_HEADER_LABELS, HIC_HEADER_LABELS, EUNIS_HEADER_LABELS
from ..data.database_specification_service import DatabaseSpecificationService
from ..data.sqlite_data_manager import DataManager


class TableWidgetService:
    @staticmethod
    def clickable(widget: QObject) -> pyqtSignal():
        mouse_event = MouseEventManager(widget)
        widget.installEventFilter(mouse_event)
        return mouse_event.clicked

    @staticmethod
    def edit_syntaxon(survey_feature: QgsFeature, row: int, observations: QgsVectorLayer, sheet: QTableWidget,
                      form: QgsAttributeForm, add_widget: QLabel, save_widget: QLabel,
                      add_survey_widgets: List[QLabel], select_widget: QComboBox, typo_buttons: QButtonGroup):
        code_phyto = sheet.item(row, 0).text() if sheet.item(row, 0) and sheet.item(row, 0).text() != '' else None
        TableWidgetService.edit_observation(survey_feature, observations, form, add_widget, save_widget,
                                            add_survey_widgets, select_widget, typo_buttons, 'obs_habitat_',
                                            'obs_hab_rel_id', 'obs_hab_code_cite', 'obs_hab_nom_cite', code_phyto,
                                            sheet.item(row, 1).text())

    @staticmethod
    def edit_taxon(survey_feature: QgsFeature, row: int, observations: QgsVectorLayer, sheet: QTableWidget,
                   form: QgsAttributeForm, add_widget: QLabel, save_widget: QLabel, add_survey_widgets: List[QLabel],
                   select_widget: QComboBox, typo_buttons: QButtonGroup):
        cd_nom = int(sheet.item(row, 0).text()) if sheet.item(row, 0) and sheet.item(row, 0).text() != '' else None
        TableWidgetService.edit_observation(survey_feature, observations, form, add_widget, save_widget,
                                            add_survey_widgets, select_widget, typo_buttons, 'nom_', 'obs_rel_id',
                                            'cd_nom', 'nom_cite', cd_nom, sheet.item(row, 1).text())

    @staticmethod
    def edit_observation(survey_feature: QgsFeature, observations: QgsVectorLayer, form: QgsAttributeForm,
                         add_widget: QLabel, save_widget: QLabel, add_survey_widgets: List[QLabel],
                         select_widget: QComboBox, typo_buttons: QButtonGroup, keyword: str, obs_rel_field_name: str,
                         obs_code_field_name: str, obs_name_field_name: str, id_to_compare: object,
                         text_to_compare: object):
        form_inputs = DatabaseSpecificationService.get_inputs_specification(observations.name())
        if len(form_inputs) == 0:
            return
        observation = TableWidgetService.find_observation(id_to_compare, text_to_compare, obs_code_field_name,
                                                          obs_name_field_name, obs_rel_field_name, observations,
                                                          survey_feature)
        if observation:
            TableWidgetService.hydrate_form(form, form_inputs, keyword, observation)
            if observations.name() == 'obs_habitats':
                TableWidgetService.populate_obs_assoc_list(survey_feature, observation, observations, form,
                                                           typo_buttons)

            for widget in add_survey_widgets:
                widget.show()
            for button in typo_buttons.buttons():
                button.setEnabled(False)
            select_widget.setEnabled(False)
            add_widget.hide()
            save_widget.show()

    @staticmethod
    def hydrate_form(form: QgsAttributeForm, form_inputs: List[dict], keyword: str, observation: QgsFeature):
        for form_input in form_inputs:
            widget = WidgetService.find_widget(form, form_input['input_type'],
                                               form_input['input_name'].split(',')[0])
            if widget:
                TableWidgetService.clean_widget(widget)
                field_names = [form_input['field_name'] for form_input in form_inputs
                               if form_input['input_name'].split(',')[0] == widget.objectName()]
                if len(field_names) > 0 and observation:
                    WidgetService.set_input_value(widget, observation[field_names[0]])
                if keyword in widget.objectName():
                    widget.setEnabled(False)

    @staticmethod
    def populate_obs_assoc_list(survey_feature: QgsFeature, observation: QgsFeature, observations: QgsVectorLayer,
                                form: QgsAttributeForm, typo_buttons: QButtonGroup):
        obs_hab_list_widget: QListWidget = form.findChild(QListWidget, 'list_hab_obs_assoc')
        try:
            obs_hab_list_widget.itemChanged.disconnect()
        except:
            pass
        obs_hab_list_widget.clear()
        if obs_hab_list_widget is not None:
            code_typo = TableWidgetService.get_code_typo(typo_buttons)
            consulted_obs_hab_id = observation['obs_hab_id']
            obs_hab_list = [observation for observation in observations.getFeatures()
                            if observation['obs_hab_rel_id'] == survey_feature.id()
                            and observation['obs_hab_code_typo'] != code_typo]
            sqlite_data_manager = DataManager(QgsProject.instance())
            for obs_hab in obs_hab_list:
                row = obs_hab_list_widget.count()
                obs_hab_typo = obs_hab['obs_hab_code_typo']
                obs_hab_id = obs_hab['obs_hab_id']
                label = TableWidgetService.get_typo_label_from_code(obs_hab_typo) + ' - '
                label += obs_hab['obs_hab_code_cite'] + ' - ' + obs_hab['obs_hab_nom_cite']
                is_linked = sqlite_data_manager.are_obs_hab_linked(obs_hab_id, consulted_obs_hab_id)
                obs_hab_item = QListWidgetItem()
                obs_hab_item.setText(label)
                obs_hab_item.setData(Qt.UserRole, obs_hab_id)
                obs_hab_item.setBackground(TableWidgetService.get_typo_color_from_code(obs_hab['obs_hab_code_typo']))
                obs_hab_item.setCheckState(Qt.Checked if is_linked else Qt.Unchecked)
                obs_hab_list_widget.insertItem(row, obs_hab_item)
            obs_hab_list_widget.sortItems(Qt.DescendingOrder)
            obs_hab_list_widget.itemChanged.connect(
                lambda item: TableWidgetService.manage_association_between_obs_hab(item, obs_hab_list_widget,
                                                                                   observation, observations))

    @staticmethod
    def manage_association_between_obs_hab(list_item: QListWidgetItem, obs_hab_list_widget: QListWidget,
                                           obs_hab: QgsFeature, observations: QgsVectorLayer):
        obs_hab_list_widget.itemChanged.disconnect()
        obs_hab_id = obs_hab['obs_hab_id']
        obs_hab_id_to_link = list_item.data(Qt.UserRole)
        obs_hab_to_link = observations.getFeature(obs_hab_id_to_link)
        sqlite_data_manager = DataManager(QgsProject.instance())
        if list_item.checkState() == 2:
            sqlite_data_manager.insert_links_between_obs_hab(obs_hab_id, list_item.data(Qt.UserRole))
        else:
            sqlite_data_manager.remove_links_between_obs_hab(obs_hab_id, list_item.data(Qt.UserRole))
            if obs_hab['obs_hab_code_typo'] == 0:
                observations.startEditing()
                obs_hab_to_link['obs_hab_rel_assoc_id'] = None
                observations.updateFeature(obs_hab_to_link)
                observations.commitChanges()
            elif obs_hab_to_link['obs_hab_code_typo'] == 0:
                observations.startEditing()
                obs_hab['obs_hab_rel_assoc_id'] = None
                observations.updateFeature(obs_hab)
                observations.commitChanges()
        obs_hab_list_widget.itemChanged.connect(
            lambda item: TableWidgetService.manage_association_between_obs_hab(item, obs_hab_list_widget,
                                                                               obs_hab, observations))

    @staticmethod
    def del_row(survey_feature: QgsFeature, row: int, observations: QgsVectorLayer, table: QTableWidget,
                obs_rel_field_name: str, obs_code_field_name: str, obs_name_field_name: str, id_to_compare: object,
                text_to_compare: object):
        observation = TableWidgetService.find_observation(id_to_compare, text_to_compare, obs_code_field_name,
                                                          obs_name_field_name, obs_rel_field_name, observations,
                                                          survey_feature)
        if observation:
            observations.deleteFeature(observation.id())
            observations.commitChanges()
            table.removeRow(row)

    @staticmethod
    def find_observation(id_to_compare, text_to_compare, obs_code_field_name, obs_name_field_name, obs_rel_field_name,
                         observations, survey_feature) -> Optional[QgsFeature]:
        survey_id = survey_feature['id']
        if survey_id is None:
            sqlite_data_manager = DataManager(QgsProject.instance())
            survey_id = sqlite_data_manager.get_survey_id_from_geometry(survey_feature)
        observation_result: List[QgsFeature] = [observation for observation in observations.getFeatures()
                                                if observation[obs_rel_field_name] == survey_id
                                                and observation[obs_code_field_name] == id_to_compare
                                                and observation[obs_name_field_name] == text_to_compare]
        if len(observation_result) == 0:
            return None
        else:
            if not observations.isEditable():
                observations.startEditing()
            return observation_result[0]

    @staticmethod
    def clean_widget(widget):
        if isinstance(widget, QLineEdit):
            widget.setText("")
        elif isinstance(widget, QPlainTextEdit) or isinstance(widget, QTextEdit):
            widget.setPlainText("")
        elif isinstance(widget, QComboBox):
            widget.setCurrentIndex(0)
        elif isinstance(widget, QCheckBox):
            widget.setCheckState(Qt.Unchecked)

    @staticmethod
    def get_code_typo(buttons: QButtonGroup) -> int:
        button_name = buttons.checkedButton().objectName()
        if button_name == "rb_phyto":
            return 0
        elif button_name == "rb_hic" or button_name == "rb_typo_hic":
            return 4
        elif button_name == "rb_eunis" or button_name == "rb_typo_eunis":
            return 7
        elif button_name == "rb_cb" or button_name == "rb_typo_cb":
            return 22
        return 0

    @staticmethod
    def get_header_labels_from_typo(code_typo: int) -> List:
        if code_typo == 0:
            return SYNTAXONS_HEADER_LABELS
        elif code_typo == 4:
            return HIC_HEADER_LABELS
        elif code_typo == 7:
            return EUNIS_HEADER_LABELS
        elif code_typo == 22:
            return CORINE_HEADER_LABELS
        return []

    @staticmethod
    def get_typo_label_from_code(code_typo: int) -> str:
        if code_typo == 0:
            return "Phyto"
        elif code_typo == 4:
            return "HIC"
        elif code_typo == 7:
            return "EUNIS"
        elif code_typo == 22:
            return "Corine"
        return ""

    @staticmethod
    def get_typo_color_from_code(code_typo: int) -> QColor:
        if code_typo == 0:
            return QColor(Qt.white)
        elif code_typo == 4:
            return QColor(127, 201, 255)
        elif code_typo == 7:
            return QColor(255, 178, 127)
        elif code_typo == 22:
            return QColor(153, 255, 137)
        return QColor(Qt.white)

    @staticmethod
    def do_nothing():
        pass

    @staticmethod
    def add_new_survey(self):
        pass
