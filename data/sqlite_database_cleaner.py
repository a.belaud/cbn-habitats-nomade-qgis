from sqlite3 import Connection, Cursor


class DatabaseCleaner:
    def __init__(self, connection: Connection):
        self.connection = connection
        self.cursor: Cursor = self.connection.cursor()
        self.geometry_tables = [('station_point', 'point'), ('station_ligne', 'ligne'), ('station_surface', 'polygon')]

    def clean_orphans(self):
        self.clean_survey_orphans()
        self.clean_observations_orphans()
        self.clean_obs_syntaxon_orphans()

    def clean_survey_orphans(self):
        orphans_request = "SELECT releves.id FROM releves LEFT JOIN "
        join_clause = []
        for geometry_table in self.geometry_tables:
            join_clause.append(geometry_table[0] + " on " + geometry_table[0] + ".uuid = releves.rel_" +
                               geometry_table[1] + "_uuid ")
        orphans_request += "LEFT JOIN ".join(join_clause) + "WHERE "
        where_clause = []
        for geometry_table in self.geometry_tables:
            where_clause.append(geometry_table[0] + ".uuid is NULL")
        orphans_request += " AND ".join(where_clause)
        self.cursor.execute(orphans_request)
        orphans = [orphan[0] for orphan in self.cursor.fetchall()]

        if len(orphans) > 0:
            delete_request: str
            if len(orphans) == 1:
                delete_request = "DELETE FROM releves WHERE id = " + str(orphans[0])
            else:
                delete_request = "DELETE FROM releves WHERE id in " + str(tuple(orphans))
            self.cursor.execute(delete_request)
            self.connection.commit()

    def clean_observations_orphans(self):
        request = "SELECT observations.obs_id FROM observations LEFT JOIN releves on releves.id = " \
                  "observations.obs_rel_id WHERE releves.id is NULL"
        self.cursor.execute(request)
        orphans = [orphan[0] for orphan in self.cursor.fetchall()]

        if len(orphans) > 0:
            delete_request: str
            if len(orphans) == 1:
                delete_request = "DELETE FROM observations WHERE obs_id = " + str(orphans[0])
            else:
                delete_request = "DELETE FROM observations WHERE obs_id in " + str(tuple(orphans))
            self.cursor.execute(delete_request)
            self.connection.commit()

    def clean_obs_syntaxon_orphans(self):
        request = "SELECT obs_syntaxons.syntaxon_id FROM obs_syntaxons LEFT JOIN releves on releves.id = " \
                  "obs_syntaxons.obs_hab_rel_id WHERE releves.id is NULL"
        self.cursor.execute(request)
        orphans = [orphan[0] for orphan in self.cursor.fetchall()]

        if len(orphans) > 0:
            delete_request: str
            if len(orphans) == 1:
                delete_request = "DELETE FROM obs_syntaxons WHERE syntaxon_id = " + str(orphans[0])
            else:
                delete_request = "DELETE FROM obs_syntaxons WHERE syntaxon_id in " + str(tuple(orphans))
            self.cursor.execute(delete_request)
            self.connection.commit()
