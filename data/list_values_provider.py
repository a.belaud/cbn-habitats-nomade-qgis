from typing import List

from qgis.core import QgsProject

from ..data.sqlite_data_manager import DataManager


class ListValuesProvider:
    def __init__(self, project: QgsProject):
        self.data_manager = DataManager(project)

    def get_values_from_list_name(self, list_name: str) -> List[List]:
        result_list = self.data_manager.get_list_values_from_list_name(list_name)
        returned_list = [[value[0], value[1], value[2]] for value in result_list]
        return returned_list
