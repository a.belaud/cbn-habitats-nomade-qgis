from typing import List, Optional

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QFont, QBrush, QColor
from PyQt5.QtWidgets import QComboBox, QTableWidgetItem, QTableWidget, QLineEdit, QFrame, QListWidget, QButtonGroup
from PyQt5.QtWidgets import QLabel, QGroupBox
from qgis.PyQt.QtCore import NULL
from qgis.core import QgsFeature, QgsVectorLayer, QgsProject
from qgis.gui import QgsAttributeForm
from qgis.utils import iface

from .database_specification_service import DatabaseSpecificationService
from .list_values_provider import ListValuesProvider
from .sqlite_data_manager import DataManager
from ..interface.survey_widget_service import WidgetService
from ..interface.table_widget_service import TableWidgetService


class SurveyDataManager:
    def __init__(self, project: QgsProject, form: QgsAttributeForm, layer: QgsVectorLayer, feature: QgsFeature):
        self.project = project
        self.form: QgsAttributeForm = form
        self.layer: QgsVectorLayer = layer
        self.geometry_feature: QgsFeature = feature
        self.list_values_provider: ListValuesProvider = ListValuesProvider(self.project)
        self.sqlite_data_manager: DataManager = DataManager(self.project)

    def habitat_auto_complete(self, select_habitat: QComboBox, catalogue_phyto: QgsVectorLayer, habref: QgsVectorLayer,
                              buttons: QButtonGroup, habitat_cite: QLineEdit, habitat_code_cite: QLineEdit,
                              syntaxon_valide: QLineEdit, syntaxon_code_valide: QLineEdit,
                              survey_feature: QgsFeature = None, obs_habitats: QgsVectorLayer = None,
                              table_syn_habitats: QTableWidget = None):
        code_typo: int = TableWidgetService.get_code_typo(buttons)
        ref_habitat_values: List
        nom_habitat_simple_index: int
        code_habitat_index: int
        code_habitat_retenu_index: int
        ref_phyto: bool

        if code_typo == 0:
            ref_habitat_values = [value.attributes() for value in catalogue_phyto.getFeatures()]
            ref_habitat_field_names = [f.name() for f in catalogue_phyto.fields()]
            nom_habitat_simple_index = ref_habitat_field_names.index('nom_syntaxon_simple')
            code_habitat_index = ref_habitat_field_names.index('code_phyto')
            code_habitat_retenu_index = ref_habitat_field_names.index('code_phyto_syntaxon_retenu')
            ref_phyto = True
        else:
            ref_habitat_values = [value.attributes() for value in habref.getFeatures() if value['cd_typo'] == code_typo]
            ref_habitat_field_names = [f.name() for f in habref.fields()]
            nom_habitat_simple_index = ref_habitat_field_names.index('lb_hab_fr')
            code_habitat_index = ref_habitat_field_names.index('lb_code')
            code_habitat_retenu_index = 0
            ref_phyto = False

        self.generate_habitat_auto_complete(select_habitat, ref_habitat_values, nom_habitat_simple_index,
                                            code_habitat_index, code_habitat_retenu_index, ref_phyto, code_typo,
                                            habitat_cite, habitat_code_cite, syntaxon_valide, syntaxon_code_valide,
                                            survey_feature, obs_habitats, table_syn_habitats)

    def syntaxon_auto_complete(self, select_habitat: QComboBox, catalogue_phyto: QgsVectorLayer,
                               syntaxon_cite: QLineEdit, syntaxon_code_cite: QLineEdit, syntaxon_valide: QLineEdit,
                               syntaxon_code_valide: QLineEdit):
        ref_habitat_values = [c.attributes() for c in catalogue_phyto.getFeatures()]
        syntaxon_field_names = [f.name() for f in catalogue_phyto.fields()]
        nom_habitat_simple_index = syntaxon_field_names.index('nom_syntaxon_simple')
        code_habitat_index = syntaxon_field_names.index('code_phyto')
        code_habitat_retenu_index = syntaxon_field_names.index('code_phyto_syntaxon_retenu')

        self.generate_habitat_auto_complete(select_habitat, ref_habitat_values, nom_habitat_simple_index,
                                            code_habitat_index, code_habitat_retenu_index, True, 0, syntaxon_cite,
                                            syntaxon_code_cite, syntaxon_valide, syntaxon_code_valide)

    def generate_habitat_auto_complete(self, select_habitat, ref_habitat_values, nom_habitat_simple_index,
                                       code_habitat_index, code_habitat_retenu_index, ref_phyto, code_typo,
                                       habitat_cite, habitat_code_cite, syntaxon_valide, syntaxon_code_valide,
                                       survey_feature: QgsFeature = None, obs_habitats: QgsVectorLayer = None,
                                       table_syn_habitats: QTableWidget = None):
        text_scripted = select_habitat.lineEdit().text()
        select_habitat.hidePopup()
        text_chunks = text_scripted.split(' ')
        found_habitats = self.filter_habitat_list(code_typo, code_habitat_index, nom_habitat_simple_index,
                                                  ref_habitat_values, text_chunks)

        if found_habitats and len(found_habitats) > 0:
            self.populate_select_habitat(code_habitat_index, code_habitat_retenu_index, found_habitats,
                                         nom_habitat_simple_index, ref_phyto, select_habitat)
            select_object_name = select_habitat.objectName()
            if select_object_name == 'select_habitat' or select_object_name == 'syntaxon_recherche':
                select_habitat.view().pressed.connect(
                    lambda line: self.add_validated_habitat(line, select_habitat, found_habitats, habitat_cite,
                                                            habitat_code_cite, syntaxon_valide, syntaxon_code_valide,
                                                            ref_phyto))
            elif select_object_name == 'habitat_recherche':
                select_habitat.view().pressed.connect(
                    lambda line: self.add_typo_habitats(line, select_habitat, survey_feature, found_habitats,
                                                        obs_habitats, table_syn_habitats, code_typo))
        if len(text_scripted) == 0:
            select_habitat.clear()

    @staticmethod
    def filter_habitat_list(code_typo, code_habitat_index, nom_habitat_simple_index, ref_habitat_values, text_chunks):
        found_habitats = None
        if code_typo == 0:
            if len(text_chunks) == 1 and len(text_chunks[0]) > 2:
                found_habitats = [habitat for habitat in ref_habitat_values if
                                  text_chunks[0].lower() == habitat[nom_habitat_simple_index][
                                                            :len(text_chunks[0])].lower()]
            elif len(text_chunks) > 1 and len(text_chunks[0]) > 2:
                found_habitats = [habitat for habitat in ref_habitat_values if
                                  text_chunks[0].lower() == habitat[nom_habitat_simple_index][
                                                            :len(text_chunks[0])].lower() and
                                  text_chunks[1].lower() in habitat[nom_habitat_simple_index][
                                                            len(text_chunks[0]):].lower()]
        else:
            if len(text_chunks) > 0 and len(text_chunks[0]) > 0:
                found_habitats = [habitat for habitat in ref_habitat_values if
                                  text_chunks[0].lower() == habitat[code_habitat_index][
                                                            :len(text_chunks[0])].lower()]
        return found_habitats

    @staticmethod
    def populate_select_habitat(code_habitat_index, code_habitat_retenu_index, found_habitats,
                                nom_habitat_simple_index, ref_phyto, select_habitat):
        select_habitat.clear()
        item_model = QStandardItemModel()
        for habitat in found_habitats:
            item = QStandardItem()
            if ref_phyto and habitat[code_habitat_index] == habitat[code_habitat_retenu_index]:
                item.setText(str(habitat[code_habitat_index]) + ' | ' + habitat[nom_habitat_simple_index])
                item.setFont(QFont('Tahoma', 8, QFont.Bold))
                item.setData(habitat[code_habitat_index])
                item_model.appendRow(item)
            else:
                item.setText(str(habitat[code_habitat_index]) + ' | ' + habitat[nom_habitat_simple_index])
                item.setData(habitat[code_habitat_index])
                item_model.appendRow(item)
        select_habitat.setModel(item_model)
        select_habitat.showPopup()

    @staticmethod
    def add_validated_habitat(line, select_habitat: QComboBox, habitats: List[QgsFeature], habitat_cite: QLineEdit,
                              habitat_code_cite: QLineEdit, syntaxon_valide: QLineEdit,
                              syntaxon_code_valide: QLineEdit, ref_phyto: bool):
        select_habitat.view().pressed.disconnect()
        habitat = habitats[line.row()]
        if ref_phyto:
            habitat_cite.setText(str(habitat[4] if habitat[4] else ''))
            habitat_code_cite.setText(str(habitat[2] if habitat[2] else ''))
            syntaxon_valide.setText(str(habitat[7] if habitat[7] else ''))
            syntaxon_code_valide.setText(str(habitat[6] if habitat[6] else ''))
        else:
            habitat_cite.setText(str(habitat[4] if habitat[4] else ''))
            habitat_code_cite.setText(str(habitat[3] if habitat[3] else ''))
            syntaxon_valide.setText('')
            syntaxon_code_valide.setText('')
        select_habitat.hidePopup()
        select_habitat.clear()

    def add_habitat(self, survey_feature: QgsFeature, obs_habitats: QgsVectorLayer, select_habitat: QComboBox,
                    obs_hab_nom_cite: QLineEdit, obs_hab_code_cite: QLineEdit, syntaxon_rec: QComboBox,
                    syntaxon_forme: QComboBox, table_habitats: QTableWidget, obs_hab_assoc_list: QListWidget,
                    add_widget: QLabel, save_widget: QLabel, typo_buttons: QButtonGroup):
        self.save_and_add_habitat(add_widget, None, obs_habitats, save_widget, select_habitat, survey_feature,
                                  obs_hab_code_cite, syntaxon_forme, obs_hab_nom_cite, syntaxon_rec,
                                  table_habitats, obs_hab_assoc_list, typo_buttons)

    def save_habitat(self, survey_feature: QgsFeature, obs_habitats: QgsVectorLayer, select_habitat: QComboBox,
                     obs_hab_nom_cite: QLineEdit, obs_hab_code_cite: QLineEdit, syntaxon_rec: QComboBox,
                     syntaxon_forme: QComboBox, table_habitats: QTableWidget, obs_hab_assoc_list: QListWidget,
                     add_widget: QLabel, save_widget: QLabel, add_survey_widgets: List[QLabel],
                     typo_buttons: QButtonGroup):
        code_phyto = obs_hab_code_cite.text() if obs_hab_code_cite else None
        if code_phyto:
            row = [i for i in range(table_habitats.rowCount())
                   if table_habitats.item(i, 1).text() == obs_hab_nom_cite.text()
                   and table_habitats.item(i, 0).text() == code_phyto][0]
            obs_habitat = [obs_habitat for obs_habitat in obs_habitats.getFeatures()
                           if obs_habitat['obs_hab_rel_id'] == survey_feature.id()
                           and obs_habitat['obs_hab_nom_cite'] == obs_hab_nom_cite.text()
                           and obs_habitat['obs_hab_code_cite'] == code_phyto][0]
        else:
            row = [i for i in range(table_habitats.rowCount())
                   if table_habitats.item(i, 1).text() == obs_hab_nom_cite.text()][0]
            obs_habitat = [obs_habitat for obs_habitat in obs_habitats.getFeatures()
                           if obs_habitat['obs_hab_rel_rel_id'] == survey_feature.id()
                           and obs_habitat['obs_hab_nom_cite'] == obs_hab_nom_cite.text()][0]

        success = self.save_and_add_habitat(add_widget, obs_habitat, obs_habitats, save_widget, select_habitat,
                                            survey_feature, obs_hab_code_cite, syntaxon_forme, obs_hab_nom_cite,
                                            syntaxon_rec, table_habitats, obs_hab_assoc_list, typo_buttons,
                                            add_survey_widgets)
        if success:
            table_habitats.removeRow(row)

    def save_and_add_habitat(self, add_widget, obs_habitat, obs_habitats, save_widget, select_habitat, survey_feature,
                             obs_hab_code_cite, syntaxon_forme, obs_hab_nom_cite, syntaxon_rec, table_habitats,
                             obs_hab_assoc_list, typo_buttons, add_survey_widgets=None) -> bool:
        if not self.check_if_code_habitat_exists(obs_habitat, obs_habitats, survey_feature, obs_hab_nom_cite.text(),
                                                 obs_hab_code_cite.text()):
            select_habitat.hidePopup()
            select_habitat.clear()
            return False

        if obs_hab_nom_cite.text():
            table_habitats.insertRow(table_habitats.rowCount())
            table_habitats.setItem(table_habitats.rowCount() - 1, 0,
                                   QTableWidgetItem(obs_hab_code_cite.text()))
            table_habitats.setItem(table_habitats.rowCount() - 1, 1, QTableWidgetItem(obs_hab_nom_cite.text()))
            table_habitats.setItem(table_habitats.rowCount() - 1, 2, QTableWidgetItem(syntaxon_rec.currentText()))
            table_habitats.setItem(table_habitats.rowCount() - 1, 3, QTableWidgetItem(syntaxon_forme.currentText()))
            table_habitats.setItem(table_habitats.rowCount() - 1, 6, SurveyDataManager.get_cross_item())
            self.save_habitat_observation(survey_feature, obs_habitats, 'obs_hab_rel_id', obs_habitat)
            self.refresh_habitat_sub_form(add_survey_widgets, add_widget, save_widget, select_habitat,
                                          obs_hab_assoc_list, typo_buttons)
            return True
        else:
            iface.messageBar().pushWarning('Ajout de syntaxon', 'Vous ne pouvez pas ajouter une observation sans '
                                                                'syntaxon cite.')
            return False

    def save_habitat_observation(self, survey_feature: QgsFeature, obs_habitats: QgsVectorLayer,
                                 linked_field_name: str = 'obs_hab_rel_id', obs_habitat: QgsFeature = None):
        if not obs_habitats.isEditable():
            obs_habitats.startEditing()
        if not obs_habitat:
            obs_habitat = QgsFeature()
            obs_habitat.setFields(obs_habitats.fields())
            obs_habitat[linked_field_name] = survey_feature['id']
            obs_habitats.updateFeature(obs_habitat)
            obs_habitats.addFeature(obs_habitat)
        self.save_layer_values(obs_habitat, obs_habitats)
        obs_habitats.commitChanges()

    def refresh_habitat_sub_form(self, add_survey_widgets, add_widget, save_widget, select_habitat, obs_hab_assoc_list,
                                 typo_buttons: QButtonGroup):
        select_habitat.clear()
        self.clean_habitat_sub_form()

        for button in typo_buttons.buttons():
            button.setEnabled(True)
        if obs_hab_assoc_list.count() > 0:
            obs_hab_assoc_list.itemChanged.disconnect()
            obs_hab_assoc_list.clear()

        if add_survey_widgets:
            for widget in add_survey_widgets:
                widget.hide()
        select_habitat.setEnabled(True)
        add_widget.show()
        save_widget.hide()

    def clean_habitat_sub_form(self):
        form_inputs = DatabaseSpecificationService.get_inputs_specification('obs_habitats')
        for form_input in form_inputs:
            widget = WidgetService.find_widget(self.form, form_input['input_type'],
                                               form_input['input_name'].split(',')[0])
            if widget and widget.objectName() != 'obs_hab_code_typo':
                TableWidgetService.clean_widget(widget)
                widget.setEnabled(True)

    @staticmethod
    def check_if_code_habitat_exists(obs_syntaxon, obs_habitats, survey_feature, obs_hab_nom_cite,
                                     obs_hab_code_cite, obs_hab_rel_field_name: str = 'obs_hab_rel_id') -> bool:
        observations_with_same_id = [observation for observation in obs_habitats.getFeatures()
                                     if observation[obs_hab_rel_field_name] == survey_feature.id()
                                     and observation['obs_hab_nom_cite'] == obs_hab_nom_cite
                                     and observation['obs_hab_code_cite'] == obs_hab_code_cite]
        if obs_syntaxon:
            if len(observations_with_same_id) > 0 \
                    and obs_syntaxon['obs_hab_code_cite'] != obs_hab_code_cite:
                iface.messageBar().pushWarning('Ajout d\'observation habitat',
                                               'Vous ne pouvez ajouter qu\'une observation par code habitat.')
                return False
        else:
            if len(observations_with_same_id) > 0:
                iface.messageBar().pushWarning('Ajout d\'observation habitat',
                                               'Une seule observation par code habitat est permise.')
                return False
        return True

    def get_habitats(self, feature_id: int, obs_habitats: QgsVectorLayer, table_habitats: QTableWidget,
                     obs_hab_assoc_list: QListWidget, buttons: QButtonGroup, obs_hab_code_typo: QLineEdit,
                     syntaxon_cplt: QFrame, syntaxon_data_add: QFrame, obs_hab_action_buttons: QGroupBox):
        code_typo: int = self.prepare_table_habitats(table_habitats, obs_hab_assoc_list, buttons, obs_hab_code_typo,
                                                     syntaxon_cplt, syntaxon_data_add, obs_hab_action_buttons)
        habitats = [observation for observation in obs_habitats.getFeatures()
                    if observation['obs_hab_rel_id'] == feature_id and observation['obs_hab_code_typo'] == code_typo]
        if len(habitats) > 0:
            type_forme_values = self.list_values_provider.get_values_from_list_name('TYPE_FORME_VALUES')
            strate_values = self.list_values_provider.get_values_from_list_name('STRATE_VALUES')
            for habitat in habitats:
                table_habitats.insertRow(table_habitats.rowCount())
                if habitat['obs_hab_code_cite'] not in (0, None, NULL):
                    table_habitats.setItem(table_habitats.rowCount() - 1, 0, QTableWidgetItem(
                        habitat['obs_hab_code_cite']))
                table_habitats.setItem(table_habitats.rowCount() - 1, 1, QTableWidgetItem(
                    habitat['obs_hab_nom_cite']))
                if habitat['obs_hab_forme_id'] not in (0, None, NULL):
                    table_habitats.setItem(table_habitats.rowCount() - 1, 3, QTableWidgetItem(
                        type_forme_values[habitat['obs_hab_forme_id']][1]))
                if habitat['obs_hab_recouvrement'] not in (0, None, NULL):
                    table_habitats.setItem(table_habitats.rowCount() - 1, 2,
                                           QTableWidgetItem(strate_values[habitat['obs_hab_recouvrement']][1]))
                table_habitats.setItem(table_habitats.rowCount() - 1, 6, SurveyDataManager.get_cross_item())

    def prepare_table_habitats(self, table_habitats: QTableWidget, obs_hab_assoc_list: QListWidget,
                               buttons: QButtonGroup, obs_hab_code_typo: QLineEdit, syntaxon_cplt: QFrame,
                               syntaxon_data_add: QFrame, obs_hab_action_buttons: QGroupBox) -> int:
        code_typo: int = TableWidgetService.get_code_typo(buttons)
        header_labels: List = TableWidgetService.get_header_labels_from_typo(code_typo)
        obs_hab_code_typo.setText(str(code_typo))
        widgets_to_display_or_not = [syntaxon_cplt, syntaxon_data_add, obs_hab_action_buttons]

        self.clean_habitat_sub_form()
        if code_typo == 0:
            for widget in widgets_to_display_or_not:
                widget.show()
        else:
            for widget in widgets_to_display_or_not:
                widget.hide()

        obs_hab_assoc_list.clear()
        table_habitats.clear()
        for i in range(table_habitats.rowCount()):
            table_habitats.removeRow(0)
        WidgetService.build_table(table_habitats, header_labels)
        return code_typo

    @staticmethod
    def get_syntaxon(form: QgsAttributeForm, obs_syntaxon_id: int, obs_habitats: QgsVectorLayer):
        obs_syntaxon_list = [observation for observation in obs_habitats.getFeatures()
                             if observation.id() == obs_syntaxon_id]
        if len(obs_syntaxon_list) > 0:
            obs_syntaxon = obs_syntaxon_list[0]
            form_inputs = DatabaseSpecificationService.get_inputs_specification('obs_habitats')
            for form_input in form_inputs:
                for form_input_name in form_input['input_name'].split(','):
                    widget = WidgetService.find_widget(form, form_input['input_type'], form_input_name.strip())
                    WidgetService.set_input_value(widget, obs_syntaxon[form_input['field_name']])

    def get_typo_habitats(self, survey_feature: QgsFeature, obs_habitats: QgsVectorLayer,
                          table_syn_habitats: QTableWidget):
        obs_syntaxon_id = survey_feature['rel_obs_syntaxon_id']
        linked_obs_hab_list = [row[0] for row in self.sqlite_data_manager.get_linked_obs_hab(obs_syntaxon_id)]
        associated_obs_habitats = obs_habitats.getFeatures(linked_obs_hab_list)
        for observation in associated_obs_habitats:
            row = table_syn_habitats.rowCount()
            code_typo = observation['obs_hab_code_typo']
            typo_widget = QTableWidgetItem(TableWidgetService.get_typo_label_from_code(code_typo))
            typo_widget.setBackground(TableWidgetService.get_typo_color_from_code(code_typo))
            table_syn_habitats.insertRow(row)
            table_syn_habitats.setItem(row, 0, QTableWidgetItem(observation['obs_hab_code_cite']))
            table_syn_habitats.setItem(row, 1, QTableWidgetItem(observation['obs_hab_nom_cite']))
            table_syn_habitats.setItem(row, 2, QTableWidgetItem(str(code_typo)))
            table_syn_habitats.setItem(row, 3, typo_widget)
            table_syn_habitats.setItem(row, 4, SurveyDataManager.get_cross_item())
        table_syn_habitats.sortItems(0, Qt.AscendingOrder)
        table_syn_habitats.sortItems(3, Qt.DescendingOrder)

    def add_typo_habitats(self, line, select_habitat: QComboBox, survey_feature: QgsFeature,
                          found_habitats: List[QgsFeature], obs_habitats: QgsVectorLayer,
                          table_syn_habitats: QTableWidget, code_typo: int):
        habitat = found_habitats[line.row()]
        obs_hab_nom_cite = str(habitat[4] if habitat[4] else '')
        obs_hab_code_cite = str(habitat[3] if habitat[3] else '')

        if not self.check_if_code_habitat_exists(None, obs_habitats, survey_feature, obs_hab_nom_cite,
                                                 obs_hab_code_cite, 'obs_hab_rel_assoc_id'):
            select_habitat.hidePopup()
            select_habitat.clear()
            return

        self.add_habitat_to_database(code_typo, obs_habitats, survey_feature, obs_hab_nom_cite, obs_hab_code_cite)
        self.add_habitat_to_table_widget(code_typo, table_syn_habitats, obs_hab_nom_cite, obs_hab_code_cite)

        obs_habitat = TableWidgetService.find_observation(obs_hab_code_cite, obs_hab_nom_cite, 'obs_hab_code_cite',
                                                          'obs_hab_nom_cite', 'obs_hab_rel_assoc_id', obs_habitats,
                                                          survey_feature)
        if obs_habitat:
            survey_id = survey_feature['id']
            obs_syntaxon_id: int
            if survey_id is None:
                survey_id = self.sqlite_data_manager.get_survey_id_from_geometry(survey_feature)
                obs_syntaxon_id = self.sqlite_data_manager.get_obs_syntaxon_id_from_survey_id(survey_id)
            else:
                obs_syntaxon_id = int(survey_feature['rel_obs_syntaxon_id'])
            obs_habitat_id = int(obs_habitat['obs_hab_id'])
            self.sqlite_data_manager.insert_links_between_obs_hab(obs_syntaxon_id, obs_habitat_id)

        select_habitat.hidePopup()
        select_habitat.view().pressed.disconnect()
        select_habitat.clear()

    def add_habitat_to_database(self, code_typo: int, obs_habitats: QgsVectorLayer, survey_feature: QgsFeature,
                                obs_hab_nom_cite: str, obs_hab_code_cite: str):
        if not obs_habitats.isEditable():
            obs_habitats.startEditing()
        survey_id = survey_feature['id']
        obs_syntaxon_id: int
        if survey_id is None:
            survey_id = self.sqlite_data_manager.get_survey_id_from_geometry(survey_feature)
            obs_syntaxon_id = self.sqlite_data_manager.get_obs_syntaxon_id_from_survey_id(survey_id)
        else:
            obs_syntaxon_id = survey_feature['rel_obs_syntaxon_id']
        obs_syntaxon = obs_habitats.getFeature(obs_syntaxon_id)
        obs_habitat = QgsFeature()
        obs_habitat.setFields(obs_habitats.fields())
        obs_habitat['obs_hab_rel_assoc_id'] = survey_id
        obs_habitat['obs_hab_nom_cite'] = obs_hab_nom_cite
        obs_habitat['obs_hab_code_cite'] = obs_hab_code_cite
        obs_habitat['obs_hab_code_typo'] = code_typo
        if obs_syntaxon is not None:
            obs_habitat['obs_hab_rel_id'] = obs_syntaxon['obs_hab_rel_id']
        obs_habitats.updateFeature(obs_habitat)
        obs_habitats.addFeature(obs_habitat)
        obs_habitats.commitChanges()

    @staticmethod
    def add_habitat_to_table_widget(code_typo: int, table_syn_habitats: QTableWidget, obs_hab_nom_cite: str,
                                    obs_hab_code_cite: str):
        row = table_syn_habitats.rowCount()
        typo_widget = QTableWidgetItem(TableWidgetService.get_typo_label_from_code(code_typo))
        typo_widget.setBackground(TableWidgetService.get_typo_color_from_code(code_typo))
        table_syn_habitats.insertRow(row)
        table_syn_habitats.setItem(row, 0, QTableWidgetItem(obs_hab_code_cite))
        table_syn_habitats.setItem(row, 1, QTableWidgetItem(obs_hab_nom_cite))
        table_syn_habitats.setItem(row, 2, QTableWidgetItem(str(code_typo)))
        table_syn_habitats.setItem(row, 3, typo_widget)
        table_syn_habitats.setItem(row, 4, SurveyDataManager.get_cross_item())
        table_syn_habitats.sortItems(0, Qt.AscendingOrder)
        table_syn_habitats.sortItems(3, Qt.DescendingOrder)

    def taxon_auto_complete(self, select_taxon: QComboBox, taxref: QgsVectorLayer, taxon_cite: QLineEdit,
                            cd_nom_cite: QLineEdit, taxon_valide: QLineEdit, cd_nom_valide: QLineEdit):
        found_taxons: Optional[List[QgsFeature]] = None
        text_scripted = select_taxon.lineEdit().text()
        select_taxon.hidePopup()
        taxref_field_names = [field.name() for field in taxref.fields()]
        text_chunks = text_scripted.split(' ')
        lb_nom_index = taxref_field_names.index('lb_nom')
        cd_nom_index = taxref_field_names.index('cd_nom')
        first_word = text_chunks[0]
        len_text_chunk_0 = len(first_word)

        if len_text_chunk_0 > 2:
            found_taxons = self.found_taxons_matching(first_word, len_text_chunk_0, text_chunks, lb_nom_index, taxref)
        if found_taxons and len(found_taxons) > 0:
            self.populate_taxon_list(cd_nom_index, found_taxons, lb_nom_index, select_taxon, taxref_field_names,
                                     taxon_cite, cd_nom_cite, taxon_valide, cd_nom_valide)
        if len(text_scripted) == 0:
            select_taxon.clear()

    def found_taxons_matching(self, first_word: str, len_text_chunk_0: int, text_chunks: List[str], lb_nom_index: int,
                              taxref: QgsVectorLayer) -> Optional[List[QgsFeature]]:
        found_taxons = None
        matching_taxon_ids = self.sqlite_data_manager.get_taxons_starting_with(first_word)
        matching_taxons = []
        for taxon_id in matching_taxon_ids:
            taxon = taxref.getFeature(taxon_id[0])
            matching_taxons.append(taxon.attributes())
        if len(text_chunks) == 1:
            found_taxons = matching_taxons
        if len(text_chunks) > 1:
            found_taxons = [taxon for taxon in matching_taxons if
                            text_chunks[1].lower() in taxon[lb_nom_index][len_text_chunk_0:].lower()]
        return found_taxons

    def populate_taxon_list(self, cd_nom_index, found_taxons, lb_nom_index, select_taxon, taxref_field_names,
                            taxon_cite, cd_nom_cite, taxon_valide, cd_nom_valide):
        item_model = QStandardItemModel()
        for taxon in found_taxons:
            item = QStandardItem()
            if taxon[cd_nom_index] == taxon[taxref_field_names.index('cd_ref')]:
                item.setData(str(taxon[cd_nom_index]) + ' | ' + taxon[lb_nom_index], role=Qt.DisplayRole)
                item.setFont(QFont('Tahoma', 8, QFont.Bold))
                item.setData(taxon[cd_nom_index])
                item_model.appendRow(item)
            else:
                item.setData(str(taxon[cd_nom_index]) + ' | ' + taxon[lb_nom_index], role=Qt.DisplayRole)
                item.setData(taxon[cd_nom_index])
                item_model.appendRow(item)
        select_taxon.clear()
        select_taxon.setModel(item_model)
        select_taxon.showPopup()
        select_taxon.view().pressed.connect(
            lambda line: self.add_validated_taxon(line, select_taxon, found_taxons, taxon_cite, cd_nom_cite,
                                                  taxon_valide, cd_nom_valide))

    @staticmethod
    def add_validated_taxon(line, select_taxon: QComboBox, taxons: List[QgsFeature], taxon_cite: QLineEdit,
                            cd_nom_cite: QLineEdit, taxon_valide: QLineEdit, cd_nom_valide: QLineEdit):
        select_taxon.view().pressed.disconnect()
        taxon = taxons[line.row()]
        taxon_cite.setText(str(taxon[2] if taxon[2] else ''))
        cd_nom_cite.setText(str(taxon[1] if taxon[1] else ''))
        taxon_valide.setText(str(taxon[4] if taxon[4] else ''))
        cd_nom_valide.setText(str(taxon[3] if taxon[3] else ''))
        select_taxon.hidePopup()
        select_taxon.clear()

    def add_taxon(self, survey_feature: QgsFeature, observations: QgsVectorLayer, select_taxon: QComboBox,
                  taxon_cite: QLineEdit, taxon_cd_nom_cite: QLineEdit, strates: List[QComboBox],
                  table_taxons: QTableWidget, add_widget: QLabel, save_widget: QLabel):
        self.save_and_add_taxon(add_widget, None, observations, save_widget, select_taxon, strates, survey_feature,
                                table_taxons, taxon_cd_nom_cite, taxon_cite)

    def save_taxon(self, survey_feature: QgsFeature, observations: QgsVectorLayer, select_taxon: QComboBox,
                   taxon_cite: QLineEdit, taxon_cd_nom_cite: QLineEdit, strates: List[QComboBox],
                   table_taxons: QTableWidget, add_widget: QLabel, save_widget: QLabel):
        cd_nom = int(taxon_cd_nom_cite.text()) if taxon_cd_nom_cite and taxon_cd_nom_cite.text() != '' else None
        if cd_nom:
            row = [i for i in range(table_taxons.rowCount())
                   if table_taxons.item(i, 1).text() == taxon_cite.text()
                   and table_taxons.item(i, 0).text() == str(cd_nom)][0]
            observation = [observation for observation in observations.getFeatures()
                           if observation['obs_rel_id'] == survey_feature.id()
                           and observation['cd_nom'] == cd_nom
                           and observation['nom_cite'] == taxon_cite.text()][0]
        else:
            row = [i for i in range(table_taxons.rowCount())
                   if table_taxons.item(i, 1).text() == taxon_cite.text()][0]
            observation = [observation for observation in observations.getFeatures()
                           if observation['obs_rel_id'] == survey_feature.id()
                           and observation['nom_cite'] == taxon_cite.text()][0]

        success = self.save_and_add_taxon(add_widget, observation, observations, save_widget, select_taxon, strates,
                                          survey_feature, table_taxons, taxon_cd_nom_cite, taxon_cite)

        if success:
            table_taxons.removeRow(row)

    def save_and_add_taxon(self, add_widget, observation, observations, save_widget, select_taxon, strates,
                           survey_feature, table_taxons, taxon_cd_nom_cite, taxon_cite) -> bool:
        if not self.check_if_cd_nom_exists(observation, observations, survey_feature, taxon_cite, taxon_cd_nom_cite):
            select_taxon.hidePopup()
            select_taxon.clear()
            return False

        if taxon_cite.text():
            table_taxons.insertRow(table_taxons.rowCount())
            table_taxons.setItem(table_taxons.rowCount() - 1, 0, QTableWidgetItem(taxon_cd_nom_cite.text()))
            table_taxons.setItem(table_taxons.rowCount() - 1, 1, QTableWidgetItem(taxon_cite.text()))
            table_taxons.setItem(table_taxons.rowCount() - 1, 6, SurveyDataManager.get_cross_item())
            for s in range(len(strates)):
                table_taxons.setItem(table_taxons.rowCount() - 1, 2 + s, QTableWidgetItem(strates[s].currentText()))

            self.save_taxon_observation(survey_feature, observations, observation)
            self.refresh_taxon_sub_form(add_widget, save_widget, select_taxon)
            return True
        else:
            iface.messageBar().pushWarning('Ajout de taxon', 'Vous ne pouvez pas ajouter une observation sans taxon '
                                                             'cite.')
            return False

    def save_taxon_observation(self, survey_feature: QgsFeature, observations: QgsVectorLayer,
                               observation: QgsFeature = None):
        if not observations.isEditable():
            observations.startEditing()
        if not observation:
            observation = QgsFeature()
            observation.setFields(observations.fields())
            observation['obs_rel_id'] = survey_feature['id']
            observations.updateFeature(observation)
            observations.addFeature(observation)
        self.save_layer_values(observation, observations)
        observations.commitChanges()

    def refresh_taxon_sub_form(self, add_widget, save_widget, select_taxon):
        select_taxon.clear()
        form_inputs = DatabaseSpecificationService.get_inputs_specification('observations')
        for form_input in form_inputs:
            widget = WidgetService.find_widget(self.form, form_input['input_type'],
                                               form_input['input_name'].split(',')[0])
            if widget:
                TableWidgetService.clean_widget(widget)
                widget.setEnabled(True)

        select_taxon.setEnabled(True)
        add_widget.show()
        save_widget.hide()

    @staticmethod
    def check_if_cd_nom_exists(observation, observations, survey_feature, nom_cite, taxon_cd_nom_cite) -> bool:
        cd_nom = int(taxon_cd_nom_cite.text()) if taxon_cd_nom_cite and taxon_cd_nom_cite.text() != '' else None
        observations_with_same_id = [observation for observation in observations.getFeatures()
                                     if observation['obs_rel_id'] == survey_feature.id()
                                     and observation['nom_cite'] == nom_cite.text()
                                     and observation['cd_nom'] == cd_nom]
        if observation:
            if len(observations_with_same_id) > 0 and observation['cd_nom'] != cd_nom:
                iface.messageBar().pushWarning('Ajout de taxon',
                                               'Vous ne pouvez ajouter qu\'une observation par cd_nom.')
                return False
        else:
            if len(observations_with_same_id) > 0:
                iface.messageBar().pushWarning('Ajout de taxon',
                                               'Une seule observation par cd_nom est permise.')
                return False
        return True

    def get_taxons(self, feature_id, observations: QgsVectorLayer, table_taxons: QTableWidget):
        taxa = [observation for observation in observations.getFeatures()
                if observation['obs_rel_id'] == feature_id]
        if len(taxa) > 0:
            strate_values = self.list_values_provider.get_values_from_list_name('STRATE_VALUES')
            for taxon in taxa:
                row = table_taxons.rowCount()
                table_taxons.insertRow(row)
                if taxon['cd_nom'] in (None, NULL):
                    table_taxons.setItem(row, 0, QTableWidgetItem(""))
                else:
                    table_taxons.setItem(row, 0, QTableWidgetItem(str(taxon['cd_nom'])))
                table_taxons.setItem(row, 1, QTableWidgetItem(taxon['nom_cite']))
                if taxon['strate_arbo'] not in (0, None, NULL):
                    table_taxons.setItem(row, 2, QTableWidgetItem(strate_values[taxon['strate_arbo']][1]))
                if taxon['strate_arbu'] not in (0, None, NULL):
                    table_taxons.setItem(row, 3, QTableWidgetItem(strate_values[taxon['strate_arbu']][1]))
                if taxon['strate_herb'] not in (0, None, NULL):
                    table_taxons.setItem(row, 4, QTableWidgetItem(strate_values[taxon['strate_herb']][1]))
                if taxon['strate_musc'] not in (0, None, NULL):
                    table_taxons.setItem(row, 5, QTableWidgetItem(strate_values[taxon['strate_musc']][1]))
                table_taxons.setItem(row, 6, SurveyDataManager.get_cross_item())

    @staticmethod
    def get_cross_item() -> QTableWidgetItem:
        cross_item = QTableWidgetItem("><")
        cross_item.setForeground(QBrush(QColor(240, 15, 15)))
        cross_item.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        font = QFont()
        font.setBold(True)
        cross_item.setFont(font)
        return cross_item

    def get_observers(self, survey_id: int, survey_observer_links: QgsVectorLayer, lst_obs: QComboBox,
                      obs_list: QListWidget):
        observers = self.sqlite_data_manager.get_observers()
        observer_list = [[observer[0], observer[1] + ' ' + observer[2] + ' - ' + observer[3]] for observer in observers]
        observer_list[:0] = [[0, NULL]]
        WidgetService.load_combo_box(lst_obs, observer_list)

        observer_ids = [link['obs_id'] for link in survey_observer_links.getFeatures() if link['rel_id'] == survey_id]
        if len(observer_ids) > 0:
            attached_observers = [[observer[0], observer[1]] for observer in observer_list
                                  if observer[0] in observer_ids]
            if len(attached_observers) > 0:
                for observer in attached_observers:
                    obs_list.addItem(observer[1])

    def add_observer(self, survey_id: int, survey_observer_links: QgsVectorLayer, lst_obs: QComboBox,
                     obs_list: QListWidget) -> bool:
        if not self.check_if_observer_id_exists(survey_observer_links, survey_id, lst_obs):
            lst_obs.setCurrentIndex(0)
            return False

        observer_id = lst_obs.itemData(lst_obs.currentIndex())
        if observer_id != 0:
            new_feature = QgsFeature(survey_observer_links.fields())
            new_feature['obs_id'] = observer_id
            new_feature['rel_id'] = survey_id
            if not survey_observer_links.isEditable():
                survey_observer_links.startEditing()
            survey_observer_links.addFeature(new_feature)
            survey_observer_links.commitChanges()
            obs_list.addItem(lst_obs.currentText())
            lst_obs.setCurrentIndex(0)
            return True
        return False

    @staticmethod
    def check_if_observer_id_exists(survey_observer_links, survey_id, lst_obs) -> bool:
        observer_id = int(lst_obs.currentData()) if lst_obs and lst_obs.currentIndex() != 0 else None
        links_with_same_id = [link for link in survey_observer_links.getFeatures()
                              if link['rel_id'] == survey_id and link['obs_id'] == observer_id]

        if len(links_with_same_id) > 0:
            iface.messageBar().pushWarning('Ajout d\'observateur',
                                           'Un observateur ne peut pas figurer plus d\'une fois.')
            return False
        return True

    @staticmethod
    def delete_observer(survey_id: int, survey_observer_links: QgsVectorLayer, obs_list: QListWidget,
                        observateurs: QgsVectorLayer):
        items = obs_list.selectedItems()
        if len(items) > 0:
            for item in items:
                observer_id = [observer['obs_id'] for observer in observateurs.getFeatures()
                               if observer['obs_email'] == item.text().split(' - ')[1]][0]
                survey_observer_link_list = [link for link in survey_observer_links.getFeatures()
                                             if link['obs_id'] == observer_id and link['rel_id'] == survey_id]
                if len(survey_observer_link_list) > 0:
                    if not survey_observer_links.isEditable():
                        survey_observer_links.startEditing()
                    survey_observer_links.deleteFeature(survey_observer_link_list[0].id())
                    survey_observer_links.commitChanges()
                obs_list.takeItem(obs_list.row(item))

    def get_strates(self, survey_id: int, survey_strate_links: QgsVectorLayer, table_strates: QTableWidget):
        link_ids = [result[0] for result in self.sqlite_data_manager.get_linked_strates_to_survey(survey_id)]
        existing_strates = [link for link in survey_strate_links.getFeatures(link_ids)]
        if len(existing_strates) > 0:
            for strate in existing_strates:
                row = table_strates.rowCount()
                table_strates.insertRow(row)
                strate_id = strate['strate_id']
                strate_name = self.sqlite_data_manager.get_strate_name(strate_id)
                if strate_id not in (0, None, NULL):
                    table_strates.setItem(row, 0, QTableWidgetItem(str(strate_id)))
                    table_strates.setItem(row, 1, QTableWidgetItem(strate_name))
                if strate['recouvrement'] not in (None, NULL):
                    table_strates.setItem(row, 2, QTableWidgetItem(str(strate['recouvrement'])))
                if strate['hauteur_min'] not in (None, NULL):
                    table_strates.setItem(row, 3, QTableWidgetItem(str(strate['hauteur_min'])))
                if strate['hauteur_modale'] not in (None, NULL):
                    table_strates.setItem(row, 4, QTableWidgetItem(str(strate['hauteur_modale'])))
                if strate['hauteur_max'] not in (None, NULL):
                    table_strates.setItem(row, 5, QTableWidgetItem(str(strate['hauteur_max'])))
                table_strates.setItem(row, 6, SurveyDataManager.get_cross_item())

    def add_strate(self, survey_id: int, survey_strate_links: QgsVectorLayer, table_strates: QTableWidget,
                   strates_combo_box: QComboBox, struct_rec_pct_strate: QLineEdit, struct_haut_mod_strate: QLineEdit,
                   struct_haut_min_strate: QLineEdit, struct_haut_max_strate: QLineEdit) -> bool:
        if not self.check_if_strate_id_exists(None, survey_strate_links, survey_id, strates_combo_box):
            return False

        if strates_combo_box.itemData(strates_combo_box.currentIndex()) != 0:
            row = table_strates.rowCount()
            table_strates.insertRow(row)
            table_strates.setItem(row, 0, QTableWidgetItem(str(strates_combo_box.currentData())))
            table_strates.setItem(row, 1, QTableWidgetItem(strates_combo_box.currentText()))
            table_strates.setItem(row, 2, QTableWidgetItem(struct_rec_pct_strate.text()))
            table_strates.setItem(row, 3, QTableWidgetItem(struct_haut_min_strate.text()))
            table_strates.setItem(row, 4, QTableWidgetItem(struct_haut_mod_strate.text()))
            table_strates.setItem(row, 5, QTableWidgetItem(struct_haut_max_strate.text()))
            table_strates.setItem(row, 6, SurveyDataManager.get_cross_item())
            self.save_link_between_strate_and_survey(survey_id, survey_strate_links)
            self.refresh_strate_sub_form(strates_combo_box, struct_rec_pct_strate, struct_haut_mod_strate,
                                         struct_haut_min_strate, struct_haut_max_strate)
        else:
            iface.messageBar().pushWarning('Ajout de strate', 'Vous ne pouvez pas ajouter une ligne sans strate.')
            return False

    def save_link_between_strate_and_survey(self, survey_id: int, survey_strate_links: QgsVectorLayer,
                                            survey_strate_link: QgsFeature = None):
        if not survey_strate_links.isEditable():
            survey_strate_links.startEditing()
        if not survey_strate_link:
            survey_strate_link = QgsFeature()
            survey_strate_link.setFields(survey_strate_links.fields())
            survey_strate_link['rel_id'] = survey_id
            survey_strate_links.updateFeature(survey_strate_link)
            survey_strate_links.addFeature(survey_strate_link)
        self.save_layer_values(survey_strate_link, survey_strate_links)
        survey_strate_links.commitChanges()

    @staticmethod
    def refresh_strate_sub_form(strates_combo_box: QComboBox, struct_rec_pct_strate: QLineEdit,
                                struct_haut_mod_strate: QLineEdit, struct_haut_min_strate: QLineEdit,
                                struct_haut_max_strate: QLineEdit):
        widgets_to_clear = [strates_combo_box, struct_rec_pct_strate, struct_haut_mod_strate, struct_haut_min_strate,
                            struct_haut_max_strate]
        for widget in widgets_to_clear:
            TableWidgetService.clean_widget(widget)
            widget.setEnabled(True)

    @staticmethod
    def check_if_strate_id_exists(observation, observations, survey_id, strates_combo_box) -> bool:
        strate_id = int(strates_combo_box.currentData()) \
            if strates_combo_box and strates_combo_box.currentIndex() != 0 else None
        observations_with_same_id = [observation for observation in observations.getFeatures()
                                     if observation['rel_id'] == survey_id and observation['strate_id'] == strate_id]
        if observation:
            if len(observations_with_same_id) > 0 and observation['strate_id'] != strate_id:
                iface.messageBar().pushWarning('Ajout de taxon',
                                               'Vous ne pouvez ajouter qu\'une ligne par type de strate.')
                return False
        else:
            if len(observations_with_same_id) > 0:
                iface.messageBar().pushWarning('Ajout de taxon',
                                               'Une seule ligne de strate est permise.')
                return False
        return True

    def add_associated_survey(self, button: QLabel, releves: QgsVectorLayer, survey_feature: QgsFeature):
        target_layer: Optional[QgsVectorLayer] = None
        if button.objectName() == 'rel_create_new_polygon_rel':
            target_layer = [layer for layer in self.project.mapLayers(True).values()
                            if layer.name() == 'station_surface'][0]
        elif button.objectName() == 'rel_create_new_ligne_rel':
            target_layer = [layer for layer in self.project.mapLayers(True).values()
                            if layer.name() == 'station_ligne'][0]
        elif button.objectName() == 'rel_create_new_point_rel':
            target_layer = [layer for layer in self.project.mapLayers(True).values()
                            if layer.name() == 'station_point'][0]

        if target_layer:
            if not target_layer.isEditable():
                target_layer.startEditing()
            iface.setActiveLayer(target_layer)
            target_layer.featureAdded.connect(
                lambda fid: SurveyDataManager.feature_added(fid, releves, survey_feature))
            iface.actionAddFeature().trigger()
            self.form.window().close()

    @staticmethod
    def feature_added(fid, releves: QgsVectorLayer, parent_feature: QgsFeature):
        layer = iface.mapCanvas().currentLayer()
        layer.featureAdded.disconnect()
        new_geometry_feature: QgsFeature = layer.getFeature(fid)
        uuid_field_name: str
        if layer.name() == 'station_surface':
            uuid_field_name = 'rel_polygon_uuid'
        elif layer.name() == 'station_ligne':
            uuid_field_name = 'rel_ligne_uuid'
        elif layer.name() == 'station_point':
            uuid_field_name = 'rel_point_uuid'
        else:
            return
        new_survey_feature: QgsFeature = [feature for feature in releves.getFeatures()
                                          if feature[uuid_field_name] == new_geometry_feature['uuid']][0]
        new_survey_feature['rel_parent_id'] = parent_feature.id()
        releves.updateFeature(new_survey_feature)
        releves.commitChanges()
        releves.startEditing()

    def add_survey_associated_to_syntaxon(self, button: QLabel, releves: QgsVectorLayer, obs_habitats: QgsVectorLayer,
                                          survey_feature: QgsFeature, syntaxon_nom_cite: QLineEdit,
                                          syntaxon_code_phyto_cite: QLineEdit):
        target_layer: Optional[QgsVectorLayer] = None
        if button.objectName() == 'rel_syntax_create_new_polygon_rel':
            target_layer = [layer for layer in self.project.mapLayers(True).values()
                            if layer.name() == 'station_surface'][0]
        elif button.objectName() == 'rel_syntax_create_new_ligne_rel':
            target_layer = [layer for layer in self.project.mapLayers(True).values()
                            if layer.name() == 'station_ligne'][0]
        elif button.objectName() == 'rel_syntax_create_new_point_rel':
            target_layer = [layer for layer in self.project.mapLayers(True).values()
                            if layer.name() == 'station_point'][0]

        if target_layer:
            if not target_layer.isEditable():
                target_layer.startEditing()
            iface.setActiveLayer(target_layer)
            parent_code_phyto = syntaxon_code_phyto_cite.text()
            parent_syntaxon = syntaxon_nom_cite.text()
            target_layer.featureAdded.connect(
                lambda fid: SurveyDataManager.feature_associated_to_syntaxon(fid, releves, obs_habitats,
                                                                             survey_feature.id(), parent_syntaxon,
                                                                             parent_code_phyto))
            iface.actionAddFeature().trigger()
            self.form.window().close()

    @staticmethod
    def feature_associated_to_syntaxon(fid, releves: QgsVectorLayer, obs_habitats: QgsVectorLayer,
                                       parent_id: int, parent_syntaxon: str, parent_code_phyto: str):
        layer = iface.mapCanvas().currentLayer()
        layer.featureAdded.disconnect()
        new_geometry_feature: QgsFeature = layer.getFeature(fid)
        uuid_field_name: str
        if layer.name() == 'station_surface':
            uuid_field_name = 'rel_polygon_uuid'
        elif layer.name() == 'station_ligne':
            uuid_field_name = 'rel_ligne_uuid'
        elif layer.name() == 'station_point':
            uuid_field_name = 'rel_point_uuid'
        else:
            return
        new_survey_feature: QgsFeature = [feature for feature in releves.getFeatures()
                                          if feature[uuid_field_name] == new_geometry_feature['uuid']][0]
        if parent_code_phyto == '':
            obs_syntaxon_list = [obs_syntaxon for obs_syntaxon in obs_habitats.getFeatures()
                                 if obs_syntaxon['obs_hab_rel_id'] == parent_id
                                 and obs_syntaxon['obs_hab_nom_cite'] == parent_syntaxon]
        else:
            obs_syntaxon_list = [obs_syntaxon for obs_syntaxon in obs_habitats.getFeatures()
                                 if obs_syntaxon['obs_hab_rel_id'] == parent_id
                                 and obs_syntaxon['obs_hab_code_cite'] == parent_code_phyto
                                 and obs_syntaxon['obs_hab_nom_cite'] == parent_syntaxon]
        if len(obs_syntaxon_list) > 0:
            obs_syntaxon = obs_syntaxon_list[0]
            obs_syntaxon['obs_hab_rel_assoc_id'] = new_survey_feature.id()
            new_survey_feature['rel_obs_syntaxon_id'] = obs_syntaxon.id()
            obs_habitats.updateFeature(obs_syntaxon)
            obs_habitats.commitChanges()
            releves.updateFeature(new_survey_feature)
            releves.commitChanges()
            releves.startEditing()

    def save_survey(self, survey_layer: QgsVectorLayer, survey_feature: QgsFeature, obs_habitats: QgsVectorLayer):
        if not self.layer.isEditable():
            self.layer.startEditing()
        if not survey_layer.isEditable():
            survey_layer.startEditing()
        self.form.save()

        self.save_layer_values(survey_feature, survey_layer)
        if survey_feature['id'] is None:
            survey_feature = survey_layer.getFeature(
                self.sqlite_data_manager.get_survey_id_from_geometry(survey_feature))
        if self.sqlite_data_manager.obs_syntaxon_id_exists(survey_feature['id']):
            obs_syntaxon = [obs_syntaxon for obs_syntaxon in obs_habitats.getFeatures()
                            if obs_syntaxon['obs_hab_rel_assoc_id'] == survey_feature['id']][0]
            survey_feature['rel_obs_syntaxon_id'] = obs_syntaxon['obs_hab_id']
            survey_layer.updateFeature(survey_feature)
            self.save_habitat_observation(survey_feature, obs_habitats, 'obs_hab_rel_assoc_id', obs_syntaxon)
        elif survey_feature['rel_type_rel_id'] != 3:
            self.save_habitat_observation(survey_feature, obs_habitats, 'obs_hab_rel_assoc_id')
            obs_syntaxon = [obs_syntaxon for obs_syntaxon in obs_habitats.getFeatures()
                            if obs_syntaxon['obs_hab_rel_assoc_id'] == survey_feature['id']][0]
            survey_feature['rel_obs_syntaxon_id'] = obs_syntaxon['obs_hab_id']
            survey_layer.updateFeature(survey_feature)

        survey_layer.commitChanges()
        obs_habitats.commitChanges()
        self.layer.commitChanges()
        iface.messageBar().pushInfo('Sauvegarde releve', 'Effectuee.')

    def activate_survey(self, frel: QFrame, fobs: QFrame, save_rel: QLabel, activate: QLabel,
                        survey_layer: QgsVectorLayer, survey_feature: QgsFeature, obs_habitats: QgsVectorLayer):
        if not self.layer.isEditable():
            self.layer.startEditing()
        if not survey_layer.isEditable():
            survey_layer.startEditing()

        rel_type_widget = WidgetService.find_widget(self.form, 'QComboBox', 'rel_type_rel_id')
        rel_protocol_widget = WidgetService.find_widget(self.form, 'QComboBox', 'rel_protocole_id')
        rel_type_data = rel_type_widget.currentData()
        rel_protocol_data = rel_protocol_widget.currentData()

        if rel_type_data != 0 and rel_protocol_data != 0:
            self.form.save()
            rel_type_widget.setCurrentIndex(rel_type_widget.findData(rel_type_data))
            rel_type_widget.setEnabled(False)
            rel_protocol_widget.setCurrentIndex(rel_protocol_widget.findData(rel_protocol_data))
            self.save_survey(survey_layer, survey_feature, obs_habitats)
            frel.show()
            fobs.hide()
            save_rel.show()
            activate.hide()

    def map_survey(self, survey_layer: QgsVectorLayer, survey_feature: QgsFeature):
        if not self.layer.isEditable():
            self.layer.startEditing()
        if not survey_layer.isEditable():
            survey_layer.startEditing()
        self.form.save()

        form_inputs = DatabaseSpecificationService.get_inputs_specification('releves')
        for form_input in form_inputs:
            for form_input_name in form_input['input_name'].split(','):
                widget = WidgetService.find_widget(self.form, form_input['input_type'], form_input_name.strip())
                WidgetService.set_input_value(widget, survey_feature[form_input['field_name']])

    def save_layer_values(self, feature: QgsFeature, layer: QgsVectorLayer):
        form_inputs = DatabaseSpecificationService.get_inputs_specification(layer.name())
        for form_input in form_inputs:
            for form_input_name in form_input['input_name'].split(','):
                widget = WidgetService.find_widget(self.form, form_input['input_type'], form_input_name.strip())
                if widget:
                    value = WidgetService.get_input_value(widget)
                    if value != '' and value != 0:
                        feature.setAttribute(form_input['field_name'], value)
        layer.updateFeature(feature)
