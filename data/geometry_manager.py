import uuid
from typing import Optional

from qgis.core import QgsFeature, QgsVectorLayer


class GeometryManager:
    def __init__(self, layer: QgsVectorLayer, feature: QgsFeature):
        self.geometry_layer: QgsVectorLayer = layer
        self.geometry_feature: QgsFeature = feature
        self.survey_feature: Optional[QgsFeature] = None

    def get_survey_feature(self, releves: QgsVectorLayer) -> QgsFeature:
        # TODO launch only in edit single feature mode
        geometry_layer_name = self.geometry_layer.name()
        geometry_uuid = self.geometry_feature['uuid']
        if geometry_uuid is None and self.geometry_layer.isEditable():
            geometry_uuid = self.set_geometry_uuid()
        rel_field_name = self.get_geometry_field_name(geometry_layer_name)

        releve_list = [releve for releve in releves.getFeatures() if releve[rel_field_name] == geometry_uuid]
        if len(releve_list) > 0:
            self.survey_feature = releve_list[0]
        else:
            self.survey_feature = self.create_new_releve(releves)
            self.survey_feature.setAttribute(rel_field_name, geometry_uuid)
            releves.updateFeature(self.survey_feature)
            releves.commitChanges()

        return self.survey_feature

    def set_geometry_uuid(self) -> str:
        geometry_uuid = str(uuid.uuid4())
        self.geometry_feature.setAttribute('uuid', geometry_uuid)
        self.geometry_layer.updateFeature(self.geometry_feature)
        self.geometry_layer.endEditCommand()
        self.geometry_layer.commitChanges()
        self.geometry_layer.startEditing()
        return geometry_uuid

    @staticmethod
    def get_geometry_field_name(geometry_layer_name) -> str:
        if geometry_layer_name == 'station_surface':
            return 'rel_polygon_uuid'
        elif geometry_layer_name == 'station_ligne':
            return 'rel_ligne_uuid'
        elif geometry_layer_name == 'station_point':
            return 'rel_point_uuid'
        else:
            return ''

    @staticmethod
    def create_new_releve(releves) -> QgsFeature:
        releve = QgsFeature()
        releve.setFields(releves.fields())
        if not releves.isEditable():
            releves.startEditing()
        releves.addFeature(releve)
        releves.updateFeature(releve)
        releves.endEditCommand()
        return releve
