import json
from typing import Optional, List

from qgis.core import QgsProject

from ..constants.connection_constants import SPECIFICATION_FILE


class DatabaseSpecificationService:
    @staticmethod
    def get_spec() -> Optional[dict]:
        spec: Optional[dict]
        project = QgsProject.instance()
        with open(project.homePath() + "\\" + SPECIFICATION_FILE, 'r') as json_file:
            data = json_file.read()
            spec = json.loads(data)
        return spec

    @staticmethod
    def get_inputs_specification(table_name) -> List[dict]:
        form_inputs = []
        spec = DatabaseSpecificationService.get_spec()
        if spec['tables']:
            fields = [table['fields'] for table in spec['tables'] if table['name'] == table_name][0]
            form_inputs = [{"field_name": field['name'], "input_name": field['input']['name'],
                            "input_type": field['input']['type']} for field in fields if 'input' in field]
        return form_inputs

    @staticmethod
    def get_combo_boxes_specification() -> List[dict]:
        combo_boxes_inputs = []
        spec = DatabaseSpecificationService.get_spec()
        if spec['tables']:
            fields = []
            grouped_fields = [table['fields'] for table in spec['tables']]
            for group_of_fields in grouped_fields:
                fields.extend(group_of_fields)
            combo_boxes_inputs = []
            for field in fields:
                if 'input' in field and field['input']['type'] == "QComboBox":
                    try:
                        combo_boxes_inputs.append({"input_name": field['input']['name'], "input_list_name": field['input']['values']})
                    except:
                        print(field)
        return combo_boxes_inputs
