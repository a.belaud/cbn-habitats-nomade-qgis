from sqlite3 import Connection, Cursor
from typing import Optional, List

from qgis.core import QgsProject, QgsFeature
from qgis.utils import iface

from ..helper.connection_helper import ConnectionHelper


class DataManager:
    def __init__(self, project: QgsProject):
        self.connection: Optional[Connection] = ConnectionHelper(iface).sqlite_connection(project)
        self.cursor: Optional[Cursor] = None
        if self.connection:
            self.cursor = self.connection.cursor()

    def get_list_values_from_list_name(self, list_name: str) -> List:
        result: List = []
        if self.cursor:
            request = 'SELECT key, value, relation FROM listes_valeurs WHERE list_name=?'
            result = self.cursor.execute(request, (list_name,)).fetchall()
        return result

    def get_taxons_starting_with(self, two_letters: str) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT id FROM taxref WHERE lb_nom LIKE '%s'"
            result = self.cursor.execute(request % (two_letters + '%')).fetchall()
        return result

    def get_observers(self) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT obs_id, obs_prenom, obs_nom, obs_email FROM observateurs WHERE obs_turned_on=1"
            result = self.cursor.execute(request).fetchall()
        return result

    def get_linked_strates_to_survey(self, survey_id) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT id FROM liens_releve_strate WHERE rel_id=%s"
            result = self.cursor.execute(request % survey_id).fetchall()
        return result

    def get_strate_name(self, strate_id) -> str:
        result: List = []
        if self.cursor:
            request = "SELECT value FROM listes_valeurs WHERE list_name='STRATES_VALUES' AND key=%s"
            result = self.cursor.execute(request % strate_id).fetchone()
        return result[0]

    def get_survey_id_from_geometry(self, survey_feature: QgsFeature):
        result: List = []
        geometry_field: str = ""
        if survey_feature['rel_point_uuid']:
            geometry_field = 'rel_point_uuid'
        elif survey_feature['rel_ligne_uuid']:
            geometry_field = 'rel_ligne_uuid'
        elif survey_feature['rel_polygon_uuid']:
            geometry_field = 'rel_polygon_uuid'
        if self.cursor and geometry_field:
            request = "SELECT id FROM releves WHERE %s='%s'"
            result = self.cursor.execute(request % (geometry_field, survey_feature[geometry_field])).fetchone()
        return result[0]

    def get_obs_syntaxon_id_from_survey_id(self, survey_id: int):
        result: List = []
        if self.cursor:
            request = "SELECT rel_obs_syntaxon_id FROM releves WHERE id=%s"
            result = self.cursor.execute(request % survey_id).fetchone()
        return result[0]

    def obs_syntaxon_id_exists(self, survey_id) -> bool:
        result: List = []
        if self.cursor:
            request = "SELECT rel_obs_syntaxon_id FROM releves WHERE id=%s"
            result = self.cursor.execute(request % survey_id).fetchone()
        return result and len(result) > 0 and result[0]

    def insert_links_between_obs_hab(self, obs_syntaxon_id: int, obs_habitat_id: int):
        if self.cursor:
            request = "INSERT INTO liens_obs_habitat (id_obs_hab_1, id_obs_hab_2) VALUES (%s, %s)"
            self.cursor.execute(request % (obs_syntaxon_id, obs_habitat_id))
            self.cursor.execute(request % (obs_habitat_id, obs_syntaxon_id))
            self.connection.commit()

    def remove_links_between_obs_hab(self, obs_syntaxon_id: int, obs_habitat_id: int):
        if self.cursor:
            request = "DELETE FROM liens_obs_habitat WHERE id_obs_hab_1=%s AND id_obs_hab_2=%s"
            self.cursor.execute(request % (obs_syntaxon_id, obs_habitat_id))
            self.cursor.execute(request % (obs_habitat_id, obs_syntaxon_id))
            self.connection.commit()

    def remove_links_by_obs_hab_id(self, obs_habitat_id: int):
        if self.cursor:
            request_1 = "DELETE FROM liens_obs_habitat WHERE id_obs_hab_1=%s"
            request_2 = "DELETE FROM liens_obs_habitat WHERE id_obs_hab_2=%s"
            self.cursor.execute(request_1 % obs_habitat_id)
            self.cursor.execute(request_2 % obs_habitat_id)
            self.connection.commit()

    def are_obs_hab_linked(self, obs_hab_id_1: int, obs_hab_id_2: int) -> bool:
        result: List = []
        if self.cursor:
            request = "SELECT id FROM liens_obs_habitat WHERE id_obs_hab_1=%s AND id_obs_hab_2=%s"
            result = self.cursor.execute(request % (obs_hab_id_1, obs_hab_id_2)).fetchone()
        return result and len(result) > 0 and result[0]

    def is_obs_hab_linked_to_other(self, obs_habitat_id: int, obs_syntaxon_id: int) -> bool:
        result: List = []
        if self.cursor:
            request = "SELECT id FROM liens_obs_habitat WHERE id_obs_hab_1=%s AND id_obs_hab_2!=%s"
            result = self.cursor.execute(request % (obs_habitat_id, obs_syntaxon_id)).fetchone()
        return result and len(result) > 0 and result[0]

    def get_linked_obs_hab(self, obs_syntaxon_id: int) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT id_obs_hab_2 FROM liens_obs_habitat WHERE id_obs_hab_1=%s"
            result = self.cursor.execute(request % obs_syntaxon_id).fetchall()
        return result

    def clean_obs_hab_link_in_assoc_survey(self, obs_hab_rel_assoc_id: int):
        if self.cursor:
            request = "UPDATE releves SET rel_obs_syntaxon_id = NULL WHERE id=%s"
            self.cursor.execute(request % obs_hab_rel_assoc_id)
            self.connection.commit()

    def clean_other_obs_hab_link_to_assoc_survey(self, obs_hab_rel_assoc_id: int):
        if self.cursor:
            request = "UPDATE obs_habitats SET obs_hab_rel_assoc_id = NULL WHERE obs_hab_rel_assoc_id=%s"
            self.cursor.execute(request % obs_hab_rel_assoc_id)
            self.connection.commit()

    def clean_link_to_assoc_survey(self, obs_habitat_id: int):
        if self.cursor:
            request = "UPDATE obs_habitats SET obs_hab_rel_assoc_id = NULL WHERE obs_hab_id=%s"
            self.cursor.execute(request % obs_habitat_id)
            self.connection.commit()
