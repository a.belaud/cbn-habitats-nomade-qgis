# CBN habitats nomade QGIS
- Version de QGIS utilisée : 3.16 LTR
- Version de python utilisée pour le développement : 3.7

**Ce projet QGis est un outil de saisie des données habitats au format Lobelia**

Ce que l'outil permet :

- saisir des relevés par une entrée géographique (point/ligne/polygone)
- décrire finement le relevé (Métadonnées)
- associer une ou plusieurs observations de taxons (rattachement possible à Taxref v13) et/ou de syntaxons (s'appuie sur
  un catalogue interne)

## Comment contribuer ?

Le code exécuté par le projet est présenté selon plusieurs fichiers organisés par répertoire, chacun accueillant le code
d'une classe python.  
QGis ne supportant par contre pas bien ce mode de développement multi fichiers, afin de profiter des nouvelles
fonctionnalités que vous ajouterez, il vous faudra fusionner votre code au sein d'un fichier unique grace au script
fourni.  
Le fichier unique nommé `survey_form.py` sera créé à la racine du projet et pourra être déployé tel quel en production.

Pour générer ce fichier unique :

```
python3 scripts/unique_file_generator.py
```

**Nota**

- Le projet assigne déjà la méthode form_open de ce fichier comme code associé aux couches de relevés.
- Le fichier 'survey_form.py' est commité afin de rendre le repository fonctionnel dès son clonage. Aussi il faut
  veiller à générer un fichier propre à chaque commit.

## Comment importer son catalogue phyto ?

1. Disposer d'un fichier csv, séparé par une virgule et encodé en UTF-8 sans BOM, contenant la description de son
   référentiel syntaxonomique.  
   Les noms de colonne attendus en entrée sont : supra, id, code, syntaxon, aut, correct, presenceCBNBP. Ils peuvent
   bien-sûr être adaptés dans le script.
2. Lancer le script cat_phyto_ingest_service.py depuis la console en indiquant en argument le chemin relatif nom du
   fichier csv cible.  
   Le script est toujours à lancer depuis la racine du projet QGis :
    ```
    python3 ingest/cat_phyto_ingest_service.py Syntaxon_CBNBP_v2020.csv
    ```

## Comment importer ses listes de valeurs ?

1. Disposer d'un fichier csv, séparé par une virgule et encodé en UTF-8 sans BOM, contenant la description des listes de
   valeurs utilisées dans Lobelia.  
   Les noms de colonne attendus en entrée sont : list_name, key, value, relation. Ils peuvent bien-sûr être adaptés dans
   le script.
2. Lancer le script list_values_ingest_service.py depuis la console en indiquant en argument le chemin relatif nom du
   fichier csv cible.  
   Le script est toujours à lancer depuis la racine du projet QGis :
    ```
    python3 ingest/list_values_ingest_service.py Listes_valeurs_v2021.csv
    ```

## Comment importer la liste des observateurs ?

1. Disposer d'un fichier csv, séparé par une virgule et encodé en UTF-8 sans BOM, contenant la listes des
   observateurs.  
   Les noms de colonne attendus en entrée sont : id, prenom, nom, email, active. Ils peuvent bien-sûr être adaptés dans
   le script.
2. Lancer le script observers_ingest_service.py depuis la console en indiquant en argument le chemin relatif nom du
   fichier csv cible.  
   Le script est toujours à lancer depuis la racine du projet QGis :
    ```
    python3 ingest/observers_ingest_service.py Observateurs.csv
    ```

## Comment générer un paquet de livrables ?

1. Lancer le script `release_package_generator.py` depuis la console.  
   Le script est toujours à lancer depuis la racine du projet QGis :
    ```
    python3 scripts/release_package_generator.py
    ```
2. Un fichier `lobelia_qgis_vX.X.zip` est généré à la racine du projet contenant l'ensemble des fichiers nécessaires au
   déploiement de l'application.  
   Ce fichier peut être utilisé comme source attachée à la déclaration d'une version sur le dépôt Gitlab.

## Comment générer la documentation utilisateur ?

La notice d'installation et la documentation utilisateur sont disponibles dans le
[wiki](https://gitlab.com/a.belaud/cbn-habitats-nomade-qgis/-/wikis/home) du dépôt.  
Afin de pouvoir embarquer cette documentation sur le terrain, il est nécessaire de transformer en pdf ces ressources
gérées sous gitlab sous la forme de fichier markdown.

1. Cloner dans un nouveau répertoire **en dehors du projet actuel** le wiki gitlab :
   ```
    git clone git@gitlab.com:a.belaud/cbn-habitats-nomade-qgis.wiki.git
    ```
   Si vous avez déjà cloné ce repo, n'oubliez pas de le mettre à jour (`git pull`) avant de passer aux étapes
   suivantes !

2. Ajuster la variable wiki_repo_path dans le script `pdf_documentation_generator.py`:
   ```
   wiki_repo_path = <my path to the wiki repo>
   ```
3. Lancer le script suivant depuis la racine du projet en console :
    ```
    python3 scripts/pdf_documentation_generator.py
    ```
4. 2 fichiers sont générés à la racine du projet `installation.pdf` et `documentation-utilisateur.pdf`.  
   Ces fichiers seront embarqués dans le paquet de livrables décrit ci-dessus.  
