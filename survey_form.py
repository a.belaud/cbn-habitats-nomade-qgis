# coding: utf-8
import json
import sqlite3
import uuid
from sqlite3 import Connection
from sqlite3 import Cursor
from typing import List, Optional

from PyQt5.QtCore import QDate, QVariant
from PyQt5.QtCore import QEvent
from PyQt5.QtCore import QObject, pyqtSignal, Qt
from PyQt5.QtGui import QColor
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QFont, QBrush
from PyQt5.QtWidgets import QButtonGroup, QListWidget, QListWidgetItem
from PyQt5.QtWidgets import QGroupBox, QWidget
from PyQt5.QtWidgets import QHeaderView, QCalendarWidget, QFrame, QMessageBox
from PyQt5.QtWidgets import QTabWidget
from PyQt5.QtWidgets import QTableWidget, QLineEdit, QPlainTextEdit, QComboBox, QLabel, QTextEdit, QCheckBox
from PyQt5.QtWidgets import QTableWidgetItem
from qgis.PyQt.QtCore import NULL
from qgis.core import QgsApplication, QgsAuthMethodConfig, QgsAuthManager
from qgis.core import QgsFeature, QgsVectorLayer
from qgis.core import QgsProject
from qgis.gui import QgisInterface
from qgis.gui import QgsAttributeForm
from qgis.utils import iface

SQLITE_FILE = "donnees_habitats.sqlite"

AUTH_CONFIG = 'lobelia_user'

SPECIFICATION_FILE = "database_specification.json"
TAXONS_HEADER_LABELS = ['cd_nom', 'taxon', 'strate_arbo', 'strate_arbu', 'strate_herb', 'strate_musc', 'suppr.']
SYNTAXONS_HEADER_LABELS = ['code_phyto', 'syntaxon', 'rec.', 'forme', 'com. ', 'attr. add.', 'suppr.']
HIC_HEADER_LABELS = ['code', 'libelle', 'rec.', 'forme', 'com. ', 'attr. add.', 'suppr.']
EUNIS_HEADER_LABELS = ['code', 'libelle', 'rec.', 'forme', 'com. ', 'attr. add.', 'suppr.']
CORINE_HEADER_LABELS = ['code', 'libelle', 'rec.', 'forme', 'com. ', 'attr. add.', 'suppr.']
HABITATS_HEADER_LABELS = ['code', 'libelle', 'code typo', 'typologie', 'suppr.']
STRATES_STRUCT_LABELS = ['id', 'strate', 'rec.', 'h min', 'h mod.', 'h max', 'suppr.']




class DatabaseSpecificationService:
    @staticmethod
    def get_spec() -> Optional[dict]:
        spec: Optional[dict]
        project = QgsProject.instance()
        with open(project.homePath() + "\\" + SPECIFICATION_FILE, 'r') as json_file:
            data = json_file.read()
            spec = json.loads(data)
        return spec

    @staticmethod
    def get_inputs_specification(table_name) -> List[dict]:
        form_inputs = []
        spec = DatabaseSpecificationService.get_spec()
        if spec['tables']:
            fields = [table['fields'] for table in spec['tables'] if table['name'] == table_name][0]
            form_inputs = [{"field_name": field['name'], "input_name": field['input']['name'],
                            "input_type": field['input']['type']} for field in fields if 'input' in field]
        return form_inputs

    @staticmethod
    def get_combo_boxes_specification() -> List[dict]:
        combo_boxes_inputs = []
        spec = DatabaseSpecificationService.get_spec()
        if spec['tables']:
            fields = []
            grouped_fields = [table['fields'] for table in spec['tables']]
            for group_of_fields in grouped_fields:
                fields.extend(group_of_fields)
            combo_boxes_inputs = []
            for field in fields:
                if 'input' in field and field['input']['type'] == "QComboBox":
                    try:
                        combo_boxes_inputs.append({"input_name": field['input']['name'], "input_list_name": field['input']['values']})
                    except:
                        print(field)
        return combo_boxes_inputs



class GeometryManager:
    def __init__(self, layer: QgsVectorLayer, feature: QgsFeature):
        self.geometry_layer: QgsVectorLayer = layer
        self.geometry_feature: QgsFeature = feature
        self.survey_feature: Optional[QgsFeature] = None

    def get_survey_feature(self, releves: QgsVectorLayer) -> QgsFeature:
        # TODO launch only in edit single feature mode
        geometry_layer_name = self.geometry_layer.name()
        geometry_uuid = self.geometry_feature['uuid']
        if geometry_uuid is None and self.geometry_layer.isEditable():
            geometry_uuid = self.set_geometry_uuid()
        rel_field_name = self.get_geometry_field_name(geometry_layer_name)

        releve_list = [releve for releve in releves.getFeatures() if releve[rel_field_name] == geometry_uuid]
        if len(releve_list) > 0:
            self.survey_feature = releve_list[0]
        else:
            self.survey_feature = self.create_new_releve(releves)
            self.survey_feature.setAttribute(rel_field_name, geometry_uuid)
            releves.updateFeature(self.survey_feature)
            releves.commitChanges()

        return self.survey_feature

    def set_geometry_uuid(self) -> str:
        geometry_uuid = str(uuid.uuid4())
        self.geometry_feature.setAttribute('uuid', geometry_uuid)
        self.geometry_layer.updateFeature(self.geometry_feature)
        self.geometry_layer.endEditCommand()
        self.geometry_layer.commitChanges()
        self.geometry_layer.startEditing()
        return geometry_uuid

    @staticmethod
    def get_geometry_field_name(geometry_layer_name) -> str:
        if geometry_layer_name == 'station_surface':
            return 'rel_polygon_uuid'
        elif geometry_layer_name == 'station_ligne':
            return 'rel_ligne_uuid'
        elif geometry_layer_name == 'station_point':
            return 'rel_point_uuid'
        else:
            return ''

    @staticmethod
    def create_new_releve(releves) -> QgsFeature:
        releve = QgsFeature()
        releve.setFields(releves.fields())
        if not releves.isEditable():
            releves.startEditing()
        releves.addFeature(releve)
        releves.updateFeature(releve)
        releves.endEditCommand()
        return releve




class ListValuesProvider:
    def __init__(self, project: QgsProject):
        self.data_manager = DataManager(project)

    def get_values_from_list_name(self, list_name: str) -> List[List]:
        result_list = self.data_manager.get_list_values_from_list_name(list_name)
        returned_list = [[value[0], value[1], value[2]] for value in result_list]
        return returned_list




class DatabaseBuilder:
    def __init__(self, project: QgsProject):
        self.spec: Optional[dict] = DatabaseSpecificationService.get_spec()
        self.connection: Optional[Connection] = ConnectionHelper(iface).sqlite_connection(project)
        self.cursor: Optional[Cursor]
        if self.connection:
            self.cursor = self.connection.cursor()

    def build_or_alter(self):
        existing_tables: List[str] = []
        tables_to_create: List[str] = []

        if self.get_database_version() != self.spec['version']:
            for table in self.spec['tables']:
                table_name = table['name']
                if self.table_exists(table_name):
                    existing_tables.append(table_name)
                else:
                    tables_to_create.append(table_name)

            for table_name in existing_tables:
                self.alter_table(table_name)

            for table_name in tables_to_create:
                self.create_table(table_name)

            self.update_database_version(self.spec['version'])

    def table_exists(self, table_name: str) -> bool:
        request = "SELECT count(name) FROM sqlite_master WHERE type='table' AND name='" + table_name + "'"
        self.cursor.execute(request)
        fetchone = self.cursor.fetchone()
        return fetchone[0] == 1

    def alter_table(self, table_name: str):
        altered = False
        table_specification = [table for table in self.spec['tables'] if table['name'] == table_name][0]
        target_columns = [(field['name'], field['type']) for field in table_specification['fields']]
        existing_columns = [i[1] for i in self.cursor.execute('PRAGMA table_info(%s)' % table_name)]

        for column in target_columns:
            if column[0] not in existing_columns:
                self.cursor.execute("ALTER TABLE " + table_name + " ADD COLUMN '%s' '%s'" % (column[0], column[1]))
                altered = True

        if table_specification['is_geometric']:
            geometry_specification = table_specification['geometry']
            geometry_name = geometry_specification['name']
            if geometry_name not in existing_columns:
                self.add_geometry_column(table_name, geometry_specification)
                altered = True

        if 'indices' in table_specification:
            self.add_indices(table_specification)

        if altered:
            iface.messageBar().pushInfo('Database builder', table_name + ' table altered')
        self.connection.commit()

    def create_table(self, table_name: str):
        table_specification = [table for table in self.spec['tables'] if table['name'] == table_name][0]
        columns = [(field['name'], field['type'], field['complement'] if 'complement' in field else '')
                   for field in table_specification['fields']]
        fields = []

        for column in columns:
            fields.append(' '.join(column))

        joined_fields = ", ".join(fields)
        request = "CREATE TABLE IF NOT EXISTS " + table_name + "(" + joined_fields + ")"
        self.cursor.execute(request)

        if table_specification['is_geometric']:
            self.add_geometry_column(table_name, table_specification['geometry'])

        if 'indices' in table_specification:
            self.add_indices(table_specification)

        iface.messageBar().pushInfo('Database builder', table_name + ' table created')
        self.connection.commit()

    def add_geometry_column(self, table_name: str, geometry):
        geom_specification = (table_name, geometry['name'], geometry['projection'], geometry['type'], geometry['axes'])
        geom_request = "SELECT AddGeometryColumn('%s', '%s', %s, '%s', '%s')"
        self.cursor.execute(geom_request % geom_specification)
        iface.messageBar().pushInfo('Database builder', table_name + ' - geometry added')

    def add_indices(self, table_specification: dict):
        table_name = table_specification['name']
        existing_indices = [i[1] for i in self.cursor.execute('PRAGMA index_list(%s)' % table_name)]
        indices = [(index['name'], table_name,
                    index['field'] + ' ' + index['complement'] if 'complement' in index else index['field'])
                   for index in table_specification['indices']]
        request = "CREATE INDEX %s ON %s (%s)"
        for index in indices:
            if index[0] not in existing_indices:
                self.cursor.execute(request % index)
                iface.messageBar().pushInfo('Database builder', table_name + ' - index ' + index[0] + ' added')

    def get_database_version(self) -> str:
        result: str = ""
        if self.cursor:
            request = "SELECT version FROM version WHERE id=1"
            result = self.cursor.execute(request).fetchone()[0]
        return result

    def update_database_version(self, version: str):
        if self.cursor:
            request = "UPDATE version SET version=? WHERE id=1"
            self.cursor.execute(request, (version,))
            self.connection.commit()


class DatabaseCleaner:
    def __init__(self, connection: Connection):
        self.connection = connection
        self.cursor: Cursor = self.connection.cursor()
        self.geometry_tables = [('station_point', 'point'), ('station_ligne', 'ligne'), ('station_surface', 'polygon')]

    def clean_orphans(self):
        self.clean_survey_orphans()
        self.clean_observations_orphans()
        self.clean_obs_syntaxon_orphans()

    def clean_survey_orphans(self):
        orphans_request = "SELECT releves.id FROM releves LEFT JOIN "
        join_clause = []
        for geometry_table in self.geometry_tables:
            join_clause.append(geometry_table[0] + " on " + geometry_table[0] + ".uuid = releves.rel_" +
                               geometry_table[1] + "_uuid ")
        orphans_request += "LEFT JOIN ".join(join_clause) + "WHERE "
        where_clause = []
        for geometry_table in self.geometry_tables:
            where_clause.append(geometry_table[0] + ".uuid is NULL")
        orphans_request += " AND ".join(where_clause)
        self.cursor.execute(orphans_request)
        orphans = [orphan[0] for orphan in self.cursor.fetchall()]

        if len(orphans) > 0:
            delete_request: str
            if len(orphans) == 1:
                delete_request = "DELETE FROM releves WHERE id = " + str(orphans[0])
            else:
                delete_request = "DELETE FROM releves WHERE id in " + str(tuple(orphans))
            self.cursor.execute(delete_request)
            self.connection.commit()

    def clean_observations_orphans(self):
        request = "SELECT observations.obs_id FROM observations LEFT JOIN releves on releves.id = " \
                  "observations.obs_rel_id WHERE releves.id is NULL"
        self.cursor.execute(request)
        orphans = [orphan[0] for orphan in self.cursor.fetchall()]

        if len(orphans) > 0:
            delete_request: str
            if len(orphans) == 1:
                delete_request = "DELETE FROM observations WHERE obs_id = " + str(orphans[0])
            else:
                delete_request = "DELETE FROM observations WHERE obs_id in " + str(tuple(orphans))
            self.cursor.execute(delete_request)
            self.connection.commit()

    def clean_obs_syntaxon_orphans(self):
        request = "SELECT obs_syntaxons.syntaxon_id FROM obs_syntaxons LEFT JOIN releves on releves.id = " \
                  "obs_syntaxons.obs_hab_rel_id WHERE releves.id is NULL"
        self.cursor.execute(request)
        orphans = [orphan[0] for orphan in self.cursor.fetchall()]

        if len(orphans) > 0:
            delete_request: str
            if len(orphans) == 1:
                delete_request = "DELETE FROM obs_syntaxons WHERE syntaxon_id = " + str(orphans[0])
            else:
                delete_request = "DELETE FROM obs_syntaxons WHERE syntaxon_id in " + str(tuple(orphans))
            self.cursor.execute(delete_request)
            self.connection.commit()




class DataManager:
    def __init__(self, project: QgsProject):
        self.connection: Optional[Connection] = ConnectionHelper(iface).sqlite_connection(project)
        self.cursor: Optional[Cursor] = None
        if self.connection:
            self.cursor = self.connection.cursor()

    def get_list_values_from_list_name(self, list_name: str) -> List:
        result: List = []
        if self.cursor:
            request = 'SELECT key, value, relation FROM listes_valeurs WHERE list_name=?'
            result = self.cursor.execute(request, (list_name,)).fetchall()
        return result

    def get_taxons_starting_with(self, two_letters: str) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT id FROM taxref WHERE lb_nom LIKE '%s'"
            result = self.cursor.execute(request % (two_letters + '%')).fetchall()
        return result

    def get_observers(self) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT obs_id, obs_prenom, obs_nom, obs_email FROM observateurs WHERE obs_turned_on=1"
            result = self.cursor.execute(request).fetchall()
        return result

    def get_linked_strates_to_survey(self, survey_id) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT id FROM liens_releve_strate WHERE rel_id=%s"
            result = self.cursor.execute(request % survey_id).fetchall()
        return result

    def get_strate_name(self, strate_id) -> str:
        result: List = []
        if self.cursor:
            request = "SELECT value FROM listes_valeurs WHERE list_name='STRATES_VALUES' AND key=%s"
            result = self.cursor.execute(request % strate_id).fetchone()
        return result[0]

    def get_survey_id_from_geometry(self, survey_feature: QgsFeature):
        result: List = []
        geometry_field: str = ""
        if survey_feature['rel_point_uuid']:
            geometry_field = 'rel_point_uuid'
        elif survey_feature['rel_ligne_uuid']:
            geometry_field = 'rel_ligne_uuid'
        elif survey_feature['rel_polygon_uuid']:
            geometry_field = 'rel_polygon_uuid'
        if self.cursor and geometry_field:
            request = "SELECT id FROM releves WHERE %s='%s'"
            result = self.cursor.execute(request % (geometry_field, survey_feature[geometry_field])).fetchone()
        return result[0]

    def get_obs_syntaxon_id_from_survey_id(self, survey_id: int):
        result: List = []
        if self.cursor:
            request = "SELECT rel_obs_syntaxon_id FROM releves WHERE id=%s"
            result = self.cursor.execute(request % survey_id).fetchone()
        return result[0]

    def obs_syntaxon_id_exists(self, survey_id) -> bool:
        result: List = []
        if self.cursor:
            request = "SELECT rel_obs_syntaxon_id FROM releves WHERE id=%s"
            result = self.cursor.execute(request % survey_id).fetchone()
        return result and len(result) > 0 and result[0]

    def insert_links_between_obs_hab(self, obs_syntaxon_id: int, obs_habitat_id: int):
        if self.cursor:
            request = "INSERT INTO liens_obs_habitat (id_obs_hab_1, id_obs_hab_2) VALUES (%s, %s)"
            self.cursor.execute(request % (obs_syntaxon_id, obs_habitat_id))
            self.cursor.execute(request % (obs_habitat_id, obs_syntaxon_id))
            self.connection.commit()

    def remove_links_between_obs_hab(self, obs_syntaxon_id: int, obs_habitat_id: int):
        if self.cursor:
            request = "DELETE FROM liens_obs_habitat WHERE id_obs_hab_1=%s AND id_obs_hab_2=%s"
            self.cursor.execute(request % (obs_syntaxon_id, obs_habitat_id))
            self.cursor.execute(request % (obs_habitat_id, obs_syntaxon_id))
            self.connection.commit()

    def remove_links_by_obs_hab_id(self, obs_habitat_id: int):
        if self.cursor:
            request_1 = "DELETE FROM liens_obs_habitat WHERE id_obs_hab_1=%s"
            request_2 = "DELETE FROM liens_obs_habitat WHERE id_obs_hab_2=%s"
            self.cursor.execute(request_1 % obs_habitat_id)
            self.cursor.execute(request_2 % obs_habitat_id)
            self.connection.commit()

    def are_obs_hab_linked(self, obs_hab_id_1: int, obs_hab_id_2: int) -> bool:
        result: List = []
        if self.cursor:
            request = "SELECT id FROM liens_obs_habitat WHERE id_obs_hab_1=%s AND id_obs_hab_2=%s"
            result = self.cursor.execute(request % (obs_hab_id_1, obs_hab_id_2)).fetchone()
        return result and len(result) > 0 and result[0]

    def is_obs_hab_linked_to_other(self, obs_habitat_id: int, obs_syntaxon_id: int) -> bool:
        result: List = []
        if self.cursor:
            request = "SELECT id FROM liens_obs_habitat WHERE id_obs_hab_1=%s AND id_obs_hab_2!=%s"
            result = self.cursor.execute(request % (obs_habitat_id, obs_syntaxon_id)).fetchone()
        return result and len(result) > 0 and result[0]

    def get_linked_obs_hab(self, obs_syntaxon_id: int) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT id_obs_hab_2 FROM liens_obs_habitat WHERE id_obs_hab_1=%s"
            result = self.cursor.execute(request % obs_syntaxon_id).fetchall()
        return result

    def clean_obs_hab_link_in_assoc_survey(self, obs_hab_rel_assoc_id: int):
        if self.cursor:
            request = "UPDATE releves SET rel_obs_syntaxon_id = NULL WHERE id=%s"
            self.cursor.execute(request % obs_hab_rel_assoc_id)
            self.connection.commit()

    def clean_other_obs_hab_link_to_assoc_survey(self, obs_hab_rel_assoc_id: int):
        if self.cursor:
            request = "UPDATE obs_habitats SET obs_hab_rel_assoc_id = NULL WHERE obs_hab_rel_assoc_id=%s"
            self.cursor.execute(request % obs_hab_rel_assoc_id)
            self.connection.commit()

    def clean_link_to_assoc_survey(self, obs_habitat_id: int):
        if self.cursor:
            request = "UPDATE obs_habitats SET obs_hab_rel_assoc_id = NULL WHERE obs_hab_id=%s"
            self.cursor.execute(request % obs_habitat_id)
            self.connection.commit()


class SurveyDataManager:
    def __init__(self, project: QgsProject, form: QgsAttributeForm, layer: QgsVectorLayer, feature: QgsFeature):
        self.project = project
        self.form: QgsAttributeForm = form
        self.layer: QgsVectorLayer = layer
        self.geometry_feature: QgsFeature = feature
        self.list_values_provider: ListValuesProvider = ListValuesProvider(self.project)
        self.sqlite_data_manager: DataManager = DataManager(self.project)

    def habitat_auto_complete(self, select_habitat: QComboBox, catalogue_phyto: QgsVectorLayer, habref: QgsVectorLayer,
                              buttons: QButtonGroup, habitat_cite: QLineEdit, habitat_code_cite: QLineEdit,
                              syntaxon_valide: QLineEdit, syntaxon_code_valide: QLineEdit,
                              survey_feature: QgsFeature = None, obs_habitats: QgsVectorLayer = None,
                              table_syn_habitats: QTableWidget = None):
        code_typo: int = TableWidgetService.get_code_typo(buttons)
        ref_habitat_values: List
        nom_habitat_simple_index: int
        code_habitat_index: int
        code_habitat_retenu_index: int
        ref_phyto: bool

        if code_typo == 0:
            ref_habitat_values = [value.attributes() for value in catalogue_phyto.getFeatures()]
            ref_habitat_field_names = [f.name() for f in catalogue_phyto.fields()]
            nom_habitat_simple_index = ref_habitat_field_names.index('nom_syntaxon_simple')
            code_habitat_index = ref_habitat_field_names.index('code_phyto')
            code_habitat_retenu_index = ref_habitat_field_names.index('code_phyto_syntaxon_retenu')
            ref_phyto = True
        else:
            ref_habitat_values = [value.attributes() for value in habref.getFeatures() if value['cd_typo'] == code_typo]
            ref_habitat_field_names = [f.name() for f in habref.fields()]
            nom_habitat_simple_index = ref_habitat_field_names.index('lb_hab_fr')
            code_habitat_index = ref_habitat_field_names.index('lb_code')
            code_habitat_retenu_index = 0
            ref_phyto = False

        self.generate_habitat_auto_complete(select_habitat, ref_habitat_values, nom_habitat_simple_index,
                                            code_habitat_index, code_habitat_retenu_index, ref_phyto, code_typo,
                                            habitat_cite, habitat_code_cite, syntaxon_valide, syntaxon_code_valide,
                                            survey_feature, obs_habitats, table_syn_habitats)

    def syntaxon_auto_complete(self, select_habitat: QComboBox, catalogue_phyto: QgsVectorLayer,
                               syntaxon_cite: QLineEdit, syntaxon_code_cite: QLineEdit, syntaxon_valide: QLineEdit,
                               syntaxon_code_valide: QLineEdit):
        ref_habitat_values = [c.attributes() for c in catalogue_phyto.getFeatures()]
        syntaxon_field_names = [f.name() for f in catalogue_phyto.fields()]
        nom_habitat_simple_index = syntaxon_field_names.index('nom_syntaxon_simple')
        code_habitat_index = syntaxon_field_names.index('code_phyto')
        code_habitat_retenu_index = syntaxon_field_names.index('code_phyto_syntaxon_retenu')

        self.generate_habitat_auto_complete(select_habitat, ref_habitat_values, nom_habitat_simple_index,
                                            code_habitat_index, code_habitat_retenu_index, True, 0, syntaxon_cite,
                                            syntaxon_code_cite, syntaxon_valide, syntaxon_code_valide)

    def generate_habitat_auto_complete(self, select_habitat, ref_habitat_values, nom_habitat_simple_index,
                                       code_habitat_index, code_habitat_retenu_index, ref_phyto, code_typo,
                                       habitat_cite, habitat_code_cite, syntaxon_valide, syntaxon_code_valide,
                                       survey_feature: QgsFeature = None, obs_habitats: QgsVectorLayer = None,
                                       table_syn_habitats: QTableWidget = None):
        text_scripted = select_habitat.lineEdit().text()
        select_habitat.hidePopup()
        text_chunks = text_scripted.split(' ')
        found_habitats = self.filter_habitat_list(code_typo, code_habitat_index, nom_habitat_simple_index,
                                                  ref_habitat_values, text_chunks)

        if found_habitats and len(found_habitats) > 0:
            self.populate_select_habitat(code_habitat_index, code_habitat_retenu_index, found_habitats,
                                         nom_habitat_simple_index, ref_phyto, select_habitat)
            select_object_name = select_habitat.objectName()
            if select_object_name == 'select_habitat' or select_object_name == 'syntaxon_recherche':
                select_habitat.view().pressed.connect(
                    lambda line: self.add_validated_habitat(line, select_habitat, found_habitats, habitat_cite,
                                                            habitat_code_cite, syntaxon_valide, syntaxon_code_valide,
                                                            ref_phyto))
            elif select_object_name == 'habitat_recherche':
                select_habitat.view().pressed.connect(
                    lambda line: self.add_typo_habitats(line, select_habitat, survey_feature, found_habitats,
                                                        obs_habitats, table_syn_habitats, code_typo))
        if len(text_scripted) == 0:
            select_habitat.clear()

    @staticmethod
    def filter_habitat_list(code_typo, code_habitat_index, nom_habitat_simple_index, ref_habitat_values, text_chunks):
        found_habitats = None
        if code_typo == 0:
            if len(text_chunks) == 1 and len(text_chunks[0]) > 2:
                found_habitats = [habitat for habitat in ref_habitat_values if
                                  text_chunks[0].lower() == habitat[nom_habitat_simple_index][
                                                            :len(text_chunks[0])].lower()]
            elif len(text_chunks) > 1 and len(text_chunks[0]) > 2:
                found_habitats = [habitat for habitat in ref_habitat_values if
                                  text_chunks[0].lower() == habitat[nom_habitat_simple_index][
                                                            :len(text_chunks[0])].lower() and
                                  text_chunks[1].lower() in habitat[nom_habitat_simple_index][
                                                            len(text_chunks[0]):].lower()]
        else:
            if len(text_chunks) > 0 and len(text_chunks[0]) > 0:
                found_habitats = [habitat for habitat in ref_habitat_values if
                                  text_chunks[0].lower() == habitat[code_habitat_index][
                                                            :len(text_chunks[0])].lower()]
        return found_habitats

    @staticmethod
    def populate_select_habitat(code_habitat_index, code_habitat_retenu_index, found_habitats,
                                nom_habitat_simple_index, ref_phyto, select_habitat):
        select_habitat.clear()
        item_model = QStandardItemModel()
        for habitat in found_habitats:
            item = QStandardItem()
            if ref_phyto and habitat[code_habitat_index] == habitat[code_habitat_retenu_index]:
                item.setText(str(habitat[code_habitat_index]) + ' | ' + habitat[nom_habitat_simple_index])
                item.setFont(QFont('Tahoma', 8, QFont.Bold))
                item.setData(habitat[code_habitat_index])
                item_model.appendRow(item)
            else:
                item.setText(str(habitat[code_habitat_index]) + ' | ' + habitat[nom_habitat_simple_index])
                item.setData(habitat[code_habitat_index])
                item_model.appendRow(item)
        select_habitat.setModel(item_model)
        select_habitat.showPopup()

    @staticmethod
    def add_validated_habitat(line, select_habitat: QComboBox, habitats: List[QgsFeature], habitat_cite: QLineEdit,
                              habitat_code_cite: QLineEdit, syntaxon_valide: QLineEdit,
                              syntaxon_code_valide: QLineEdit, ref_phyto: bool):
        select_habitat.view().pressed.disconnect()
        habitat = habitats[line.row()]
        if ref_phyto:
            habitat_cite.setText(str(habitat[4] if habitat[4] else ''))
            habitat_code_cite.setText(str(habitat[2] if habitat[2] else ''))
            syntaxon_valide.setText(str(habitat[7] if habitat[7] else ''))
            syntaxon_code_valide.setText(str(habitat[6] if habitat[6] else ''))
        else:
            habitat_cite.setText(str(habitat[4] if habitat[4] else ''))
            habitat_code_cite.setText(str(habitat[3] if habitat[3] else ''))
            syntaxon_valide.setText('')
            syntaxon_code_valide.setText('')
        select_habitat.hidePopup()
        select_habitat.clear()

    def add_habitat(self, survey_feature: QgsFeature, obs_habitats: QgsVectorLayer, select_habitat: QComboBox,
                    obs_hab_nom_cite: QLineEdit, obs_hab_code_cite: QLineEdit, syntaxon_rec: QComboBox,
                    syntaxon_forme: QComboBox, table_habitats: QTableWidget, obs_hab_assoc_list: QListWidget,
                    add_widget: QLabel, save_widget: QLabel, typo_buttons: QButtonGroup):
        self.save_and_add_habitat(add_widget, None, obs_habitats, save_widget, select_habitat, survey_feature,
                                  obs_hab_code_cite, syntaxon_forme, obs_hab_nom_cite, syntaxon_rec,
                                  table_habitats, obs_hab_assoc_list, typo_buttons)

    def save_habitat(self, survey_feature: QgsFeature, obs_habitats: QgsVectorLayer, select_habitat: QComboBox,
                     obs_hab_nom_cite: QLineEdit, obs_hab_code_cite: QLineEdit, syntaxon_rec: QComboBox,
                     syntaxon_forme: QComboBox, table_habitats: QTableWidget, obs_hab_assoc_list: QListWidget,
                     add_widget: QLabel, save_widget: QLabel, add_survey_widgets: List[QLabel],
                     typo_buttons: QButtonGroup):
        code_phyto = obs_hab_code_cite.text() if obs_hab_code_cite else None
        if code_phyto:
            row = [i for i in range(table_habitats.rowCount())
                   if table_habitats.item(i, 1).text() == obs_hab_nom_cite.text()
                   and table_habitats.item(i, 0).text() == code_phyto][0]
            obs_habitat = [obs_habitat for obs_habitat in obs_habitats.getFeatures()
                           if obs_habitat['obs_hab_rel_id'] == survey_feature.id()
                           and obs_habitat['obs_hab_nom_cite'] == obs_hab_nom_cite.text()
                           and obs_habitat['obs_hab_code_cite'] == code_phyto][0]
        else:
            row = [i for i in range(table_habitats.rowCount())
                   if table_habitats.item(i, 1).text() == obs_hab_nom_cite.text()][0]
            obs_habitat = [obs_habitat for obs_habitat in obs_habitats.getFeatures()
                           if obs_habitat['obs_hab_rel_rel_id'] == survey_feature.id()
                           and obs_habitat['obs_hab_nom_cite'] == obs_hab_nom_cite.text()][0]

        success = self.save_and_add_habitat(add_widget, obs_habitat, obs_habitats, save_widget, select_habitat,
                                            survey_feature, obs_hab_code_cite, syntaxon_forme, obs_hab_nom_cite,
                                            syntaxon_rec, table_habitats, obs_hab_assoc_list, typo_buttons,
                                            add_survey_widgets)
        if success:
            table_habitats.removeRow(row)

    def save_and_add_habitat(self, add_widget, obs_habitat, obs_habitats, save_widget, select_habitat, survey_feature,
                             obs_hab_code_cite, syntaxon_forme, obs_hab_nom_cite, syntaxon_rec, table_habitats,
                             obs_hab_assoc_list, typo_buttons, add_survey_widgets=None) -> bool:
        if not self.check_if_code_habitat_exists(obs_habitat, obs_habitats, survey_feature, obs_hab_nom_cite.text(),
                                                 obs_hab_code_cite.text()):
            select_habitat.hidePopup()
            select_habitat.clear()
            return False

        if obs_hab_nom_cite.text():
            table_habitats.insertRow(table_habitats.rowCount())
            table_habitats.setItem(table_habitats.rowCount() - 1, 0,
                                   QTableWidgetItem(obs_hab_code_cite.text()))
            table_habitats.setItem(table_habitats.rowCount() - 1, 1, QTableWidgetItem(obs_hab_nom_cite.text()))
            table_habitats.setItem(table_habitats.rowCount() - 1, 2, QTableWidgetItem(syntaxon_rec.currentText()))
            table_habitats.setItem(table_habitats.rowCount() - 1, 3, QTableWidgetItem(syntaxon_forme.currentText()))
            table_habitats.setItem(table_habitats.rowCount() - 1, 6, SurveyDataManager.get_cross_item())
            self.save_habitat_observation(survey_feature, obs_habitats, 'obs_hab_rel_id', obs_habitat)
            self.refresh_habitat_sub_form(add_survey_widgets, add_widget, save_widget, select_habitat,
                                          obs_hab_assoc_list, typo_buttons)
            return True
        else:
            iface.messageBar().pushWarning('Ajout de syntaxon', 'Vous ne pouvez pas ajouter une observation sans '
                                                                'syntaxon cite.')
            return False

    def save_habitat_observation(self, survey_feature: QgsFeature, obs_habitats: QgsVectorLayer,
                                 linked_field_name: str = 'obs_hab_rel_id', obs_habitat: QgsFeature = None):
        if not obs_habitats.isEditable():
            obs_habitats.startEditing()
        if not obs_habitat:
            obs_habitat = QgsFeature()
            obs_habitat.setFields(obs_habitats.fields())
            obs_habitat[linked_field_name] = survey_feature['id']
            obs_habitats.updateFeature(obs_habitat)
            obs_habitats.addFeature(obs_habitat)
        self.save_layer_values(obs_habitat, obs_habitats)
        obs_habitats.commitChanges()

    def refresh_habitat_sub_form(self, add_survey_widgets, add_widget, save_widget, select_habitat, obs_hab_assoc_list,
                                 typo_buttons: QButtonGroup):
        select_habitat.clear()
        self.clean_habitat_sub_form()

        for button in typo_buttons.buttons():
            button.setEnabled(True)
        if obs_hab_assoc_list.count() > 0:
            obs_hab_assoc_list.itemChanged.disconnect()
            obs_hab_assoc_list.clear()

        if add_survey_widgets:
            for widget in add_survey_widgets:
                widget.hide()
        select_habitat.setEnabled(True)
        add_widget.show()
        save_widget.hide()

    def clean_habitat_sub_form(self):
        form_inputs = DatabaseSpecificationService.get_inputs_specification('obs_habitats')
        for form_input in form_inputs:
            widget = WidgetService.find_widget(self.form, form_input['input_type'],
                                               form_input['input_name'].split(',')[0])
            if widget and widget.objectName() != 'obs_hab_code_typo':
                TableWidgetService.clean_widget(widget)
                widget.setEnabled(True)

    @staticmethod
    def check_if_code_habitat_exists(obs_syntaxon, obs_habitats, survey_feature, obs_hab_nom_cite,
                                     obs_hab_code_cite, obs_hab_rel_field_name: str = 'obs_hab_rel_id') -> bool:
        observations_with_same_id = [observation for observation in obs_habitats.getFeatures()
                                     if observation[obs_hab_rel_field_name] == survey_feature.id()
                                     and observation['obs_hab_nom_cite'] == obs_hab_nom_cite
                                     and observation['obs_hab_code_cite'] == obs_hab_code_cite]
        if obs_syntaxon:
            if len(observations_with_same_id) > 0 \
                    and obs_syntaxon['obs_hab_code_cite'] != obs_hab_code_cite:
                iface.messageBar().pushWarning('Ajout d\'observation habitat',
                                               'Vous ne pouvez ajouter qu\'une observation par code habitat.')
                return False
        else:
            if len(observations_with_same_id) > 0:
                iface.messageBar().pushWarning('Ajout d\'observation habitat',
                                               'Une seule observation par code habitat est permise.')
                return False
        return True

    def get_habitats(self, feature_id: int, obs_habitats: QgsVectorLayer, table_habitats: QTableWidget,
                     obs_hab_assoc_list: QListWidget, buttons: QButtonGroup, obs_hab_code_typo: QLineEdit,
                     syntaxon_cplt: QFrame, syntaxon_data_add: QFrame, obs_hab_action_buttons: QGroupBox):
        code_typo: int = self.prepare_table_habitats(table_habitats, obs_hab_assoc_list, buttons, obs_hab_code_typo,
                                                     syntaxon_cplt, syntaxon_data_add, obs_hab_action_buttons)
        habitats = [observation for observation in obs_habitats.getFeatures()
                    if observation['obs_hab_rel_id'] == feature_id and observation['obs_hab_code_typo'] == code_typo]
        if len(habitats) > 0:
            type_forme_values = self.list_values_provider.get_values_from_list_name('TYPE_FORME_VALUES')
            strate_values = self.list_values_provider.get_values_from_list_name('STRATE_VALUES')
            for habitat in habitats:
                table_habitats.insertRow(table_habitats.rowCount())
                if habitat['obs_hab_code_cite'] not in (0, None, NULL):
                    table_habitats.setItem(table_habitats.rowCount() - 1, 0, QTableWidgetItem(
                        habitat['obs_hab_code_cite']))
                table_habitats.setItem(table_habitats.rowCount() - 1, 1, QTableWidgetItem(
                    habitat['obs_hab_nom_cite']))
                if habitat['obs_hab_forme_id'] not in (0, None, NULL):
                    table_habitats.setItem(table_habitats.rowCount() - 1, 3, QTableWidgetItem(
                        type_forme_values[habitat['obs_hab_forme_id']][1]))
                if habitat['obs_hab_recouvrement'] not in (0, None, NULL):
                    table_habitats.setItem(table_habitats.rowCount() - 1, 2,
                                           QTableWidgetItem(strate_values[habitat['obs_hab_recouvrement']][1]))
                table_habitats.setItem(table_habitats.rowCount() - 1, 6, SurveyDataManager.get_cross_item())

    def prepare_table_habitats(self, table_habitats: QTableWidget, obs_hab_assoc_list: QListWidget,
                               buttons: QButtonGroup, obs_hab_code_typo: QLineEdit, syntaxon_cplt: QFrame,
                               syntaxon_data_add: QFrame, obs_hab_action_buttons: QGroupBox) -> int:
        code_typo: int = TableWidgetService.get_code_typo(buttons)
        header_labels: List = TableWidgetService.get_header_labels_from_typo(code_typo)
        obs_hab_code_typo.setText(str(code_typo))
        widgets_to_display_or_not = [syntaxon_cplt, syntaxon_data_add, obs_hab_action_buttons]

        self.clean_habitat_sub_form()
        if code_typo == 0:
            for widget in widgets_to_display_or_not:
                widget.show()
        else:
            for widget in widgets_to_display_or_not:
                widget.hide()

        obs_hab_assoc_list.clear()
        table_habitats.clear()
        for i in range(table_habitats.rowCount()):
            table_habitats.removeRow(0)
        WidgetService.build_table(table_habitats, header_labels)
        return code_typo

    @staticmethod
    def get_syntaxon(form: QgsAttributeForm, obs_syntaxon_id: int, obs_habitats: QgsVectorLayer):
        obs_syntaxon_list = [observation for observation in obs_habitats.getFeatures()
                             if observation.id() == obs_syntaxon_id]
        if len(obs_syntaxon_list) > 0:
            obs_syntaxon = obs_syntaxon_list[0]
            form_inputs = DatabaseSpecificationService.get_inputs_specification('obs_habitats')
            for form_input in form_inputs:
                for form_input_name in form_input['input_name'].split(','):
                    widget = WidgetService.find_widget(form, form_input['input_type'], form_input_name.strip())
                    WidgetService.set_input_value(widget, obs_syntaxon[form_input['field_name']])

    def get_typo_habitats(self, survey_feature: QgsFeature, obs_habitats: QgsVectorLayer,
                          table_syn_habitats: QTableWidget):
        obs_syntaxon_id = survey_feature['rel_obs_syntaxon_id']
        linked_obs_hab_list = [row[0] for row in self.sqlite_data_manager.get_linked_obs_hab(obs_syntaxon_id)]
        associated_obs_habitats = obs_habitats.getFeatures(linked_obs_hab_list)
        for observation in associated_obs_habitats:
            row = table_syn_habitats.rowCount()
            code_typo = observation['obs_hab_code_typo']
            typo_widget = QTableWidgetItem(TableWidgetService.get_typo_label_from_code(code_typo))
            typo_widget.setBackground(TableWidgetService.get_typo_color_from_code(code_typo))
            table_syn_habitats.insertRow(row)
            table_syn_habitats.setItem(row, 0, QTableWidgetItem(observation['obs_hab_code_cite']))
            table_syn_habitats.setItem(row, 1, QTableWidgetItem(observation['obs_hab_nom_cite']))
            table_syn_habitats.setItem(row, 2, QTableWidgetItem(str(code_typo)))
            table_syn_habitats.setItem(row, 3, typo_widget)
            table_syn_habitats.setItem(row, 4, SurveyDataManager.get_cross_item())
        table_syn_habitats.sortItems(0, Qt.AscendingOrder)
        table_syn_habitats.sortItems(3, Qt.DescendingOrder)

    def add_typo_habitats(self, line, select_habitat: QComboBox, survey_feature: QgsFeature,
                          found_habitats: List[QgsFeature], obs_habitats: QgsVectorLayer,
                          table_syn_habitats: QTableWidget, code_typo: int):
        habitat = found_habitats[line.row()]
        obs_hab_nom_cite = str(habitat[4] if habitat[4] else '')
        obs_hab_code_cite = str(habitat[3] if habitat[3] else '')

        if not self.check_if_code_habitat_exists(None, obs_habitats, survey_feature, obs_hab_nom_cite,
                                                 obs_hab_code_cite, 'obs_hab_rel_assoc_id'):
            select_habitat.hidePopup()
            select_habitat.clear()
            return

        self.add_habitat_to_database(code_typo, obs_habitats, survey_feature, obs_hab_nom_cite, obs_hab_code_cite)
        self.add_habitat_to_table_widget(code_typo, table_syn_habitats, obs_hab_nom_cite, obs_hab_code_cite)

        obs_habitat = TableWidgetService.find_observation(obs_hab_code_cite, obs_hab_nom_cite, 'obs_hab_code_cite',
                                                          'obs_hab_nom_cite', 'obs_hab_rel_assoc_id', obs_habitats,
                                                          survey_feature)
        if obs_habitat:
            survey_id = survey_feature['id']
            obs_syntaxon_id: int
            if survey_id is None:
                survey_id = self.sqlite_data_manager.get_survey_id_from_geometry(survey_feature)
                obs_syntaxon_id = self.sqlite_data_manager.get_obs_syntaxon_id_from_survey_id(survey_id)
            else:
                obs_syntaxon_id = int(survey_feature['rel_obs_syntaxon_id'])
            obs_habitat_id = int(obs_habitat['obs_hab_id'])
            self.sqlite_data_manager.insert_links_between_obs_hab(obs_syntaxon_id, obs_habitat_id)

        select_habitat.hidePopup()
        select_habitat.view().pressed.disconnect()
        select_habitat.clear()

    def add_habitat_to_database(self, code_typo: int, obs_habitats: QgsVectorLayer, survey_feature: QgsFeature,
                                obs_hab_nom_cite: str, obs_hab_code_cite: str):
        if not obs_habitats.isEditable():
            obs_habitats.startEditing()
        survey_id = survey_feature['id']
        obs_syntaxon_id: int
        if survey_id is None:
            survey_id = self.sqlite_data_manager.get_survey_id_from_geometry(survey_feature)
            obs_syntaxon_id = self.sqlite_data_manager.get_obs_syntaxon_id_from_survey_id(survey_id)
        else:
            obs_syntaxon_id = survey_feature['rel_obs_syntaxon_id']
        obs_syntaxon = obs_habitats.getFeature(obs_syntaxon_id)
        obs_habitat = QgsFeature()
        obs_habitat.setFields(obs_habitats.fields())
        obs_habitat['obs_hab_rel_assoc_id'] = survey_id
        obs_habitat['obs_hab_nom_cite'] = obs_hab_nom_cite
        obs_habitat['obs_hab_code_cite'] = obs_hab_code_cite
        obs_habitat['obs_hab_code_typo'] = code_typo
        if obs_syntaxon is not None:
            obs_habitat['obs_hab_rel_id'] = obs_syntaxon['obs_hab_rel_id']
        obs_habitats.updateFeature(obs_habitat)
        obs_habitats.addFeature(obs_habitat)
        obs_habitats.commitChanges()

    @staticmethod
    def add_habitat_to_table_widget(code_typo: int, table_syn_habitats: QTableWidget, obs_hab_nom_cite: str,
                                    obs_hab_code_cite: str):
        row = table_syn_habitats.rowCount()
        typo_widget = QTableWidgetItem(TableWidgetService.get_typo_label_from_code(code_typo))
        typo_widget.setBackground(TableWidgetService.get_typo_color_from_code(code_typo))
        table_syn_habitats.insertRow(row)
        table_syn_habitats.setItem(row, 0, QTableWidgetItem(obs_hab_code_cite))
        table_syn_habitats.setItem(row, 1, QTableWidgetItem(obs_hab_nom_cite))
        table_syn_habitats.setItem(row, 2, QTableWidgetItem(str(code_typo)))
        table_syn_habitats.setItem(row, 3, typo_widget)
        table_syn_habitats.setItem(row, 4, SurveyDataManager.get_cross_item())
        table_syn_habitats.sortItems(0, Qt.AscendingOrder)
        table_syn_habitats.sortItems(3, Qt.DescendingOrder)

    def taxon_auto_complete(self, select_taxon: QComboBox, taxref: QgsVectorLayer, taxon_cite: QLineEdit,
                            cd_nom_cite: QLineEdit, taxon_valide: QLineEdit, cd_nom_valide: QLineEdit):
        found_taxons: Optional[List[QgsFeature]] = None
        text_scripted = select_taxon.lineEdit().text()
        select_taxon.hidePopup()
        taxref_field_names = [field.name() for field in taxref.fields()]
        text_chunks = text_scripted.split(' ')
        lb_nom_index = taxref_field_names.index('lb_nom')
        cd_nom_index = taxref_field_names.index('cd_nom')
        first_word = text_chunks[0]
        len_text_chunk_0 = len(first_word)

        if len_text_chunk_0 > 2:
            found_taxons = self.found_taxons_matching(first_word, len_text_chunk_0, text_chunks, lb_nom_index, taxref)
        if found_taxons and len(found_taxons) > 0:
            self.populate_taxon_list(cd_nom_index, found_taxons, lb_nom_index, select_taxon, taxref_field_names,
                                     taxon_cite, cd_nom_cite, taxon_valide, cd_nom_valide)
        if len(text_scripted) == 0:
            select_taxon.clear()

    def found_taxons_matching(self, first_word: str, len_text_chunk_0: int, text_chunks: List[str], lb_nom_index: int,
                              taxref: QgsVectorLayer) -> Optional[List[QgsFeature]]:
        found_taxons = None
        matching_taxon_ids = self.sqlite_data_manager.get_taxons_starting_with(first_word)
        matching_taxons = []
        for taxon_id in matching_taxon_ids:
            taxon = taxref.getFeature(taxon_id[0])
            matching_taxons.append(taxon.attributes())
        if len(text_chunks) == 1:
            found_taxons = matching_taxons
        if len(text_chunks) > 1:
            found_taxons = [taxon for taxon in matching_taxons if
                            text_chunks[1].lower() in taxon[lb_nom_index][len_text_chunk_0:].lower()]
        return found_taxons

    def populate_taxon_list(self, cd_nom_index, found_taxons, lb_nom_index, select_taxon, taxref_field_names,
                            taxon_cite, cd_nom_cite, taxon_valide, cd_nom_valide):
        item_model = QStandardItemModel()
        for taxon in found_taxons:
            item = QStandardItem()
            if taxon[cd_nom_index] == taxon[taxref_field_names.index('cd_ref')]:
                item.setData(str(taxon[cd_nom_index]) + ' | ' + taxon[lb_nom_index], role=Qt.DisplayRole)
                item.setFont(QFont('Tahoma', 8, QFont.Bold))
                item.setData(taxon[cd_nom_index])
                item_model.appendRow(item)
            else:
                item.setData(str(taxon[cd_nom_index]) + ' | ' + taxon[lb_nom_index], role=Qt.DisplayRole)
                item.setData(taxon[cd_nom_index])
                item_model.appendRow(item)
        select_taxon.clear()
        select_taxon.setModel(item_model)
        select_taxon.showPopup()
        select_taxon.view().pressed.connect(
            lambda line: self.add_validated_taxon(line, select_taxon, found_taxons, taxon_cite, cd_nom_cite,
                                                  taxon_valide, cd_nom_valide))

    @staticmethod
    def add_validated_taxon(line, select_taxon: QComboBox, taxons: List[QgsFeature], taxon_cite: QLineEdit,
                            cd_nom_cite: QLineEdit, taxon_valide: QLineEdit, cd_nom_valide: QLineEdit):
        select_taxon.view().pressed.disconnect()
        taxon = taxons[line.row()]
        taxon_cite.setText(str(taxon[2] if taxon[2] else ''))
        cd_nom_cite.setText(str(taxon[1] if taxon[1] else ''))
        taxon_valide.setText(str(taxon[4] if taxon[4] else ''))
        cd_nom_valide.setText(str(taxon[3] if taxon[3] else ''))
        select_taxon.hidePopup()
        select_taxon.clear()

    def add_taxon(self, survey_feature: QgsFeature, observations: QgsVectorLayer, select_taxon: QComboBox,
                  taxon_cite: QLineEdit, taxon_cd_nom_cite: QLineEdit, strates: List[QComboBox],
                  table_taxons: QTableWidget, add_widget: QLabel, save_widget: QLabel):
        self.save_and_add_taxon(add_widget, None, observations, save_widget, select_taxon, strates, survey_feature,
                                table_taxons, taxon_cd_nom_cite, taxon_cite)

    def save_taxon(self, survey_feature: QgsFeature, observations: QgsVectorLayer, select_taxon: QComboBox,
                   taxon_cite: QLineEdit, taxon_cd_nom_cite: QLineEdit, strates: List[QComboBox],
                   table_taxons: QTableWidget, add_widget: QLabel, save_widget: QLabel):
        cd_nom = int(taxon_cd_nom_cite.text()) if taxon_cd_nom_cite and taxon_cd_nom_cite.text() != '' else None
        if cd_nom:
            row = [i for i in range(table_taxons.rowCount())
                   if table_taxons.item(i, 1).text() == taxon_cite.text()
                   and table_taxons.item(i, 0).text() == str(cd_nom)][0]
            observation = [observation for observation in observations.getFeatures()
                           if observation['obs_rel_id'] == survey_feature.id()
                           and observation['cd_nom'] == cd_nom
                           and observation['nom_cite'] == taxon_cite.text()][0]
        else:
            row = [i for i in range(table_taxons.rowCount())
                   if table_taxons.item(i, 1).text() == taxon_cite.text()][0]
            observation = [observation for observation in observations.getFeatures()
                           if observation['obs_rel_id'] == survey_feature.id()
                           and observation['nom_cite'] == taxon_cite.text()][0]

        success = self.save_and_add_taxon(add_widget, observation, observations, save_widget, select_taxon, strates,
                                          survey_feature, table_taxons, taxon_cd_nom_cite, taxon_cite)

        if success:
            table_taxons.removeRow(row)

    def save_and_add_taxon(self, add_widget, observation, observations, save_widget, select_taxon, strates,
                           survey_feature, table_taxons, taxon_cd_nom_cite, taxon_cite) -> bool:
        if not self.check_if_cd_nom_exists(observation, observations, survey_feature, taxon_cite, taxon_cd_nom_cite):
            select_taxon.hidePopup()
            select_taxon.clear()
            return False

        if taxon_cite.text():
            table_taxons.insertRow(table_taxons.rowCount())
            table_taxons.setItem(table_taxons.rowCount() - 1, 0, QTableWidgetItem(taxon_cd_nom_cite.text()))
            table_taxons.setItem(table_taxons.rowCount() - 1, 1, QTableWidgetItem(taxon_cite.text()))
            table_taxons.setItem(table_taxons.rowCount() - 1, 6, SurveyDataManager.get_cross_item())
            for s in range(len(strates)):
                table_taxons.setItem(table_taxons.rowCount() - 1, 2 + s, QTableWidgetItem(strates[s].currentText()))

            self.save_taxon_observation(survey_feature, observations, observation)
            self.refresh_taxon_sub_form(add_widget, save_widget, select_taxon)
            return True
        else:
            iface.messageBar().pushWarning('Ajout de taxon', 'Vous ne pouvez pas ajouter une observation sans taxon '
                                                             'cite.')
            return False

    def save_taxon_observation(self, survey_feature: QgsFeature, observations: QgsVectorLayer,
                               observation: QgsFeature = None):
        if not observations.isEditable():
            observations.startEditing()
        if not observation:
            observation = QgsFeature()
            observation.setFields(observations.fields())
            observation['obs_rel_id'] = survey_feature['id']
            observations.updateFeature(observation)
            observations.addFeature(observation)
        self.save_layer_values(observation, observations)
        observations.commitChanges()

    def refresh_taxon_sub_form(self, add_widget, save_widget, select_taxon):
        select_taxon.clear()
        form_inputs = DatabaseSpecificationService.get_inputs_specification('observations')
        for form_input in form_inputs:
            widget = WidgetService.find_widget(self.form, form_input['input_type'],
                                               form_input['input_name'].split(',')[0])
            if widget:
                TableWidgetService.clean_widget(widget)
                widget.setEnabled(True)

        select_taxon.setEnabled(True)
        add_widget.show()
        save_widget.hide()

    @staticmethod
    def check_if_cd_nom_exists(observation, observations, survey_feature, nom_cite, taxon_cd_nom_cite) -> bool:
        cd_nom = int(taxon_cd_nom_cite.text()) if taxon_cd_nom_cite and taxon_cd_nom_cite.text() != '' else None
        observations_with_same_id = [observation for observation in observations.getFeatures()
                                     if observation['obs_rel_id'] == survey_feature.id()
                                     and observation['nom_cite'] == nom_cite.text()
                                     and observation['cd_nom'] == cd_nom]
        if observation:
            if len(observations_with_same_id) > 0 and observation['cd_nom'] != cd_nom:
                iface.messageBar().pushWarning('Ajout de taxon',
                                               'Vous ne pouvez ajouter qu\'une observation par cd_nom.')
                return False
        else:
            if len(observations_with_same_id) > 0:
                iface.messageBar().pushWarning('Ajout de taxon',
                                               'Une seule observation par cd_nom est permise.')
                return False
        return True

    def get_taxons(self, feature_id, observations: QgsVectorLayer, table_taxons: QTableWidget):
        taxa = [observation for observation in observations.getFeatures()
                if observation['obs_rel_id'] == feature_id]
        if len(taxa) > 0:
            strate_values = self.list_values_provider.get_values_from_list_name('STRATE_VALUES')
            for taxon in taxa:
                row = table_taxons.rowCount()
                table_taxons.insertRow(row)
                if taxon['cd_nom'] in (None, NULL):
                    table_taxons.setItem(row, 0, QTableWidgetItem(""))
                else:
                    table_taxons.setItem(row, 0, QTableWidgetItem(str(taxon['cd_nom'])))
                table_taxons.setItem(row, 1, QTableWidgetItem(taxon['nom_cite']))
                if taxon['strate_arbo'] not in (0, None, NULL):
                    table_taxons.setItem(row, 2, QTableWidgetItem(strate_values[taxon['strate_arbo']][1]))
                if taxon['strate_arbu'] not in (0, None, NULL):
                    table_taxons.setItem(row, 3, QTableWidgetItem(strate_values[taxon['strate_arbu']][1]))
                if taxon['strate_herb'] not in (0, None, NULL):
                    table_taxons.setItem(row, 4, QTableWidgetItem(strate_values[taxon['strate_herb']][1]))
                if taxon['strate_musc'] not in (0, None, NULL):
                    table_taxons.setItem(row, 5, QTableWidgetItem(strate_values[taxon['strate_musc']][1]))
                table_taxons.setItem(row, 6, SurveyDataManager.get_cross_item())

    @staticmethod
    def get_cross_item() -> QTableWidgetItem:
        cross_item = QTableWidgetItem("><")
        cross_item.setForeground(QBrush(QColor(240, 15, 15)))
        cross_item.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        font = QFont()
        font.setBold(True)
        cross_item.setFont(font)
        return cross_item

    def get_observers(self, survey_id: int, survey_observer_links: QgsVectorLayer, lst_obs: QComboBox,
                      obs_list: QListWidget):
        observers = self.sqlite_data_manager.get_observers()
        observer_list = [[observer[0], observer[1] + ' ' + observer[2] + ' - ' + observer[3]] for observer in observers]
        observer_list[:0] = [[0, NULL]]
        WidgetService.load_combo_box(lst_obs, observer_list)

        observer_ids = [link['obs_id'] for link in survey_observer_links.getFeatures() if link['rel_id'] == survey_id]
        if len(observer_ids) > 0:
            attached_observers = [[observer[0], observer[1]] for observer in observer_list
                                  if observer[0] in observer_ids]
            if len(attached_observers) > 0:
                for observer in attached_observers:
                    obs_list.addItem(observer[1])

    def add_observer(self, survey_id: int, survey_observer_links: QgsVectorLayer, lst_obs: QComboBox,
                     obs_list: QListWidget) -> bool:
        if not self.check_if_observer_id_exists(survey_observer_links, survey_id, lst_obs):
            lst_obs.setCurrentIndex(0)
            return False

        observer_id = lst_obs.itemData(lst_obs.currentIndex())
        if observer_id != 0:
            new_feature = QgsFeature(survey_observer_links.fields())
            new_feature['obs_id'] = observer_id
            new_feature['rel_id'] = survey_id
            if not survey_observer_links.isEditable():
                survey_observer_links.startEditing()
            survey_observer_links.addFeature(new_feature)
            survey_observer_links.commitChanges()
            obs_list.addItem(lst_obs.currentText())
            lst_obs.setCurrentIndex(0)
            return True
        return False

    @staticmethod
    def check_if_observer_id_exists(survey_observer_links, survey_id, lst_obs) -> bool:
        observer_id = int(lst_obs.currentData()) if lst_obs and lst_obs.currentIndex() != 0 else None
        links_with_same_id = [link for link in survey_observer_links.getFeatures()
                              if link['rel_id'] == survey_id and link['obs_id'] == observer_id]

        if len(links_with_same_id) > 0:
            iface.messageBar().pushWarning('Ajout d\'observateur',
                                           'Un observateur ne peut pas figurer plus d\'une fois.')
            return False
        return True

    @staticmethod
    def delete_observer(survey_id: int, survey_observer_links: QgsVectorLayer, obs_list: QListWidget,
                        observateurs: QgsVectorLayer):
        items = obs_list.selectedItems()
        if len(items) > 0:
            for item in items:
                observer_id = [observer['obs_id'] for observer in observateurs.getFeatures()
                               if observer['obs_email'] == item.text().split(' - ')[1]][0]
                survey_observer_link_list = [link for link in survey_observer_links.getFeatures()
                                             if link['obs_id'] == observer_id and link['rel_id'] == survey_id]
                if len(survey_observer_link_list) > 0:
                    if not survey_observer_links.isEditable():
                        survey_observer_links.startEditing()
                    survey_observer_links.deleteFeature(survey_observer_link_list[0].id())
                    survey_observer_links.commitChanges()
                obs_list.takeItem(obs_list.row(item))

    def get_strates(self, survey_id: int, survey_strate_links: QgsVectorLayer, table_strates: QTableWidget):
        link_ids = [result[0] for result in self.sqlite_data_manager.get_linked_strates_to_survey(survey_id)]
        existing_strates = [link for link in survey_strate_links.getFeatures(link_ids)]
        if len(existing_strates) > 0:
            for strate in existing_strates:
                row = table_strates.rowCount()
                table_strates.insertRow(row)
                strate_id = strate['strate_id']
                strate_name = self.sqlite_data_manager.get_strate_name(strate_id)
                if strate_id not in (0, None, NULL):
                    table_strates.setItem(row, 0, QTableWidgetItem(str(strate_id)))
                    table_strates.setItem(row, 1, QTableWidgetItem(strate_name))
                if strate['recouvrement'] not in (None, NULL):
                    table_strates.setItem(row, 2, QTableWidgetItem(str(strate['recouvrement'])))
                if strate['hauteur_min'] not in (None, NULL):
                    table_strates.setItem(row, 3, QTableWidgetItem(str(strate['hauteur_min'])))
                if strate['hauteur_modale'] not in (None, NULL):
                    table_strates.setItem(row, 4, QTableWidgetItem(str(strate['hauteur_modale'])))
                if strate['hauteur_max'] not in (None, NULL):
                    table_strates.setItem(row, 5, QTableWidgetItem(str(strate['hauteur_max'])))
                table_strates.setItem(row, 6, SurveyDataManager.get_cross_item())

    def add_strate(self, survey_id: int, survey_strate_links: QgsVectorLayer, table_strates: QTableWidget,
                   strates_combo_box: QComboBox, struct_rec_pct_strate: QLineEdit, struct_haut_mod_strate: QLineEdit,
                   struct_haut_min_strate: QLineEdit, struct_haut_max_strate: QLineEdit) -> bool:
        if not self.check_if_strate_id_exists(None, survey_strate_links, survey_id, strates_combo_box):
            return False

        if strates_combo_box.itemData(strates_combo_box.currentIndex()) != 0:
            row = table_strates.rowCount()
            table_strates.insertRow(row)
            table_strates.setItem(row, 0, QTableWidgetItem(str(strates_combo_box.currentData())))
            table_strates.setItem(row, 1, QTableWidgetItem(strates_combo_box.currentText()))
            table_strates.setItem(row, 2, QTableWidgetItem(struct_rec_pct_strate.text()))
            table_strates.setItem(row, 3, QTableWidgetItem(struct_haut_min_strate.text()))
            table_strates.setItem(row, 4, QTableWidgetItem(struct_haut_mod_strate.text()))
            table_strates.setItem(row, 5, QTableWidgetItem(struct_haut_max_strate.text()))
            table_strates.setItem(row, 6, SurveyDataManager.get_cross_item())
            self.save_link_between_strate_and_survey(survey_id, survey_strate_links)
            self.refresh_strate_sub_form(strates_combo_box, struct_rec_pct_strate, struct_haut_mod_strate,
                                         struct_haut_min_strate, struct_haut_max_strate)
        else:
            iface.messageBar().pushWarning('Ajout de strate', 'Vous ne pouvez pas ajouter une ligne sans strate.')
            return False

    def save_link_between_strate_and_survey(self, survey_id: int, survey_strate_links: QgsVectorLayer,
                                            survey_strate_link: QgsFeature = None):
        if not survey_strate_links.isEditable():
            survey_strate_links.startEditing()
        if not survey_strate_link:
            survey_strate_link = QgsFeature()
            survey_strate_link.setFields(survey_strate_links.fields())
            survey_strate_link['rel_id'] = survey_id
            survey_strate_links.updateFeature(survey_strate_link)
            survey_strate_links.addFeature(survey_strate_link)
        self.save_layer_values(survey_strate_link, survey_strate_links)
        survey_strate_links.commitChanges()

    @staticmethod
    def refresh_strate_sub_form(strates_combo_box: QComboBox, struct_rec_pct_strate: QLineEdit,
                                struct_haut_mod_strate: QLineEdit, struct_haut_min_strate: QLineEdit,
                                struct_haut_max_strate: QLineEdit):
        widgets_to_clear = [strates_combo_box, struct_rec_pct_strate, struct_haut_mod_strate, struct_haut_min_strate,
                            struct_haut_max_strate]
        for widget in widgets_to_clear:
            TableWidgetService.clean_widget(widget)
            widget.setEnabled(True)

    @staticmethod
    def check_if_strate_id_exists(observation, observations, survey_id, strates_combo_box) -> bool:
        strate_id = int(strates_combo_box.currentData()) \
            if strates_combo_box and strates_combo_box.currentIndex() != 0 else None
        observations_with_same_id = [observation for observation in observations.getFeatures()
                                     if observation['rel_id'] == survey_id and observation['strate_id'] == strate_id]
        if observation:
            if len(observations_with_same_id) > 0 and observation['strate_id'] != strate_id:
                iface.messageBar().pushWarning('Ajout de taxon',
                                               'Vous ne pouvez ajouter qu\'une ligne par type de strate.')
                return False
        else:
            if len(observations_with_same_id) > 0:
                iface.messageBar().pushWarning('Ajout de taxon',
                                               'Une seule ligne de strate est permise.')
                return False
        return True

    def add_associated_survey(self, button: QLabel, releves: QgsVectorLayer, survey_feature: QgsFeature):
        target_layer: Optional[QgsVectorLayer] = None
        if button.objectName() == 'rel_create_new_polygon_rel':
            target_layer = [layer for layer in self.project.mapLayers(True).values()
                            if layer.name() == 'station_surface'][0]
        elif button.objectName() == 'rel_create_new_ligne_rel':
            target_layer = [layer for layer in self.project.mapLayers(True).values()
                            if layer.name() == 'station_ligne'][0]
        elif button.objectName() == 'rel_create_new_point_rel':
            target_layer = [layer for layer in self.project.mapLayers(True).values()
                            if layer.name() == 'station_point'][0]

        if target_layer:
            if not target_layer.isEditable():
                target_layer.startEditing()
            iface.setActiveLayer(target_layer)
            target_layer.featureAdded.connect(
                lambda fid: SurveyDataManager.feature_added(fid, releves, survey_feature))
            iface.actionAddFeature().trigger()
            self.form.window().close()

    @staticmethod
    def feature_added(fid, releves: QgsVectorLayer, parent_feature: QgsFeature):
        layer = iface.mapCanvas().currentLayer()
        layer.featureAdded.disconnect()
        new_geometry_feature: QgsFeature = layer.getFeature(fid)
        uuid_field_name: str
        if layer.name() == 'station_surface':
            uuid_field_name = 'rel_polygon_uuid'
        elif layer.name() == 'station_ligne':
            uuid_field_name = 'rel_ligne_uuid'
        elif layer.name() == 'station_point':
            uuid_field_name = 'rel_point_uuid'
        else:
            return
        new_survey_feature: QgsFeature = [feature for feature in releves.getFeatures()
                                          if feature[uuid_field_name] == new_geometry_feature['uuid']][0]
        new_survey_feature['rel_parent_id'] = parent_feature.id()
        releves.updateFeature(new_survey_feature)
        releves.commitChanges()
        releves.startEditing()

    def add_survey_associated_to_syntaxon(self, button: QLabel, releves: QgsVectorLayer, obs_habitats: QgsVectorLayer,
                                          survey_feature: QgsFeature, syntaxon_nom_cite: QLineEdit,
                                          syntaxon_code_phyto_cite: QLineEdit):
        target_layer: Optional[QgsVectorLayer] = None
        if button.objectName() == 'rel_syntax_create_new_polygon_rel':
            target_layer = [layer for layer in self.project.mapLayers(True).values()
                            if layer.name() == 'station_surface'][0]
        elif button.objectName() == 'rel_syntax_create_new_ligne_rel':
            target_layer = [layer for layer in self.project.mapLayers(True).values()
                            if layer.name() == 'station_ligne'][0]
        elif button.objectName() == 'rel_syntax_create_new_point_rel':
            target_layer = [layer for layer in self.project.mapLayers(True).values()
                            if layer.name() == 'station_point'][0]

        if target_layer:
            if not target_layer.isEditable():
                target_layer.startEditing()
            iface.setActiveLayer(target_layer)
            parent_code_phyto = syntaxon_code_phyto_cite.text()
            parent_syntaxon = syntaxon_nom_cite.text()
            target_layer.featureAdded.connect(
                lambda fid: SurveyDataManager.feature_associated_to_syntaxon(fid, releves, obs_habitats,
                                                                             survey_feature.id(), parent_syntaxon,
                                                                             parent_code_phyto))
            iface.actionAddFeature().trigger()
            self.form.window().close()

    @staticmethod
    def feature_associated_to_syntaxon(fid, releves: QgsVectorLayer, obs_habitats: QgsVectorLayer,
                                       parent_id: int, parent_syntaxon: str, parent_code_phyto: str):
        layer = iface.mapCanvas().currentLayer()
        layer.featureAdded.disconnect()
        new_geometry_feature: QgsFeature = layer.getFeature(fid)
        uuid_field_name: str
        if layer.name() == 'station_surface':
            uuid_field_name = 'rel_polygon_uuid'
        elif layer.name() == 'station_ligne':
            uuid_field_name = 'rel_ligne_uuid'
        elif layer.name() == 'station_point':
            uuid_field_name = 'rel_point_uuid'
        else:
            return
        new_survey_feature: QgsFeature = [feature for feature in releves.getFeatures()
                                          if feature[uuid_field_name] == new_geometry_feature['uuid']][0]
        if parent_code_phyto == '':
            obs_syntaxon_list = [obs_syntaxon for obs_syntaxon in obs_habitats.getFeatures()
                                 if obs_syntaxon['obs_hab_rel_id'] == parent_id
                                 and obs_syntaxon['obs_hab_nom_cite'] == parent_syntaxon]
        else:
            obs_syntaxon_list = [obs_syntaxon for obs_syntaxon in obs_habitats.getFeatures()
                                 if obs_syntaxon['obs_hab_rel_id'] == parent_id
                                 and obs_syntaxon['obs_hab_code_cite'] == parent_code_phyto
                                 and obs_syntaxon['obs_hab_nom_cite'] == parent_syntaxon]
        if len(obs_syntaxon_list) > 0:
            obs_syntaxon = obs_syntaxon_list[0]
            obs_syntaxon['obs_hab_rel_assoc_id'] = new_survey_feature.id()
            new_survey_feature['rel_obs_syntaxon_id'] = obs_syntaxon.id()
            obs_habitats.updateFeature(obs_syntaxon)
            obs_habitats.commitChanges()
            releves.updateFeature(new_survey_feature)
            releves.commitChanges()
            releves.startEditing()

    def save_survey(self, survey_layer: QgsVectorLayer, survey_feature: QgsFeature, obs_habitats: QgsVectorLayer):
        if not self.layer.isEditable():
            self.layer.startEditing()
        if not survey_layer.isEditable():
            survey_layer.startEditing()
        self.form.save()

        self.save_layer_values(survey_feature, survey_layer)
        if survey_feature['id'] is None:
            survey_feature = survey_layer.getFeature(
                self.sqlite_data_manager.get_survey_id_from_geometry(survey_feature))
        if self.sqlite_data_manager.obs_syntaxon_id_exists(survey_feature['id']):
            obs_syntaxon = [obs_syntaxon for obs_syntaxon in obs_habitats.getFeatures()
                            if obs_syntaxon['obs_hab_rel_assoc_id'] == survey_feature['id']][0]
            survey_feature['rel_obs_syntaxon_id'] = obs_syntaxon['obs_hab_id']
            survey_layer.updateFeature(survey_feature)
            self.save_habitat_observation(survey_feature, obs_habitats, 'obs_hab_rel_assoc_id', obs_syntaxon)
        elif survey_feature['rel_type_rel_id'] != 3:
            self.save_habitat_observation(survey_feature, obs_habitats, 'obs_hab_rel_assoc_id')
            obs_syntaxon = [obs_syntaxon for obs_syntaxon in obs_habitats.getFeatures()
                            if obs_syntaxon['obs_hab_rel_assoc_id'] == survey_feature['id']][0]
            survey_feature['rel_obs_syntaxon_id'] = obs_syntaxon['obs_hab_id']
            survey_layer.updateFeature(survey_feature)

        survey_layer.commitChanges()
        obs_habitats.commitChanges()
        self.layer.commitChanges()
        iface.messageBar().pushInfo('Sauvegarde releve', 'Effectuee.')

    def activate_survey(self, frel: QFrame, fobs: QFrame, save_rel: QLabel, activate: QLabel,
                        survey_layer: QgsVectorLayer, survey_feature: QgsFeature, obs_habitats: QgsVectorLayer):
        if not self.layer.isEditable():
            self.layer.startEditing()
        if not survey_layer.isEditable():
            survey_layer.startEditing()

        rel_type_widget = WidgetService.find_widget(self.form, 'QComboBox', 'rel_type_rel_id')
        rel_protocol_widget = WidgetService.find_widget(self.form, 'QComboBox', 'rel_protocole_id')
        rel_type_data = rel_type_widget.currentData()
        rel_protocol_data = rel_protocol_widget.currentData()

        if rel_type_data != 0 and rel_protocol_data != 0:
            self.form.save()
            rel_type_widget.setCurrentIndex(rel_type_widget.findData(rel_type_data))
            rel_type_widget.setEnabled(False)
            rel_protocol_widget.setCurrentIndex(rel_protocol_widget.findData(rel_protocol_data))
            self.save_survey(survey_layer, survey_feature, obs_habitats)
            frel.show()
            fobs.hide()
            save_rel.show()
            activate.hide()

    def map_survey(self, survey_layer: QgsVectorLayer, survey_feature: QgsFeature):
        if not self.layer.isEditable():
            self.layer.startEditing()
        if not survey_layer.isEditable():
            survey_layer.startEditing()
        self.form.save()

        form_inputs = DatabaseSpecificationService.get_inputs_specification('releves')
        for form_input in form_inputs:
            for form_input_name in form_input['input_name'].split(','):
                widget = WidgetService.find_widget(self.form, form_input['input_type'], form_input_name.strip())
                WidgetService.set_input_value(widget, survey_feature[form_input['field_name']])

    def save_layer_values(self, feature: QgsFeature, layer: QgsVectorLayer):
        form_inputs = DatabaseSpecificationService.get_inputs_specification(layer.name())
        for form_input in form_inputs:
            for form_input_name in form_input['input_name'].split(','):
                widget = WidgetService.find_widget(self.form, form_input['input_type'], form_input_name.strip())
                if widget:
                    value = WidgetService.get_input_value(widget)
                    if value != '' and value != 0:
                        feature.setAttribute(form_input['field_name'], value)
        layer.updateFeature(feature)




class ConnectionHelper:
    def __init__(self, iface: QgisInterface):
        self.iface = iface

    def test_authentication_config(self) -> bool:
        auth_manager: QgsAuthManager = QgsApplication.authManager()
        config_ids: List[str] = auth_manager.configIds()
        try:
            config_ids.index(AUTH_CONFIG)
        except ValueError:
            self.iface.messageBar().pushWarning('Authentification', 'votre profil Lobelia n\'est pas défini')
            return False
        self.iface.messageBar().pushInfo('Authentification', 'votre profil Lobelia est bien connu')
        return True

    def create_authentication_profile(self):
        auth_manager = QgsApplication.authManager()
        config = QgsAuthMethodConfig()
        config.setMethod('Basic')
        config.setName('lobelia_user')
        config.setId(AUTH_CONFIG)
        config.setConfig('username', 'your_email')
        config.setConfig('password', 'your_lobelia_password')
        auth_manager.storeAuthenticationConfig(config)
        self.iface.messageBar().pushInfo('Authentification', 'un exemple de profil Lobelia a été créé')

    @staticmethod
    def sqlite_connection(project: QgsProject) -> Optional[Connection]:
        connection: Optional[Connection] = None
        try:
            connection = sqlite3.connect(project.homePath() + "\\" + SQLITE_FILE)
            connection.enable_load_extension(True)
            connection.execute('SELECT load_extension("mod_spatialite")')
            connection.execute('SELECT InitSpatialMetaData(1);')
            connection.row_factory = sqlite3.Row
            # connection.text_factory = lambda x: x.decode("utf-8") # or str or bytes... # TODO needed ?
            connection.enable_load_extension(False)
        finally:
            return connection




class SurveyFormManager:
    def __init__(self, form: QgsAttributeForm, layer: QgsVectorLayer, feature: QgsFeature):
        self.form = form
        self.layer = layer
        self.feature = feature
        self.project: QgsProject = QgsProject.instance()
        self.data_manager: SurveyDataManager = SurveyDataManager(self.project, form, layer, feature)
        self.geometry_manager: GeometryManager = GeometryManager(layer, feature)
        self.list_values_provider: ListValuesProvider = ListValuesProvider(self.project)
        self.survey_feature: Optional[QgsFeature] = None

        # TODO replace this enumeration of widgets by a big dict, populate by specifications ?
        # global variables to store widgets
        self.frel: Optional[QFrame] = None
        self.fobs: Optional[QFrame] = None
        self.fobs_taxons: Optional[QFrame] = None
        self.ftypo_taxons: Optional[QFrame] = None
        self.fobs_syntaxons: Optional[QFrame] = None
        self.ftypo_syntaxons: Optional[QFrame] = None
        self.frame_jdd: Optional[QFrame] = None
        self.frame_syntaxon_complement: Optional[QFrame] = None
        self.frame_syntaxon_data_add: Optional[QFrame] = None

        self.labrel: Optional[QLabel] = None
        self.labobs: Optional[QLabel] = None
        self.save_rel: Optional[QLabel] = None
        self.activate: Optional[QLabel] = None
        self.add_taxon: Optional[QLabel] = None
        self.save_taxon: Optional[QLabel] = None
        self.add_syntaxon: Optional[QLabel] = None
        self.save_syntaxon: Optional[QLabel] = None
        self.cal1_label: Optional[QLabel] = None
        self.cal2_label: Optional[QLabel] = None
        self.rel_create_new_polygon_rel: Optional[QLabel] = None
        self.rel_create_new_line_rel: Optional[QLabel] = None
        self.rel_create_new_point_rel: Optional[QLabel] = None
        self.rel_syntax_create_new_polygon_rel: Optional[QLabel] = None
        self.rel_syntax_create_new_line_rel: Optional[QLabel] = None
        self.rel_syntax_create_new_point_rel: Optional[QLabel] = None
        self.lab_alerte_structure: Optional[QLabel] = None
        self.lab_alerte_desc_gen: Optional[QLabel] = None
        self.add_obs_btn: Optional[QLabel] = None
        self.suppr_obs_btn: Optional[QLabel] = None
        self.add_strate_struct: Optional[QLabel] = None

        self.rel_date_min: Optional[QLineEdit] = None
        self.rel_date_max: Optional[QLineEdit] = None
        self.taxon_cite: Optional[QLineEdit] = None
        self.taxon_valide: Optional[QLineEdit] = None
        self.taxon_cd_nom_cite: Optional[QLineEdit] = None
        self.taxon_cd_nom_valide: Optional[QLineEdit] = None
        self.obs_hab_nom_retenu: Optional[QLineEdit] = None
        self.obs_hab_code_retenu: Optional[QLineEdit] = None
        self.obs_hab_nom_cite: Optional[QLineEdit] = None
        self.obs_hab_code_cite: Optional[QLineEdit] = None
        self.syntaxon_retenu: Optional[QLineEdit] = None
        self.syntaxon_code_retenu: Optional[QLineEdit] = None
        self.syntaxon_cite: Optional[QLineEdit] = None
        self.syntaxon_code_cite: Optional[QLineEdit] = None
        self.obs_hab_rec_pct: Optional[QLineEdit] = None
        self.obs_hab_code_typo: Optional[QLineEdit] = None
        self.struct_rec_pct_strate: Optional[QLineEdit] = None
        self.struct_haut_mod_strate: Optional[QLineEdit] = None
        self.struct_haut_min_strate: Optional[QLineEdit] = None
        self.struct_haut_max_strate: Optional[QLineEdit] = None
        self.taxon_dominant_code: Optional[QLineEdit] = None
        self.taxon_dominant_nom: Optional[QLineEdit] = None
        self.obs_hab_taxon_dominant_code: Optional[QLineEdit] = None
        self.obs_hab_taxon_dominant_nom: Optional[QLineEdit] = None

        self.ctrl: Optional[QGroupBox] = None
        self.structure: Optional[QGroupBox] = None
        self.desc_gen: Optional[QGroupBox] = None
        self.obs_hab_action_buttons: Optional[QGroupBox] = None

        self.habitat_buttons: Optional[QButtonGroup] = None
        self.typo_habitat_buttons: Optional[QButtonGroup] = None

        self.observer_list: Optional[QListWidget] = None
        self.obs_hab_assoc_list: Optional[QListWidget] = None

        self.lst_obs: Optional[QComboBox] = None
        self.select_taxon: Optional[QComboBox] = None
        self.select_habitat: Optional[QComboBox] = None
        self.select_strate_struct: Optional[QComboBox] = None
        self.obs_hab_recouvrement: Optional[QComboBox] = None
        self.strate_arbo: Optional[QComboBox] = None
        self.strate_arbu: Optional[QComboBox] = None
        self.strate_herb: Optional[QComboBox] = None
        self.strate_musc: Optional[QComboBox] = None
        self.rel_type_rel_id: Optional[QComboBox] = None
        self.rel_protocole_id: Optional[QComboBox] = None
        self.syntaxon_recherche: Optional[QComboBox] = None
        self.habitat_recherche: Optional[QComboBox] = None
        self.obs_hab_forme: Optional[QComboBox] = None
        self.syntaxon_comm_non_sat_obs: Optional[QComboBox] = None
        self.taxon_dominant_recherche: Optional[QComboBox] = None
        self.select_taxon_dominant: Optional[QComboBox] = None

        self.syntaxon_diag_originale: Optional[QCheckBox] = None

        self.cal1: QMessageBox = QMessageBox()
        self.cal2: QMessageBox = QMessageBox()

        self.table_taxons: Optional[QTableWidget] = None
        self.table_habitats: Optional[QTableWidget] = None
        self.table_syn_habitats: Optional[QTableWidget] = None
        self.table_strates_struct: Optional[QTableWidget] = None

        self.tab_widget: Optional[QTabWidget] = None

        self.releves: Optional[QgsVectorLayer] = None
        self.observations: Optional[QgsVectorLayer] = None
        self.obs_habitats: Optional[QgsVectorLayer] = None
        self.liens_releve_observateur: Optional[QgsVectorLayer] = None
        self.liens_releve_strate: Optional[QgsVectorLayer] = None
        self.liens_obs_habitat: Optional[QgsVectorLayer] = None
        self.observateurs: Optional[QgsVectorLayer] = None
        self.catalogue_phyto: Optional[QgsVectorLayer] = None
        self.habref: Optional[QgsVectorLayer] = None
        self.taxref: Optional[QgsVectorLayer] = None

    def initialize(self):
        self.layer.startEditing()
        self.populate_variables()
        self.populate_layers()
        self.populate_combo_boxes()

        strates = [self.strate_arbo, self.strate_arbu, self.strate_herb, self.strate_musc]
        add_survey_to_syntaxon_widgets = [self.rel_syntax_create_new_point_rel, self.rel_syntax_create_new_line_rel,
                                          self.rel_syntax_create_new_polygon_rel]

        if self.releves is not None:
            self.survey_feature = self.geometry_manager.get_survey_feature(self.releves)
        else:
            iface.messageBar().pushWarning('Layer error', 'la couche releves n\'est pas disponible')

        self.prepare_table_widget(add_survey_to_syntaxon_widgets)
        self.populate_form()
        self.activate_event_listeners(strates, add_survey_to_syntaxon_widgets)
        self.prepare_startup_interface(add_survey_to_syntaxon_widgets)

    def populate_form(self):
        feature_id = 0
        if self.survey_feature:
            feature_id = self.survey_feature.id()
        if feature_id > 0:
            self.data_manager.map_survey(self.releves, self.survey_feature)
            self.data_manager.get_observers(feature_id, self.liens_releve_observateur, self.lst_obs, self.observer_list)
            self.data_manager.get_taxons(feature_id, self.observations, self.table_taxons)
            self.data_manager.get_syntaxon(self.form, self.survey_feature['rel_obs_syntaxon_id'], self.obs_habitats)
            self.data_manager.get_habitats(feature_id, self.obs_habitats, self.table_habitats, self.obs_hab_assoc_list,
                                           self.habitat_buttons, self.obs_hab_code_typo, self.frame_syntaxon_complement,
                                           self.frame_syntaxon_data_add, self.obs_hab_action_buttons)
            self.data_manager.get_typo_habitats(self.survey_feature, self.obs_habitats, self.table_syn_habitats)
            self.data_manager.get_strates(feature_id, self.liens_releve_strate, self.table_strates_struct)
        else:
            self.rel_type_rel_id.setCurrentIndex(self.rel_type_rel_id.findData(0))

    def populate_variables(self):
        # TODO replace this enumeration by a loop reading specifications
        self.frel = self.form.findChild(QFrame, "frel")
        self.fobs = self.form.findChild(QFrame, "fobs")
        self.fobs_taxons = self.form.findChild(QFrame, "frame_obs_taxons")
        self.ftypo_taxons = self.form.findChild(QFrame, "frame_typologie_phyto")
        self.fobs_syntaxons = self.form.findChild(QFrame, "frame_obs_habitats")
        self.ftypo_syntaxons = self.form.findChild(QFrame, "frame_typologie_symphyto")
        self.frame_jdd = self.form.findChild(QFrame, "frame_jdd")
        self.frame_syntaxon_complement = self.form.findChild(QFrame, "frame_syntaxon_complement")
        self.frame_syntaxon_data_add = self.form.findChild(QFrame, "frame_obs_hab_data_add_syntaxon")
        self.labrel = self.form.findChild(QLabel, "lab_form_rel")
        self.labobs = self.form.findChild(QLabel, "lab_form_obs")
        self.save_rel = self.form.findChild(QLabel, "save_releve")
        self.activate = self.form.findChild(QLabel, "edit_releve")
        self.add_taxon = self.form.findChild(QLabel, "add_taxon")
        self.save_taxon = self.form.findChild(QLabel, "save_taxon")
        self.add_syntaxon = self.form.findChild(QLabel, "lb_add_syntaxon")
        self.save_syntaxon = self.form.findChild(QLabel, "lb_save_syntaxon")
        self.cal1_label = self.form.findChild(QLabel, "calendrier_min")
        self.cal2_label = self.form.findChild(QLabel, "calendrier_max")
        self.rel_create_new_polygon_rel = self.form.findChild(QLabel, "rel_create_new_polygon_rel")
        self.rel_create_new_line_rel = self.form.findChild(QLabel, "rel_create_new_ligne_rel")
        self.rel_create_new_point_rel = self.form.findChild(QLabel, "rel_create_new_point_rel")
        self.rel_syntax_create_new_polygon_rel = self.form.findChild(QLabel, "rel_syntax_create_new_polygon_rel")
        self.rel_syntax_create_new_line_rel = self.form.findChild(QLabel, "rel_syntax_create_new_ligne_rel")
        self.rel_syntax_create_new_point_rel = self.form.findChild(QLabel, "rel_syntax_create_new_point_rel")
        self.lab_alerte_structure = self.form.findChild(QLabel, "lab_alerte_structure")
        self.lab_alerte_desc_gen = self.form.findChild(QLabel, "lab_alerte_desc_gen")
        self.add_obs_btn = self.form.findChild(QLabel, "add_obs_btn")
        self.suppr_obs_btn = self.form.findChild(QLabel, "suppr_obs_btn")
        self.add_strate_struct = self.form.findChild(QLabel, "add_strate_struct")
        self.rel_date_min = self.form.findChild(QLineEdit, "rel_date_min")
        self.rel_date_max = self.form.findChild(QLineEdit, "rel_date_max")
        self.taxon_cite = self.form.findChild(QLineEdit, "nom_cite")
        self.taxon_valide = self.form.findChild(QLineEdit, "nom_valide")
        self.taxon_cd_nom_cite = self.form.findChild(QLineEdit, "cd_nom_cite")
        self.taxon_cd_nom_valide = self.form.findChild(QLineEdit, "cd_nom_valide")
        self.obs_hab_nom_retenu = self.form.findChild(QLineEdit, "obs_habitat_nom_retenu")
        self.obs_hab_code_retenu = self.form.findChild(QLineEdit, "obs_habitat_code_retenu")
        self.obs_hab_nom_cite = self.form.findChild(QLineEdit, "obs_habitat_nom_cite")
        self.obs_hab_code_cite = self.form.findChild(QLineEdit, "obs_habitat_code_cite")
        self.syntaxon_retenu = self.form.findChild(QLineEdit, "syntaxon_retenu")
        self.syntaxon_code_retenu = self.form.findChild(QLineEdit, "syntaxon_code_retenu")
        self.syntaxon_cite = self.form.findChild(QLineEdit, "syntaxon_cite")
        self.syntaxon_code_cite = self.form.findChild(QLineEdit, "syntaxon_code_cite")
        self.obs_hab_rec_pct = self.form.findChild(QLineEdit, "obs_hab_rec_pct")
        self.obs_hab_code_typo = self.form.findChild(QLineEdit, "obs_hab_code_typo")
        self.struct_rec_pct_strate = self.form.findChild(QLineEdit, "strate_rec_pct")
        self.struct_haut_mod_strate = self.form.findChild(QLineEdit, "strate_haut_mod")
        self.struct_haut_min_strate = self.form.findChild(QLineEdit, "strate_haut_min")
        self.struct_haut_max_strate = self.form.findChild(QLineEdit, "strate_haut_max")
        self.taxon_dominant_code = self.form.findChild(QLineEdit, "syntaxon_taxon_dominant_cns_code")
        self.taxon_dominant_nom = self.form.findChild(QLineEdit, "syntaxon_taxon_dominant_cns_nom")
        self.obs_hab_taxon_dominant_code = self.form.findChild(QLineEdit, "syn_comm_non_sat_taxon_code")
        self.obs_hab_taxon_dominant_nom = self.form.findChild(QLineEdit, "syn_comm_non_sat_taxon_nom")
        self.ctrl = self.form.findChild(QGroupBox, 'typo_groupbox')
        self.structure = self.form.findChild(QGroupBox, 'groupbox_structure')
        self.desc_gen = self.form.findChild(QGroupBox, 'groupbox_desc_gen')
        self.obs_hab_action_buttons = self.form.findChild(QGroupBox, 'obs_hab_action_buttons')
        self.habitat_buttons = self.form.findChild(QButtonGroup, 'habitats_buttons')
        self.typo_habitat_buttons = self.form.findChild(QButtonGroup, 'habitat_buttons')
        self.observer_list = self.form.findChild(QListWidget, "observateurs_list")
        self.obs_hab_assoc_list = self.form.findChild(QListWidget, "list_hab_obs_assoc")
        self.lst_obs = self.form.findChild(QComboBox, "lst_obs")
        self.select_taxon = self.form.findChild(QComboBox, "select_taxon")
        self.select_habitat = self.form.findChild(QComboBox, "select_habitat")
        self.obs_hab_recouvrement = self.form.findChild(QComboBox, "obs_hab_rec_indic")
        self.obs_hab_forme = self.form.findChild(QComboBox, "obs_hab_forme_id")
        self.syntaxon_comm_non_sat_obs = self.form.findChild(QComboBox, "syn_comm_non_sat_obs")
        self.strate_arbo = self.form.findChild(QComboBox, "strate_arbo")
        self.strate_arbu = self.form.findChild(QComboBox, "strate_arbu")
        self.strate_herb = self.form.findChild(QComboBox, "strate_herb")
        self.strate_musc = self.form.findChild(QComboBox, "strate_musc")
        self.rel_type_rel_id = self.form.findChild(QComboBox, "rel_type_rel_id")
        self.rel_protocole_id = self.form.findChild(QComboBox, "rel_protocole_id")
        self.syntaxon_recherche = self.form.findChild(QComboBox, "syntaxon_recherche")
        self.habitat_recherche = self.form.findChild(QComboBox, "habitat_recherche")
        self.select_strate_struct = self.form.findChild(QComboBox, "cb_strate_struct")
        self.taxon_dominant_recherche = self.form.findChild(QComboBox, "taxon_dominant_recherche")
        self.select_taxon_dominant = self.form.findChild(QComboBox, "select_taxon_dominant")
        self.syntaxon_diag_originale = self.form.findChild(QCheckBox, "syn_diag_originale")
        self.table_habitats = self.form.findChild(QTableWidget, "table_habitats")
        self.table_syn_habitats = self.form.findChild(QTableWidget, "table_syn_habitats")
        self.table_taxons = self.form.findChild(QTableWidget, "table_taxons")
        self.table_strates_struct = self.form.findChild(QTableWidget, "table_strates_struct")
        self.tab_widget = self.form.findChild(QTabWidget, "tabWidget_phyto")

    def populate_layers(self):
        for layer in self.project.mapLayers().values():
            if layer.name() == 'releves':
                self.releves = layer
            elif layer.name() == 'observations':
                self.observations = layer
            elif layer.name() == 'obs_habitats':
                self.obs_habitats = layer
            elif layer.name() == 'liens_releve_observateur':
                self.liens_releve_observateur = layer
            elif layer.name() == 'liens_releve_strate':
                self.liens_releve_strate = layer
            elif layer.name() == 'liens_obs_habitat':
                self.liens_obs_habitat = layer
            elif layer.name() == 'observateurs':
                self.observateurs = layer
            elif layer.name() == 'catalogue_phyto':
                self.catalogue_phyto = layer
            elif layer.name() == 'taxref':
                self.taxref = layer
            elif layer.name() == 'habref':
                self.habref = layer

    def populate_combo_boxes(self):
        combo_boxes_specification: List[dict] = DatabaseSpecificationService.get_combo_boxes_specification()
        for combo_box_spec in combo_boxes_specification:
            for combo_box_name in combo_box_spec['input_name'].split(', '):
                widget: Optional[QComboBox] = self.form.findChild(QComboBox, combo_box_name)
                if widget is not None:
                    list_values = self.list_values_provider.get_values_from_list_name(combo_box_spec['input_list_name'])
                    WidgetService.load_combo_box(widget, list_values)

    def prepare_table_widget(self, add_survey_to_syntaxon_widgets):
        WidgetService.build_table(self.table_taxons, TAXONS_HEADER_LABELS)
        self.table_taxons.itemDoubleClicked.connect(
            lambda i: WidgetService.cell_clicked(i, self.survey_feature, self.observations, self.form, self.add_taxon,
                                                 self.save_taxon, self.select_taxon, self.habitat_buttons))

        self.table_habitats.itemDoubleClicked.connect(
            lambda i: WidgetService.cell_clicked(i, self.survey_feature, self.obs_habitats, self.form,
                                                 self.add_syntaxon, self.save_syntaxon, self.select_habitat,
                                                 self.habitat_buttons, add_survey_to_syntaxon_widgets))

        WidgetService.build_table(self.table_syn_habitats, HABITATS_HEADER_LABELS)
        self.table_syn_habitats.itemDoubleClicked.connect(
            lambda i: WidgetService.cell_clicked(i, self.survey_feature, self.obs_habitats, self.form, QLabel(),
                                                 QLabel(), self.habitat_recherche, QButtonGroup())
        )

        WidgetService.build_table(self.table_strates_struct, STRATES_STRUCT_LABELS)
        self.table_strates_struct.itemDoubleClicked.connect(
            lambda i: WidgetService.cell_clicked(i, self.survey_feature, self.liens_releve_strate, self.form, QLabel(),
                                                 QLabel(), self.select_strate_struct, QButtonGroup()))

    def activate_event_listeners(self, strates, add_survey_to_syntaxon_widgets):
        self.rel_type_rel_id.currentIndexChanged.connect(
            lambda: WidgetService.fobs_show(self.rel_type_rel_id, self.rel_protocole_id, self.fobs_taxons,
                                            self.fobs_syntaxons, self.ftypo_taxons, self.ftypo_syntaxons,
                                            self.save_rel, self.lab_alerte_structure, self.structure,
                                            self.lab_alerte_desc_gen, self.desc_gen))
        self.habitat_buttons.buttonClicked.connect(
            lambda: self.data_manager.get_habitats(self.survey_feature.id(), self.obs_habitats, self.table_habitats,
                                                   self.obs_hab_assoc_list, self.habitat_buttons,
                                                   self.obs_hab_code_typo, self.frame_syntaxon_complement,
                                                   self.frame_syntaxon_data_add, self.obs_hab_action_buttons))
        WidgetService.clickable(self.labrel).connect(
            lambda: WidgetService.fshow(self.rel_type_rel_id, self.rel_protocole_id, self.frel, self.fobs,
                                        self.save_rel, self.activate))
        WidgetService.clickable(self.labobs).connect(
            lambda: WidgetService.fshow(self.rel_type_rel_id, self.rel_protocole_id, self.frel, self.fobs,
                                        self.save_rel, self.activate))
        WidgetService.clickable(self.save_rel).connect(
            lambda: self.data_manager.save_survey(self.releves, self.survey_feature, self.obs_habitats))
        WidgetService.clickable(self.activate).connect(
            lambda: self.data_manager.activate_survey(self.frel, self.fobs, self.save_rel, self.activate,
                                                      self.releves, self.survey_feature, self.obs_habitats))
        WidgetService.clickable(self.add_taxon).connect(
            lambda: self.data_manager.add_taxon(self.survey_feature, self.observations, self.select_taxon,
                                                self.taxon_cite, self.taxon_cd_nom_cite, strates, self.table_taxons,
                                                self.add_taxon, self.save_taxon))
        WidgetService.clickable(self.save_taxon).connect(
            lambda: self.data_manager.save_taxon(self.survey_feature, self.observations, self.select_taxon,
                                                 self.taxon_cite, self.taxon_cd_nom_cite, strates, self.table_taxons,
                                                 self.add_taxon, self.save_taxon))
        WidgetService.clickable(self.add_syntaxon).connect(
            lambda: self.data_manager.add_habitat(self.survey_feature, self.obs_habitats, self.select_habitat,
                                                  self.obs_hab_nom_cite, self.obs_hab_code_cite,
                                                  self.obs_hab_recouvrement, self.obs_hab_forme, self.table_habitats,
                                                  self.obs_hab_assoc_list, self.add_syntaxon, self.save_syntaxon,
                                                  self.habitat_buttons))
        WidgetService.clickable(self.save_syntaxon).connect(
            lambda: self.data_manager.save_habitat(self.survey_feature, self.obs_habitats, self.select_habitat,
                                                   self.obs_hab_nom_cite, self.obs_hab_code_cite,
                                                   self.obs_hab_recouvrement, self.obs_hab_forme, self.table_habitats,
                                                   self.obs_hab_assoc_list, self.add_syntaxon, self.save_syntaxon,
                                                   add_survey_to_syntaxon_widgets, self.habitat_buttons))
        WidgetService.clickable(self.cal1_label).connect(
            lambda: WidgetService.calendar(self.cal1, self.cal1_label, self.rel_date_min, self.rel_date_max,
                                           WidgetService.transform_date_1))
        WidgetService.clickable(self.cal2_label).connect(
            lambda: WidgetService.calendar(self.cal2, self.cal2_label, self.rel_date_min, self.rel_date_max,
                                           WidgetService.transform_date_2))
        WidgetService.clickable(self.rel_create_new_polygon_rel).connect(
            lambda: self.data_manager.add_associated_survey(self.rel_create_new_polygon_rel, self.releves,
                                                            self.survey_feature))
        WidgetService.clickable(self.rel_create_new_point_rel).connect(
            lambda: self.data_manager.add_associated_survey(self.rel_create_new_point_rel, self.releves,
                                                            self.survey_feature))
        WidgetService.clickable(self.rel_create_new_line_rel).connect(
            lambda: self.data_manager.add_associated_survey(self.rel_create_new_line_rel, self.releves,
                                                            self.survey_feature))
        WidgetService.clickable(self.rel_syntax_create_new_polygon_rel).connect(
            lambda: self.data_manager.add_survey_associated_to_syntaxon(self.rel_syntax_create_new_polygon_rel,
                                                                        self.releves, self.obs_habitats,
                                                                        self.survey_feature,
                                                                        self.obs_hab_nom_cite,
                                                                        self.obs_hab_code_cite))
        WidgetService.clickable(self.rel_syntax_create_new_point_rel).connect(
            lambda: self.data_manager.add_survey_associated_to_syntaxon(self.rel_syntax_create_new_point_rel,
                                                                        self.releves, self.obs_habitats,
                                                                        self.survey_feature,
                                                                        self.obs_hab_nom_cite,
                                                                        self.obs_hab_code_cite))
        WidgetService.clickable(self.rel_syntax_create_new_line_rel).connect(
            lambda: self.data_manager.add_survey_associated_to_syntaxon(self.rel_syntax_create_new_line_rel,
                                                                        self.releves, self.obs_habitats,
                                                                        self.survey_feature,
                                                                        self.obs_hab_nom_cite,
                                                                        self.obs_hab_code_cite))
        WidgetService.clickable(self.add_obs_btn).connect(
            lambda: self.data_manager.add_observer(self.survey_feature.id(), self.liens_releve_observateur,
                                                   self.lst_obs, self.observer_list))
        WidgetService.clickable(self.suppr_obs_btn).connect(
            lambda: self.data_manager.delete_observer(self.survey_feature.id(), self.liens_releve_observateur,
                                                      self.observer_list, self.observateurs))
        WidgetService.clickable(self.add_strate_struct).connect(
            lambda: self.data_manager.add_strate(self.survey_feature.id(), self.liens_releve_strate,
                                                 self.table_strates_struct, self.select_strate_struct,
                                                 self.struct_rec_pct_strate, self.struct_haut_mod_strate,
                                                 self.struct_haut_min_strate, self.struct_haut_max_strate))
        self.select_taxon.lineEdit().returnPressed.connect(
            lambda: self.data_manager.taxon_auto_complete(self.select_taxon, self.taxref, self.taxon_cite,
                                                          self.taxon_cd_nom_cite, self.taxon_valide,
                                                          self.taxon_cd_nom_valide))
        self.taxon_dominant_recherche.lineEdit().returnPressed.connect(
            lambda: self.data_manager.taxon_auto_complete(self.taxon_dominant_recherche, self.taxref,
                                                          self.taxon_dominant_nom, self.taxon_dominant_code,
                                                          QLineEdit(), QLineEdit()))
        self.select_taxon_dominant.lineEdit().returnPressed.connect(
            lambda: self.data_manager.taxon_auto_complete(self.select_taxon_dominant, self.taxref,
                                                          self.obs_hab_taxon_dominant_nom,
                                                          self.obs_hab_taxon_dominant_code, QLineEdit(), QLineEdit()))
        self.select_habitat.lineEdit().returnPressed.connect(
            lambda: self.data_manager.habitat_auto_complete(self.select_habitat, self.catalogue_phyto, self.habref,
                                                            self.habitat_buttons, self.obs_hab_nom_cite,
                                                            self.obs_hab_code_cite, self.obs_hab_nom_retenu,
                                                            self.obs_hab_code_retenu))
        self.habitat_recherche.lineEdit().returnPressed.connect(
            lambda: self.data_manager.habitat_auto_complete(self.habitat_recherche, self.catalogue_phyto, self.habref,
                                                            self.typo_habitat_buttons, QLineEdit(), QLineEdit(),
                                                            QLineEdit(), QLineEdit(), self.survey_feature,
                                                            self.obs_habitats, self.table_syn_habitats))
        self.syntaxon_recherche.lineEdit().returnPressed.connect(
            lambda: self.data_manager.syntaxon_auto_complete(self.syntaxon_recherche, self.catalogue_phyto,
                                                             self.syntaxon_cite, self.syntaxon_code_cite,
                                                             self.syntaxon_retenu, self.syntaxon_code_retenu))

    def prepare_startup_interface(self, add_survey_to_syntaxon_widgets):
        widgets_to_hide = [self.save_taxon, self.save_syntaxon, self.syntaxon_code_cite, self.syntaxon_code_retenu,
                           self.taxon_cd_nom_cite, self.taxon_cd_nom_valide, self.rel_create_new_point_rel,
                           self.rel_create_new_line_rel, self.rel_create_new_polygon_rel, self.obs_hab_code_typo,
                           self.frame_jdd]
        widgets_to_hide.extend(add_survey_to_syntaxon_widgets)
        for widget in widgets_to_hide:
            widget.hide()

        self.frel.hide()
        self.tab_widget.setCurrentIndex(0)
        self.form.hideButtonBox()
        if self.rel_type_rel_id.itemData(self.rel_type_rel_id.currentIndex()) != 0 and self.rel_protocole_id.itemData(
                self.rel_protocole_id.currentIndex()) != 0:
            self.activate.hide()

        WidgetService.fshow(self.rel_type_rel_id, self.rel_protocole_id, self.frel, self.fobs, self.save_rel,
                            self.activate)
        WidgetService.fobs_show(self.rel_type_rel_id, self.rel_protocole_id, self.fobs_taxons, self.fobs_syntaxons,
                                self.ftypo_taxons, self.ftypo_syntaxons, self.save_rel, self.lab_alerte_structure,
                                self.structure, self.lab_alerte_desc_gen, self.desc_gen)


class MouseEventManager(QObject):
    clicked = pyqtSignal()

    def __init__(self, widget: QObject):
        super(MouseEventManager, self).__init__(widget)
        self.widget = widget

    def eventFilter(self, obj, event) -> bool:
        if obj == self.widget:
            if event.type() == QEvent.MouseButtonRelease:
                if obj.rect().contains(event.pos()):
                    self.clicked.emit()
                    return True
        return False




class WidgetService:
    @staticmethod
    def load_combo_box(combo_box: QComboBox, values: List):
        combo_box.clear()
        for v in values:
            if v[1]:
                combo_box.addItem(v[1], v[0])
            else:
                combo_box.addItem('', v[0])

    @staticmethod
    def clickable(widget: QObject) -> pyqtSignal():
        mouse_event = MouseEventManager(widget)
        widget.installEventFilter(mouse_event)
        return mouse_event.clicked

    @staticmethod
    def build_table(table: QTableWidget, header_labels: List):
        table.setEditTriggers(QTableWidget.NoEditTriggers)
        table.setColumnCount(len(header_labels))
        table.setHorizontalHeaderLabels(header_labels)

        header = table.horizontalHeader()
        for i in range(len(header_labels)):
            if i != 1:
                header.setSectionResizeMode(i, QHeaderView.ResizeToContents)
            else:
                header.setSectionResizeMode(i, QHeaderView.Stretch)

    @staticmethod
    def find_widget(form, widget_type, widget_name) -> QWidget:
        if widget_type == 'QComboBox':
            return form.findChild(QComboBox, widget_name)
        elif widget_type == 'QLineEdit':
            return form.findChild(QLineEdit, widget_name)
        elif widget_type == 'QTextEdit':
            return form.findChild(QTextEdit, widget_name)
        elif widget_type == 'QPlainTextEdit':
            return form.findChild(QPlainTextEdit, widget_name)
        elif widget_type == 'QCheckBox':
            return form.findChild(QCheckBox, widget_name)
        return QWidget()

    @staticmethod
    def get_input_value(widget):
        if isinstance(widget, QLineEdit):
            return widget.text()
        elif isinstance(widget, QTextEdit):
            return widget.toPlainText()
        elif isinstance(widget, QPlainTextEdit):
            return widget.toPlainText()
        elif isinstance(widget, QCheckBox):
            if widget.isChecked():
                return 1
            return -1
        elif isinstance(widget, QComboBox):
            return widget.currentData()

    @staticmethod
    def set_input_value(widget, value):
        if isinstance(widget, QLineEdit) and not isinstance(value, QVariant):
            widget.setText(str(value))
        elif isinstance(widget, QTextEdit) and not isinstance(value, QVariant):
            widget.setPlainText(str(value))
        elif isinstance(widget, QPlainTextEdit) and not isinstance(value, QVariant):
            widget.setPlainText(str(value))
        elif isinstance(widget, QCheckBox) and not isinstance(value, QVariant):
            if value == 1:
                widget.setCheckState(Qt.Checked)
        elif isinstance(widget, QComboBox) and not isinstance(value, QVariant):
            widget.setCurrentIndex(widget.findData(value))

    @staticmethod
    def cell_clicked(i, survey_feature: QgsFeature, observations: QgsVectorLayer, form: QgsAttributeForm,
                     add_widget: QLabel, save_widget: QLabel, select_widget: QComboBox, typo_buttons: QButtonGroup,
                     add_survey_widgets: List[QLabel] = None):
        try:
            table: QTableWidget = i.tableWidget()
            table_name = table.objectName()
            col_name = table.horizontalHeaderItem(i.column()).text()
            row = i.row()

            if col_name == 'suppr.' and add_widget.isVisible():
                WidgetService.delete_observation(observations, row, survey_feature, table, table_name)
                return
            if col_name == 'suppr.' and table_name == 'table_syn_habitats':
                WidgetService.delete_typo_habitat(observations, row, survey_feature, table)
                return

            add_survey_widgets = [] if add_survey_widgets is None else add_survey_widgets
            if table_name == 'table_habitats':
                TableWidgetService.edit_syntaxon(survey_feature, row, observations, table, form,
                                                 add_widget, save_widget, add_survey_widgets, select_widget,
                                                 typo_buttons)
            elif table_name == 'table_taxons':
                TableWidgetService.edit_taxon(survey_feature, row, observations, table, form,
                                              add_widget, save_widget, add_survey_widgets, select_widget, typo_buttons)
        except:
            pass

    @staticmethod
    def delete_observation(observations, row, survey_feature, table, table_name):
        if table_name == 'table_habitats':
            code_cell_value = table.item(row, 0).text() if table.item(row, 0) else None
            obs_habitat = TableWidgetService.find_observation(table.item(row, 0).text(), table.item(row, 1).text(),
                                                              'obs_hab_code_cite', 'obs_hab_nom_cite',
                                                              'obs_hab_rel_id', observations, survey_feature)
            TableWidgetService.del_row(survey_feature, row, observations, table, 'obs_hab_rel_id',
                                       'obs_hab_code_cite', 'obs_hab_nom_cite', code_cell_value,
                                       table.item(row, 1).text())
            if obs_habitat:
                obs_habitat_id = int(obs_habitat['obs_hab_id'])
                sqlite_data_manager = DataManager(QgsProject.instance())
                sqlite_data_manager.remove_links_by_obs_hab_id(obs_habitat_id)
                if obs_habitat['obs_hab_code_typo'] == 0 and obs_habitat['obs_hab_rel_assoc_id'] is not None:
                    sqlite_data_manager.clean_obs_hab_link_in_assoc_survey(obs_habitat['obs_hab_rel_assoc_id'])
                    sqlite_data_manager.clean_other_obs_hab_link_to_assoc_survey(obs_habitat['obs_hab_rel_assoc_id'])

        elif table_name == 'table_taxons':
            code_cell_value = int(table.item(row, 0).text()) \
                if table.item(row, 0) and table.item(row, 0).text() != '' else None
            TableWidgetService.del_row(survey_feature, row, observations, table, 'obs_rel_id', 'cd_nom', 'nom_cite',
                                       code_cell_value, table.item(row, 1).text())
        elif table_name == 'table_strates_struct':
            code_cell_value = int(table.item(row, 0).text()) \
                if table.item(row, 0) and table.item(row, 0).text() != '' else None
            TableWidgetService.del_row(survey_feature, row, observations, table, 'rel_id', 'strate_id', 'strate_id',
                                       code_cell_value, code_cell_value)

    @staticmethod
    def delete_typo_habitat(observations, row, survey_feature, table):
        code_cell_value = table.item(row, 0).text() if table.item(row, 0) else None
        obs_habitat = TableWidgetService.find_observation(table.item(row, 0).text(), table.item(row, 1).text(),
                                                          'obs_hab_code_cite', 'obs_hab_nom_cite',
                                                          'obs_hab_rel_assoc_id', observations, survey_feature)
        if obs_habitat:
            sqlite_data_manager = DataManager(QgsProject.instance())
            survey_id = survey_feature['id']
            obs_syntaxon_id: int
            if survey_id is None:
                survey_id = sqlite_data_manager.get_survey_id_from_geometry(survey_feature)
                obs_syntaxon_id = sqlite_data_manager.get_obs_syntaxon_id_from_survey_id(survey_id)
            else:
                obs_syntaxon_id = int(survey_feature['rel_obs_syntaxon_id'])
            obs_habitat_id = int(obs_habitat['obs_hab_id'])
            if sqlite_data_manager.is_obs_hab_linked_to_other(obs_habitat_id, obs_syntaxon_id):
                sqlite_data_manager.remove_links_between_obs_hab(obs_habitat_id, obs_syntaxon_id)
                sqlite_data_manager.clean_link_to_assoc_survey(obs_habitat_id)
                table.removeRow(row)
            else:
                sqlite_data_manager.remove_links_by_obs_hab_id(obs_habitat_id)
                TableWidgetService.del_row(survey_feature, row, observations, table, 'obs_hab_rel_assoc_id',
                                           'obs_hab_code_cite', 'obs_hab_nom_cite', code_cell_value,
                                           table.item(row, 1).text())
        else:
            iface.messageBar().pushWarning('Suppression d\'observation habitat', 'Vous ne pouvez pas supprimer une '
                                                                                 'observation qui n\'a pas ete creee '
                                                                                 'dans ce releve.')

    @staticmethod
    def fshow(rel_type_widget: QComboBox, rel_protocol_widget: QComboBox, frel: QFrame, fobs: QFrame,
              save_rel: QLabel, activate: QLabel):
        if rel_type_widget.itemData(rel_type_widget.currentIndex()) != 0 and rel_protocol_widget.itemData(
                rel_protocol_widget.currentIndex()) != 0 and activate.isHidden():
            rel_type_widget.setEnabled(False)
            if frel.isHidden():
                frel.show()
                fobs.hide()
            else:
                frel.hide()
                fobs.show()
        else:
            frel.hide()
            fobs.hide()
            save_rel.hide()

    @staticmethod
    def fobs_show(rel_type_widget: QComboBox, rel_protocol_widget: QComboBox, fobs_taxons: QFrame,
                  fobs_syntaxons: QFrame, ftypo_taxons: QFrame, ftypo_syntaxons: QFrame, activate: QLabel,
                  label_structure: QLabel, structure_box: QGroupBox, label_desc_gen: QLabel, desc_gen_box: QGroupBox):
        current_index = rel_type_widget.currentIndex()
        current_protocol_data = rel_protocol_widget.currentData()
        WidgetService.load_protocol_list(current_index, rel_protocol_widget)

        if rel_protocol_widget.findData(current_protocol_data) > -1:
            rel_protocol_widget.setCurrentIndex(rel_protocol_widget.findData(current_protocol_data))
            # TODO avoid null entry to be removed before activation
            rel_protocol_widget.removeItem(rel_protocol_widget.findData(0))

        if rel_type_widget.itemData(current_index) in [1, 2]:
            fobs_taxons.show()
            ftypo_taxons.show()
            fobs_syntaxons.hide()
            ftypo_syntaxons.hide()
            label_structure.hide()
            label_desc_gen.hide()
            structure_box.show()
            desc_gen_box.show()
            activate.setEnabled(True)
        elif rel_type_widget.itemData(current_index) == 3:
            fobs_taxons.hide()
            ftypo_taxons.hide()
            fobs_syntaxons.show()
            ftypo_syntaxons.show()
            label_structure.show()
            label_desc_gen.show()
            structure_box.hide()
            desc_gen_box.hide()
            activate.setEnabled(True)
        else:
            fobs_taxons.hide()
            ftypo_taxons.hide()
            fobs_syntaxons.hide()
            ftypo_syntaxons.hide()
            label_structure.hide()
            label_desc_gen.hide()
            structure_box.hide()
            desc_gen_box.hide()
            activate.setEnabled(False)

    @staticmethod
    def calendar(calendar_box, calendar_label, rel_date_min, rel_date_max, transform_function):
        if calendar_label.isEnabled():
            calendar_box.setWindowTitle("Calendrier")
            msglt = calendar_box.layout()
            qcal1 = QCalendarWidget()
            msglt.addWidget(qcal1)
            qcal1.clicked.connect(lambda date: transform_function(date, rel_date_min, rel_date_max, calendar_box))
            calendar_box.exec_()

    @staticmethod
    def transform_date_1(date, rel_date_min: QLineEdit, rel_date_max: QLineEdit, cal1: QMessageBox):
        rel_date_min.setText(str(date.toString('yyyy-MM-dd')))
        rel_date_max.setText(str(date.toString('yyyy-MM-dd')))
        cal1.close()

    @staticmethod
    def transform_date_2(date, rel_date_min: QLineEdit, rel_date_max: QLineEdit, cal2: QMessageBox):
        # TODO adapt date below...
        if date < QDate.fromString(rel_date_min.text(), 'yyyy-MM-dd'):
            rel_date_max.setStyleSheet("background : rgba(255, 1, 1, 80)")
        else:
            rel_date_max.setStyleSheet("background : white")
            rel_date_max.setText(str(date.toString('yyyy-MM-dd')))
            cal2.close()

    @staticmethod
    def load_protocol_list(rel_type_rel_id: int, rel_protocol_widget: QComboBox):
        list_values_provider = ListValuesProvider(QgsProject.instance())
        protocole_values = list_values_provider.get_values_from_list_name('PROTOCOLE_VALUES')
        protocol_list = [protocol for protocol in protocole_values if protocol[2] in (0, rel_type_rel_id)]
        WidgetService.load_combo_box(rel_protocol_widget, protocol_list)




class TableWidgetService:
    @staticmethod
    def clickable(widget: QObject) -> pyqtSignal():
        mouse_event = MouseEventManager(widget)
        widget.installEventFilter(mouse_event)
        return mouse_event.clicked

    @staticmethod
    def edit_syntaxon(survey_feature: QgsFeature, row: int, observations: QgsVectorLayer, sheet: QTableWidget,
                      form: QgsAttributeForm, add_widget: QLabel, save_widget: QLabel,
                      add_survey_widgets: List[QLabel], select_widget: QComboBox, typo_buttons: QButtonGroup):
        code_phyto = sheet.item(row, 0).text() if sheet.item(row, 0) and sheet.item(row, 0).text() != '' else None
        TableWidgetService.edit_observation(survey_feature, observations, form, add_widget, save_widget,
                                            add_survey_widgets, select_widget, typo_buttons, 'obs_habitat_',
                                            'obs_hab_rel_id', 'obs_hab_code_cite', 'obs_hab_nom_cite', code_phyto,
                                            sheet.item(row, 1).text())

    @staticmethod
    def edit_taxon(survey_feature: QgsFeature, row: int, observations: QgsVectorLayer, sheet: QTableWidget,
                   form: QgsAttributeForm, add_widget: QLabel, save_widget: QLabel, add_survey_widgets: List[QLabel],
                   select_widget: QComboBox, typo_buttons: QButtonGroup):
        cd_nom = int(sheet.item(row, 0).text()) if sheet.item(row, 0) and sheet.item(row, 0).text() != '' else None
        TableWidgetService.edit_observation(survey_feature, observations, form, add_widget, save_widget,
                                            add_survey_widgets, select_widget, typo_buttons, 'nom_', 'obs_rel_id',
                                            'cd_nom', 'nom_cite', cd_nom, sheet.item(row, 1).text())

    @staticmethod
    def edit_observation(survey_feature: QgsFeature, observations: QgsVectorLayer, form: QgsAttributeForm,
                         add_widget: QLabel, save_widget: QLabel, add_survey_widgets: List[QLabel],
                         select_widget: QComboBox, typo_buttons: QButtonGroup, keyword: str, obs_rel_field_name: str,
                         obs_code_field_name: str, obs_name_field_name: str, id_to_compare: object,
                         text_to_compare: object):
        form_inputs = DatabaseSpecificationService.get_inputs_specification(observations.name())
        if len(form_inputs) == 0:
            return
        observation = TableWidgetService.find_observation(id_to_compare, text_to_compare, obs_code_field_name,
                                                          obs_name_field_name, obs_rel_field_name, observations,
                                                          survey_feature)
        if observation:
            TableWidgetService.hydrate_form(form, form_inputs, keyword, observation)
            if observations.name() == 'obs_habitats':
                TableWidgetService.populate_obs_assoc_list(survey_feature, observation, observations, form,
                                                           typo_buttons)

            for widget in add_survey_widgets:
                widget.show()
            for button in typo_buttons.buttons():
                button.setEnabled(False)
            select_widget.setEnabled(False)
            add_widget.hide()
            save_widget.show()

    @staticmethod
    def hydrate_form(form: QgsAttributeForm, form_inputs: List[dict], keyword: str, observation: QgsFeature):
        for form_input in form_inputs:
            widget = WidgetService.find_widget(form, form_input['input_type'],
                                               form_input['input_name'].split(',')[0])
            if widget:
                TableWidgetService.clean_widget(widget)
                field_names = [form_input['field_name'] for form_input in form_inputs
                               if form_input['input_name'].split(',')[0] == widget.objectName()]
                if len(field_names) > 0 and observation:
                    WidgetService.set_input_value(widget, observation[field_names[0]])
                if keyword in widget.objectName():
                    widget.setEnabled(False)

    @staticmethod
    def populate_obs_assoc_list(survey_feature: QgsFeature, observation: QgsFeature, observations: QgsVectorLayer,
                                form: QgsAttributeForm, typo_buttons: QButtonGroup):
        obs_hab_list_widget: QListWidget = form.findChild(QListWidget, 'list_hab_obs_assoc')
        try:
            obs_hab_list_widget.itemChanged.disconnect()
        except:
            pass
        obs_hab_list_widget.clear()
        if obs_hab_list_widget is not None:
            code_typo = TableWidgetService.get_code_typo(typo_buttons)
            consulted_obs_hab_id = observation['obs_hab_id']
            obs_hab_list = [observation for observation in observations.getFeatures()
                            if observation['obs_hab_rel_id'] == survey_feature.id()
                            and observation['obs_hab_code_typo'] != code_typo]
            sqlite_data_manager = DataManager(QgsProject.instance())
            for obs_hab in obs_hab_list:
                row = obs_hab_list_widget.count()
                obs_hab_typo = obs_hab['obs_hab_code_typo']
                obs_hab_id = obs_hab['obs_hab_id']
                label = TableWidgetService.get_typo_label_from_code(obs_hab_typo) + ' - '
                label += obs_hab['obs_hab_code_cite'] + ' - ' + obs_hab['obs_hab_nom_cite']
                is_linked = sqlite_data_manager.are_obs_hab_linked(obs_hab_id, consulted_obs_hab_id)
                obs_hab_item = QListWidgetItem()
                obs_hab_item.setText(label)
                obs_hab_item.setData(Qt.UserRole, obs_hab_id)
                obs_hab_item.setBackground(TableWidgetService.get_typo_color_from_code(obs_hab['obs_hab_code_typo']))
                obs_hab_item.setCheckState(Qt.Checked if is_linked else Qt.Unchecked)
                obs_hab_list_widget.insertItem(row, obs_hab_item)
            obs_hab_list_widget.sortItems(Qt.DescendingOrder)
            obs_hab_list_widget.itemChanged.connect(
                lambda item: TableWidgetService.manage_association_between_obs_hab(item, obs_hab_list_widget,
                                                                                   observation, observations))

    @staticmethod
    def manage_association_between_obs_hab(list_item: QListWidgetItem, obs_hab_list_widget: QListWidget,
                                           obs_hab: QgsFeature, observations: QgsVectorLayer):
        obs_hab_list_widget.itemChanged.disconnect()
        obs_hab_id = obs_hab['obs_hab_id']
        obs_hab_id_to_link = list_item.data(Qt.UserRole)
        obs_hab_to_link = observations.getFeature(obs_hab_id_to_link)
        sqlite_data_manager = DataManager(QgsProject.instance())
        if list_item.checkState() == 2:
            sqlite_data_manager.insert_links_between_obs_hab(obs_hab_id, list_item.data(Qt.UserRole))
        else:
            sqlite_data_manager.remove_links_between_obs_hab(obs_hab_id, list_item.data(Qt.UserRole))
            if obs_hab['obs_hab_code_typo'] == 0:
                observations.startEditing()
                obs_hab_to_link['obs_hab_rel_assoc_id'] = None
                observations.updateFeature(obs_hab_to_link)
                observations.commitChanges()
            elif obs_hab_to_link['obs_hab_code_typo'] == 0:
                observations.startEditing()
                obs_hab['obs_hab_rel_assoc_id'] = None
                observations.updateFeature(obs_hab)
                observations.commitChanges()
        obs_hab_list_widget.itemChanged.connect(
            lambda item: TableWidgetService.manage_association_between_obs_hab(item, obs_hab_list_widget,
                                                                               obs_hab, observations))

    @staticmethod
    def del_row(survey_feature: QgsFeature, row: int, observations: QgsVectorLayer, table: QTableWidget,
                obs_rel_field_name: str, obs_code_field_name: str, obs_name_field_name: str, id_to_compare: object,
                text_to_compare: object):
        observation = TableWidgetService.find_observation(id_to_compare, text_to_compare, obs_code_field_name,
                                                          obs_name_field_name, obs_rel_field_name, observations,
                                                          survey_feature)
        if observation:
            observations.deleteFeature(observation.id())
            observations.commitChanges()
            table.removeRow(row)

    @staticmethod
    def find_observation(id_to_compare, text_to_compare, obs_code_field_name, obs_name_field_name, obs_rel_field_name,
                         observations, survey_feature) -> Optional[QgsFeature]:
        survey_id = survey_feature['id']
        if survey_id is None:
            sqlite_data_manager = DataManager(QgsProject.instance())
            survey_id = sqlite_data_manager.get_survey_id_from_geometry(survey_feature)
        observation_result: List[QgsFeature] = [observation for observation in observations.getFeatures()
                                                if observation[obs_rel_field_name] == survey_id
                                                and observation[obs_code_field_name] == id_to_compare
                                                and observation[obs_name_field_name] == text_to_compare]
        if len(observation_result) == 0:
            return None
        else:
            if not observations.isEditable():
                observations.startEditing()
            return observation_result[0]

    @staticmethod
    def clean_widget(widget):
        if isinstance(widget, QLineEdit):
            widget.setText("")
        elif isinstance(widget, QPlainTextEdit) or isinstance(widget, QTextEdit):
            widget.setPlainText("")
        elif isinstance(widget, QComboBox):
            widget.setCurrentIndex(0)
        elif isinstance(widget, QCheckBox):
            widget.setCheckState(Qt.Unchecked)

    @staticmethod
    def get_code_typo(buttons: QButtonGroup) -> int:
        button_name = buttons.checkedButton().objectName()
        if button_name == "rb_phyto":
            return 0
        elif button_name == "rb_hic" or button_name == "rb_typo_hic":
            return 4
        elif button_name == "rb_eunis" or button_name == "rb_typo_eunis":
            return 7
        elif button_name == "rb_cb" or button_name == "rb_typo_cb":
            return 22
        return 0

    @staticmethod
    def get_header_labels_from_typo(code_typo: int) -> List:
        if code_typo == 0:
            return SYNTAXONS_HEADER_LABELS
        elif code_typo == 4:
            return HIC_HEADER_LABELS
        elif code_typo == 7:
            return EUNIS_HEADER_LABELS
        elif code_typo == 22:
            return CORINE_HEADER_LABELS
        return []

    @staticmethod
    def get_typo_label_from_code(code_typo: int) -> str:
        if code_typo == 0:
            return "Phyto"
        elif code_typo == 4:
            return "HIC"
        elif code_typo == 7:
            return "EUNIS"
        elif code_typo == 22:
            return "Corine"
        return ""

    @staticmethod
    def get_typo_color_from_code(code_typo: int) -> QColor:
        if code_typo == 0:
            return QColor(Qt.white)
        elif code_typo == 4:
            return QColor(127, 201, 255)
        elif code_typo == 7:
            return QColor(255, 178, 127)
        elif code_typo == 22:
            return QColor(153, 255, 137)
        return QColor(Qt.white)

    @staticmethod
    def do_nothing():
        pass

    @staticmethod
    def add_new_survey(self):
        pass



DEBUGMODE = True


def form_open(dialog, layer, feature):
    iface.messageBar().pushInfo('Habitats application', 'launched')

    project = QgsProject.instance()
    # TODO build or alter database through another way (plugin ?), properly not at form launch...
    database_builder: DatabaseBuilder = DatabaseBuilder(project)
    database_builder.build_or_alter()
    iface.messageBar().pushInfo('Database connection', 'database mounted and complete')

    form_manager: SurveyFormManager = SurveyFormManager(dialog, layer, feature)
    form_manager.initialize()
    iface.messageBar().pushInfo('Habitats form', 'on air')

    # TODO should be launch another way (plugin)
    # database_cleaner: DatabaseCleaner = DatabaseCleaner(connection)
    # database_cleaner.clean_orphans()
    # iface.messageBar().pushInfo('Database connection', 'database cleaned')
