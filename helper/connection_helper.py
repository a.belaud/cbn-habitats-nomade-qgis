import sqlite3
from sqlite3 import Connection
from typing import List, Optional

from qgis.core import QgsProject, QgsApplication, QgsAuthMethodConfig, QgsAuthManager
from qgis.gui import QgisInterface

from ..constants.connection_constants import SQLITE_FILE, AUTH_CONFIG


class ConnectionHelper:
    def __init__(self, iface: QgisInterface):
        self.iface = iface

    def test_authentication_config(self) -> bool:
        auth_manager: QgsAuthManager = QgsApplication.authManager()
        config_ids: List[str] = auth_manager.configIds()
        try:
            config_ids.index(AUTH_CONFIG)
        except ValueError:
            self.iface.messageBar().pushWarning('Authentification', 'votre profil Lobelia n\'est pas défini')
            return False
        self.iface.messageBar().pushInfo('Authentification', 'votre profil Lobelia est bien connu')
        return True

    def create_authentication_profile(self):
        auth_manager = QgsApplication.authManager()
        config = QgsAuthMethodConfig()
        config.setMethod('Basic')
        config.setName('lobelia_user')
        config.setId(AUTH_CONFIG)
        config.setConfig('username', 'your_email')
        config.setConfig('password', 'your_lobelia_password')
        auth_manager.storeAuthenticationConfig(config)
        self.iface.messageBar().pushInfo('Authentification', 'un exemple de profil Lobelia a été créé')

    @staticmethod
    def sqlite_connection(project: QgsProject) -> Optional[Connection]:
        connection: Optional[Connection] = None
        try:
            connection = sqlite3.connect(project.homePath() + "\\" + SQLITE_FILE)
            connection.enable_load_extension(True)
            connection.execute('SELECT load_extension("mod_spatialite")')
            connection.execute('SELECT InitSpatialMetaData(1);')
            connection.row_factory = sqlite3.Row
            # connection.text_factory = lambda x: x.decode("utf-8") # or str or bytes... # TODO needed ?
            connection.enable_load_extension(False)
        finally:
            return connection
