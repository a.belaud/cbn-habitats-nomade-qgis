# coding: utf-8

from qgis.core import QgsProject
from qgis.utils import iface

from ..data.sqlite_database_builder import DatabaseBuilder
from ..interface.survey_form_manager import SurveyFormManager

DEBUGMODE = True


def form_open(dialog, layer, feature):
    iface.messageBar().pushInfo('Habitats application', 'launched')

    project = QgsProject.instance()
    # TODO build or alter database through another way (plugin ?), properly not at form launch...
    database_builder: DatabaseBuilder = DatabaseBuilder(project)
    database_builder.build_or_alter()
    iface.messageBar().pushInfo('Database connection', 'database mounted and complete')

    form_manager: SurveyFormManager = SurveyFormManager(dialog, layer, feature)
    form_manager.initialize()
    iface.messageBar().pushInfo('Habitats form', 'on air')

    # TODO should be launch another way (plugin)
    # database_cleaner: DatabaseCleaner = DatabaseCleaner(connection)
    # database_cleaner.clean_orphans()
    # iface.messageBar().pushInfo('Database connection', 'database cleaned')
