from pathlib import Path

from markdown import markdown
from weasyprint import HTML, CSS
from weasyprint.fonts import FontConfiguration

# TODO target a fresh wiki repo in argument ?
dir_path = Path.cwd()
wiki_repo_path = dir_path.parent / 'lobelia_qgis_wiki'
input_filenames = ['installation.md', 'documentation-utilisateur.md']

for input_filename in input_filenames:
    input_file_path = wiki_repo_path / input_filename
    output_filename = input_filename.split('.')[0] + '.pdf'
    output_file_path = dir_path / output_filename

    with open(input_file_path, 'r') as f:
        html_text = markdown(f.read(), output_format='html')

    title = input_filename.split('.')[0].replace('-', ' ')
    html_text = '<h1>' + title.capitalize() + '</h1>' + html_text
    html_text = html_text.replace("<img alt=\"image\" src=\"uploads",
                                  "<img src=\"file://" + str(wiki_repo_path) + "/uploads")
    html_text = html_text.replace("href=\"uploads",
                                  "href=\"https://gitlab.com/a.belaud/cbn-habitats-nomade-qgis/-/wikis/uploads")
    print(html_text)
    html = HTML(string=html_text)
    font_config = FontConfiguration()
    css = CSS(string='''
        @page { size: A4; margin: 1.5cm; 
            @bottom-right { font-size: 80%; content: "page " counter(page) " / " counter(pages); }
        }
        @font-face { font-family: Arial; }
        img { max-width: 100%; }
        h1 { text-align: center; color: rgb(74, 155, 40); }
        h2 { color: rgb(127, 201, 255); }
        h3 { color: rgb(255, 178, 127); font-style: italic; }
        h4 { color: rgb(74, 155, 40); font-style: italic; }
        em { font-size: 80% }
    ''', font_config=font_config)
    html.write_pdf(output_file_path, stylesheets=[css], font_config=font_config)
