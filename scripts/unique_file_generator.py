import os
from pathlib import Path
from typing import List

# Get the filenames in dir
dir_path = Path.cwd()
_, _, filenames = next(os.walk(dir_path))

# Select only python code files to aggregate
my_code_file_list: List[Path] = []
for file in dir_path.rglob('*.py'):
    if file.name not in ['__init__.py', 'unique_file_generator.py', 'ingest_service.py', 'cat_phyto_ingest_service.py',
                         'habref_ingest_service.py', 'list_values_ingest_service.py', 'observers_ingest_service.py',
                         'release_package_generator.py', 'pdf_documentation_generator.py', 'survey_form.py']:
        my_code_file_list.append(file)

# Aggregate code
if Path.exists(dir_path / "survey_form.py"):
    Path.unlink(dir_path / "survey_form.py")

with open(dir_path / "survey_form.py", "a", encoding="utf-8") as target:
    target.write('# coding: utf-8\n')

for file in my_code_file_list:
    with open(dir_path / file, "r", encoding="utf-8") as source, \
            open(dir_path / "survey_form.py", "a", encoding="utf-8") as target:
        for line in source:
            if line.startswith("import") or (line.startswith("from") and not line.startswith("from .")):
                # TODO improve the imports removing the duplicates
                target.write(line)

for file in my_code_file_list:
    if file.name != "survey_form_base.py" and file.parent.name != "utils":
        with open(dir_path / file, "r", encoding="utf-8") as source, \
                open(dir_path / "survey_form.py", "a", encoding="utf-8") as target:
            for line in source:
                if not line.startswith("import") and not line.startswith("from") and not line.startswith(
                        "#") and not line.startswith("\"\"\""):
                    target.write(line)

with open(dir_path / "scripts/survey_form_base.py", "r", encoding="utf-8") as source, \
        open(dir_path / "survey_form.py", "a", encoding="utf-8") as target:
    for line in source:
        if not line.startswith("from") and not line.startswith("#") and not line.startswith("\"\"\""):
            target.write(line)
